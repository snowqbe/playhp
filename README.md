PlayHP - Fun yet powerful PHP Web Framework
============================================

Why another?
--------------------------
* Because we needed to use a modern web framework we can adapt to our business needs.
* Because it's fun.


What does it require?
--------------------------
We tried to keep requirements as low as possible, i.e. the same requirements as you would have when creating a raw PHP script:

* PHP 5.3+ (we use namespaces)
* SimpleXML
* For database  mapping:
	* PDO
	* Whatever PDO driver you must have depending on the database engine you're targeting (and obviously its dependencies)
* For unit testing:
	* cURL
	
We also use [composer](http://getcomposer.org "Visit Composer") for easy installation.




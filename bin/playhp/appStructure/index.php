<?php
/**
 * PlayHP app boot
 */

define('P_MODE', 'dev'); // 'dev', 'test' or 'prod'
date_default_timezone_set('Europe/Paris'); // Or whatever

require './vendor/autoload.php'; // Composer

\PlayHP\PlayHP::run(); // And go!
<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace YourApp;

use PlayHP\DB\Model;

/**
 * User model
 * @package YourApp
 *
 * @Table(USER_TABLE)
 */
class User extends Model
{
    /**
     * @Id(name="user_id", autoIncrement=true)
     * @var int
     */
    public $id;

    /**
     * @Column(user_email)
     * @var string
     */
    public $email;
}
<?php
/**
 * Main layout
 * @var \PlayHP\Controllers\Rendering\View $this
 */

$this->charset('utf-8');

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= _("PlayHP - Welcome") ?></title>
    <?php $this->renderHeader() ?>

    <!-- Le styles -->
    <link href="<?= STATIC_BASE ?>css/styles.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" type="image/png" href="<?= STATIC_BASE ?>img/favicon.png">
</head>
<body>
<?php $this->renderBody() ?>
<?= $this->getScriptAsHtml() ?>
</body>
</html>
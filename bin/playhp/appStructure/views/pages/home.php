<?php
/**
 * Home page view
 * ${HomeController}
 *
 * @var \PlayHP\Controllers\Rendering\View $this
 */

$this->useLayout('main');

$this->renderPart('header');

?>

    <p><?= _("Welcome") ?></p>

<?php

$this->renderPart('footer');

?>
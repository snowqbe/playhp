<?php

namespace ${App}\Controllers;

use PlayHP\Controllers\Controller;

/**
 * ${ClassName} controller.
 *
 * @package ${App}\Controllers
 */
class ${ClassName} extends Controller
{
    public function route()
    {
        $this->render('view', get_defined_vars());
}
}
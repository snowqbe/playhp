<?php
/**
 * ${ClassName} layout
 *
 * @var \PlayHP\Controllers\Layout $this
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= _('Title') ?></title>
    <?php $this->renderHeader() ?>

    <!-- Styles -->
    <link href="<?= STATIC_BASE ?>css/bootstrap.css" rel="stylesheet">
    <link href="<?= STATIC_BASE ?>css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" type="image/png" href="<?= STATIC_BASE ?>img/favicon.png">
</head>
<body>
<?php $this->renderBody() ?>
</body>
</html>
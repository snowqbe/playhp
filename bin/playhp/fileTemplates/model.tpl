<?php

namespace ${App}\Models;

/**
 * ${Controller} controller.
 *
 * @package ${App}\Controllers
 *
 * @Table(table)
 */
class ${ClassName}
{
    /**
     * @var int
     * @Id(name="${ClassName}_id", autoIncrement=true, sequenceName="seq")
     */
    public $id;

    /**
     * @var string
     * @Column(nullable=false, length=150)
     */
    public $label;
}
<?php
/**
 * ${ClassName} page
 *
 * @var \PlayHP\Controllers\Layout $this
 */

$this->useLayout('${Layout}');

?>
<header>
    <h1><?= _("Header title") ?></h1>
</header>
<section>
    <h1><?= _("Page title") ?></h1>

    <p><?= _('You\'ve set up your page!') ?></p>
</section>
<footer>
    <p><?= _("Copyright © - You") ?></p>
</footer>
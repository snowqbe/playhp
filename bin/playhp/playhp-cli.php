#!/usr/bin/env php
<?php


/**
 * PlayHP command line commands
 */

const EXIT_OK = 0;

define('ERR_HELP', 1);
define('ERR_ARGS', 2);
define('ERR_IO', 3);
define('ERR_COMMAND', 4);
define('ERR_EXISTS', 5);
define('ERR_NO_APP', 6);

//------------------------------
// PARSE ARGUMENTS

$debug = false;
array_shift($argv);
$callPath = array_shift($argv);
$command = array_shift($argv);
$params = array();
while (count($argv) > 0) {
    $value = array_shift($argv);
    if ($value === '--debug') {
        $debug = true;
    } else {
        $params[] = $value;
    }
}

define('DBG', $debug);

//------------------------------
// FUNCTIONS

function debugParams()
{
    global $callPath, $command, $params;

    print_r($callPath);
    echo PHP_EOL;
    print_r($command);
    echo PHP_EOL;
    print_r($params);
    echo DBG ? "debug mode on" : "debug mode off";
    echo PHP_EOL;
    exit;

}

function endScript($message, $errorCode = 0)
{
    echo $message . PHP_EOL . PHP_EOL;
    if (DBG) {
        echo "Exit code $errorCode";
    }
    exit;
}

function trace($obj)
{
    if (DBG === true) {
        print_r($obj);
        echo PHP_EOL;
    }
}

function isYes($answer)
{
    return $answer === 'yes' || $answer === 'y';
}


//------------------------------
// INIT PATHS AND APP NAME

$dir = strtr(dirname(__FILE__), '\\', '/');
$playHP = $dir . '/../../src/main/PlayHP';
require "$playHP/IO/FileSystem.php";

$mainSrcPath = $callPath . '/src/main/';
$appName = null;
if (is_dir($mainSrcPath)) {
    $d = dir($mainSrcPath);
    while (false !== ($entry = $d->read())) {
        if ($entry !== '.' && $entry !== '..' && is_dir($mainSrcPath . $entry)) {
            $appName = $entry;
            break;
        }
    }
    $d->close();
}

//------------------------------
// LET'S GO

echo PHP_EOL . "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" . PHP_EOL;
echo "Thanks for choosing PlayHP to code $appName :)" . PHP_EOL;
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" . PHP_EOL . PHP_EOL;

switch ($command) {
    case 'ctrl':
    case 'page':
    case 'part':
    case 'model':
    case 'layout':
        if ($appName === null) {
            endScript('No PlayHP app detected at current path, aborting...', ERR_NO_APP);
        }
        createFile();
        break;
    case 'new':
        newApp();
        break;
    default:
        $message = "PlayHP Command line help" . PHP_EOL;
        $message .= "-----------------------------------" . PHP_EOL . PHP_EOL;
        $message .= " `new :name [:rewriteBase]`" . PHP_EOL . "    Creates a new PlayHP app called 'name'. You can optionaly define " . PHP_EOL;
        $message .= "    the rewrite base of Apache mod_rewrite configuration" . PHP_EOL . PHP_EOL;
        $message .= " `ctrl :name`" . PHP_EOL . "    Create a controller named 'name'" . PHP_EOL . PHP_EOL;
        $message .= " `page :name [:layout]`" . PHP_EOL . "    Create a page in the views/page folder named 'name' and using layout named 'layout'." . PHP_EOL;
        $message .= "    If layout is omitted, the main layout is used" . PHP_EOL . PHP_EOL;
        $message .= " `part :name [:page_override]`" . PHP_EOL . "    Create a part named 'name'. For overriding parts for given pages, you " . PHP_EOL;
        $message .= "    must add the optional 'page_override' parameter which should then be set to the name of the " . PHP_EOL;
        $message .= "    page you want to override the default part for." . PHP_EOL . PHP_EOL;
        $message .= " `model :name`" . PHP_EOL . "    Create a model named 'name'. Models inherit from Model class and can be mapped to a" . PHP_EOL;
        $message .= "    relational database" . PHP_EOL . PHP_EOL;
        $message .= " `layout :name`" . PHP_EOL . "    Create a layout file named 'name'. Layouts usually propose a basis for HTML pages" . PHP_EOL . PHP_EOL;
        $message .= "-----------------------------------" . PHP_EOL;
        $message .= " You can use --debug to activate debug traces";
        endScript($message, 1);
        break;

}


//------------------------------
// FUNCTIONS

/**
 * Creates the file according to called command
 */
function createFile()
{
    global $params, $appName, $callPath, $dir, $command;

    $type = $command;
    $fileNameSuffix = ''; // For part overrides
    $layoutName = 'main';

    if (empty($params)) {
        endScript('ERROR : you must provide the name of the file to create.', ERR_ARGS);
    }
    $className = array_shift($params);
    $folderName = '';
    switch ($type) {
        case 'ctrl':
            $folderName = '/src/main/' . $appName . '/Controllers/';
            break;
        case 'model':
            $folderName = '/src/main/' . $appName . '/Models/';
            break;
        case 'page':
            if (!empty($params)) {
                // If part override
                $layoutName = array_shift($params);
            }
            $folderName = '/views/pages/';
            break;
        case 'part':
            if (!empty($params)) {
                // If part override
                $fileNameSuffix = '_' . array_shift($params);
            }
            $folderName = '/views/parts/';
            break;
        case 'layout':
            $folderName = '/views/layouts/';
            break;
        default:
            endScript("ERROR: unrecognized type $type", ERR_COMMAND);
            break;
    }
    $fileRelativePath = $folderName . $className . $fileNameSuffix . '.php';
    $absoluteFile = $callPath . $fileRelativePath;
    if (file_exists($absoluteFile)) {
        echo ucfirst($type) . " file named $className already exists at $fileRelativePath. Replace? [no]: ";
        $answer = strtolower(trim(fgets(STDIN)));
        if (isYes($answer)) {
            unlink($absoluteFile);
        } else {
            endScript('Operation aborted.');
        }
    }
    $controllerContents = file_get_contents("$dir/fileTemplates/$type.tpl");
    $controllerContents = strtr($controllerContents, array(
        '${App}' => $appName,
        '${ClassName}' => $className,
        '${Layout}' => $layoutName,
    ));
    if (file_put_contents($absoluteFile, $controllerContents) === false) {
        endScript('Unable to write the file. Please check the write access.', ERR_IO);
    }
    endScript("Created $fileRelativePath.");
}

/**
 * Creates a new PlayHP app's structure
 */
function newApp()
{
    global $params, $appName, $callPath, $dir;

    checkForComposer();
    echo PHP_EOL;

    // Parse arguments
    if (empty($params)) {
        endScript('ERROR : you must provide the name of the app to create.', ERR_ARGS);
    }
    // Checking
    $askedForReset = false;
    $toCheck = array(
        $callPath . '/src',
        $callPath . '/conf',
        $callPath . '/locale',
        $callPath . '/views',
        $callPath . '/static'
    );
    foreach ($toCheck as $check) {
        if (file_exists($check)) {
            if (!$askedForReset) {
                echo 'Folder ' . $check . ' already exists. Do you want to ERASE ALL and RESET? [no]: ';
                $answer = strtolower(trim(fgets(STDIN)));
                if (isYes($answer)) {
                    $askedForReset = true;
                } else {
                    endScript('Aborting...', ERR_EXISTS);
                }
            }
            \PlayHP\IO\FileSystem::clearDir($check);
        }
    }

    $appName = ucfirst(preg_replace('/[^a-zA-Z0-9_-]*/', '', array_shift($params)));
    $rewriteBase = null;
    if (!empty($params)) {
        // If part override
        $rewriteBase = preg_replace('~[^\/a-zA-Z0-9_-]*~', '', array_shift($params));
    } else {
        echo 'Define the RewriteBase directive or type enter to keep "/" []: ';
        $answer = preg_replace('~[^\/a-zA-Z0-9_-]*~', '', trim(fgets(STDIN)));
        if (empty($answer)) {
            $rewriteBase = '/';
        } else {
            $rewriteBase = (substr($answer, 0, 1) !== '/' ? '/' : '') . $answer;
            if (substr($rewriteBase, -1) != '/') {
                $rewriteBase .= '/';
            }
        }
    }

    trace('Creating app ' . $appName . ' with rewrite base to ' . $rewriteBase . PHP_EOL);

    // Copy structure
    $appStructureTplPath = "$dir/appStructure/";
    \PlayHP\IO\FileSystem::copyPath($appStructureTplPath, $callPath);

    // App name and namespace
    $mainSrcPath = "$callPath/src/main/$appName";
    $testSrcPath = "$callPath/src/test/${appName}Tests";

    rename("$callPath/src/main/YourApp/", $mainSrcPath);
    rename("$callPath/src/test/YourAppTests/", $testSrcPath);
    replaceInFile("$mainSrcPath/Controllers/HomeController.php", array('YourApp' => $appName));
    replaceInFile("$mainSrcPath/Models/User.php", array('YourApp' => $appName));
    replaceInFile("$testSrcPath/AppTest.php", array('YourAppTests' => $appName . 'Tests'));
    replaceInFile("$callPath/conf/application.xml", array('YourApp' => $appName, 'Salt' => sha1(time())));
    replaceInFile("$callPath/conf/routes.xml", array('YourApp' => $appName));
    replaceInFile("$callPath/composer.json", array('YourApp' => $appName));

    // RewriteBase directive
    if ($rewriteBase !== null) {
        $htaccessPath = "$callPath/.htaccess";
        replaceInFile($htaccessPath, array('RewriteBase /' => 'RewriteBase ' . $rewriteBase));
    }
    echo "Updating dependencies using composer... Please wait..." . PHP_EOL;
    exec('composer update --dev');
    endScript('App structure was created!');
}

/**
 * Checks for composer installation and installs it if it doesn't exist
 */
function checkForComposer()
{
    $exitCode = null;
    $composerPath = array();
    exec('which composer', $composerPath, $exitCode);
    if ($exitCode !== EXIT_OK) {
        echo 'Composer seems not to be installed. Install? [yes]: ';
        $answer = strtolower(trim(fgets(STDIN)));
        if ($answer === '' || isYes($answer)) {
            $output = array();
            exec('curl -sS https://getcomposer.org/installer | php', $output, $exitCode);
            $error = $exitCode !== EXIT_OK || count($output) !== 6;
            if (!$error) {
                echo "Composer is now installed! Check http://getcomposer.org for more information on this project." . PHP_EOL;
                exec('mv composer.phar /usr/local/bin/composer', $output, $exitCode);
                $error = ($exitCode !== EXIT_OK);
            }
            if ($error) {
                echo "Composer installation failed. Please refer to http://getcomposer.org for installing it." . PHP_EOL;
                return false;
            }
        } else {
            echo "Sure, no problem. You'll need composer to use PlayHP though." . PHP_EOL;
            return false;
        }
    }
    return true;
}

/**
 * Replaces text parts directly into a given file, and ends script if error
 * @param string $filePath File where replacement should take place
 * @param array $replacements List of replacements to do
 */
function replaceInFile($filePath, $replacements)
{
    $ctrlContents = file_get_contents($filePath);
    $ctrlContents = strtr($ctrlContents, $replacements);
    if (!file_put_contents($filePath, $ctrlContents)) {
        endScript('Error while changing the ' . $filePath . ' contents. No write access?', ERR_IO);
    }
}

<?php

define('P_MODE', 'dev');
date_default_timezone_set('Europe/Paris');

require __DIR__ . "/vendor/autoload.php";

\PlayHP\PlayHP::run();
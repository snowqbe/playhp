<?php
/**
 * Global, utility functions, PHP additions
 */


if (!defined('P_MODE')) {
    define('P_MODE', 'prod');
}
define('P_DEV', '0');
define('P_USER', '1');

// Root path
$currentDir = str_replace('\\', '/', __DIR__);
$currentDir = substr($currentDir, 0, strpos($currentDir, '/src'));

// Default paths
define('P_ROOT', $currentDir . '/');
define('P_SRC_PATH', P_ROOT . 'src/main/');
define('P_TEST_PATH', P_ROOT . 'src/test/');
define('P_LOCALE_PATH', P_ROOT . 'locale/');
define('P_VIEWS_PATH', P_ROOT . 'views/');
define('P_LAYOUTS_PATH', P_ROOT . 'views/layouts/');

// Cancel magic quotes: it sucks
if (get_magic_quotes_gpc() || get_magic_quotes_runtime()) {
    unslashArray($_GET);
    unslashArray($_POST);
    unslashArray($_COOKIE);
}

/**
 * Lists the subclasses of a given class
 * @param string $parent Parent class name
 * @return array List of subclasses
 */
function getSubclassesOf($parent)
{
    $result = array();
    foreach (get_declared_classes() as $class) {
        if (is_subclass_of($class, $parent))
            $result[] = $class;
    }
    return $result;
}

/**
 * Validates an email
 * @param string $email Email to validate
 * @return bool Status of email validation
 */
function isEmailValid($email)
{
    $atom = '[-a-z0-9!#$%&\'*+\\/=?^_`{|}~]';
    $domain = '([a-z0-9]([-a-z0-9]*[a-z0-9]+)?)';
    $regex = '/^' . $atom . '+(\.' . $atom . '+)*@(' . $domain . '{1,63}\.)+{2,63}$/i';
    return preg_match($regex, $email) == 1;
}

/**
 * Debugging function
 * @param $msg
 * @param bool $bufferingActive
 */
function trace($msg, $bufferingActive = false)
{
    \PlayHP\PlayHP::trace($msg, $bufferingActive);
}

/**
 * Recursive glob call.
 * Does not support flag GLOB_BRACE
 * @link http://php.net/manual/fr/function.glob.php
 */
function globRecursive($pattern, $flags = 0)
{
    $files = glob($pattern, $flags);
    foreach (glob(dirname($pattern) . '/*', GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {
        $files = array_merge($files, globRecursive($dir . '/' . basename($pattern), $flags));
    }
    return $files;
}

/**
 * Gettext shortcut for PlayHP strings
 * @param string $text Text to grab
 * @return string Localized text
 */
function _p($text)
{
    return dgettext(\PlayHP\PlayHP::GETTEXT_DOMAIN, $text);
}

/**
 * Returns a possible name based on the given filename and path.
 * If filename already exists in path, the function proposes a new filename with a counter.
 * Result can be retrieved with path in it or not.
 * @param string $filename The file name to test
 * @param string $path The path where the file should be
 * @param bool|string $get_as_path Flag telling to return the path with the new filename
 * @return string
 */
function getPossibleName($filename, $path, $get_as_path = true)
{
    if (substr($path, -1) == '/') {
        $path = substr($path, 0, strlen($path) - 1);
    }
    if (!file_exists("$path/$filename")) {
        return $get_as_path ? "$path/$filename" : $filename;
    }
    $i = 1;
    while (file_exists("$path/$filename")) {
        $i++;
        $res = pathinfo("$path/$filename");
        $filename = substr($res['basename'], 0, strrpos($res['basename'], '.')) . $i . '.' . $res['extension'];
    }
    return $get_as_path ? "$path/$filename" : $filename;
}


/**
 * Handles unslashing magic quoted elements
 */
function unslashArray(&$array)
{
    foreach ($array as $key => $val) {
        if (is_array($val))
            unslashArray($array[$key]);
        else
            $array[stripslashes($key)] = stripslashes($val);
    }
}

/**
 * @param string $key Key of the parameter to fetch
 * @param null $default Default value to use otherwise
 * @return mixed|null The parameter's value
 */
function mapParam($key, $default = null)
{
    return isset($_GET[$key]) ? $_GET[$key] : (isset($_POST[$key]) ? $_POST[$key] : $default);
}

/**
 * Implements default entities behavior as of PHP 5.4
 * @param string $toencode String to encode
 * @return string Encoded string (in UTF-8)
 */
function entities($toencode)
{
    return htmlentities($toencode, null, 'UTF-8');
}


/**
 * Returns a formatted messages based on an exception
 * @param Exception $e
 * @return string Formatted message
 */
function fmt(\Exception $e)
{
    return '<pre>' .
    $e->getMessage() . "\nIn" .
    $e->getFile() . ' at line ' . $e->getLine() . "\n" . $e->getTraceAsString() . '</pre>';
}


/**
 * Redirects response to a new URL using `header Location: $url`
 * @param string $url URL to redirect to
 */
function redirect($url)
{
    $redirect = 'Location: ' . filter_var($url, FILTER_SANITIZE_URL);
    header($redirect);
    exit;
}
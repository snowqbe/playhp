<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Capabilities;

/**
 * Browser and OS detection utility class
 * @package PlayHP\Capabilities
 */
class BrowserDetect
{

    public static function isWindows()
    {
        return strpos($_SERVER['HTTP_USER_AGENT'], 'Windows') !== false;
    }

    public static function isMac()
    {
        return strpos($_SERVER['HTTP_USER_AGENT'], 'Macintosh') !== false;
    }
}
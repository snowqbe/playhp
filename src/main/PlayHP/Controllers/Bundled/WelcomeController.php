<?php

namespace PlayHP\Controllers\Bundled;

use PlayHP\Controllers\Controller;

/**
 * Welcome controller for creating a default page
 */
class WelcomeController extends Controller
{

    /**
     * Default welcome controller
     */
    public function welcome()
    {
        return $this->render('welcome');
    }

}

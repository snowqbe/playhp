<?php

namespace PlayHP\Controllers;

use PlayHP\Controllers\Exceptions\ViewNotFoundException;
use PlayHP\Controllers\Rendering\Renderable;
use PlayHP\Controllers\Rendering\RenderingEngine;
use PlayHP\I18n\Locale;
use PlayHP\PlayHP;
use PlayHP\Routing\Exceptions\RenderException;
use PlayHP\Routing\HttpMethod;
use PlayHP\Routing\Route;
use PlayHP\Validation\ObjectValidator;

/**
 * Controller handling requests (route targets).
 * Subclass this class to create your own controller.
 * Targeting a controller's method is done in the conf/routes.xml configuration file.
 * Invoked controller is automatically added to the object pool, meaning you can use (at)Inject annotations to inject
 * instances of other objects from the pool.
 */
abstract class Controller
{
    /**
     * Validator used for form fields
     * @var \PlayHP\Validation\ObjectValidator
     */
    private $_validator;

    /**
     * @var array Collection of meta to add to HTML view
     */
    private $_metas = array();

    /**
     * @var array List of variables to add before rendering
     */
    protected $_viewVariables = array();

    /**
     * Rendering engine
     * @var RenderingEngine
     */
    public $renderingEngine;

    /**
     * Default page title
     * @var string
     */
    public $title = 'Welcome on PlayHP';

    /**
     * Current route, injected by PlayHP context
     * @Inject
     * @var Route
     */
    public $currentRoute;

    /**
     * Current locale, injected by PlayHP context
     * @Inject
     * @var Locale
     */
    public $locale;

    /**
     * Builds the controller
     */
    public function __construct()
    {
        $this->_validator = new ObjectValidator();
    }

    /**
     * Called before controller method invocation pass
     */
    public function preinvoke()
    {
    }

    /**
     * Called before rendering pass
     */
    public function prerender()
    {
    }

    /**
     * Called after rendering pass
     */
    public function postrender()
    {
    }

    /**
     * @return array List of HTML meta tags to add to the page rendering
     */
    public function getMetas()
    {
        return $this->_metas;
    }

    /**
     * Returns the view viewId to use for rendering
     * @param string $viewId The main view ID
     * @return string The view viewId to include
     */
    public function getViewFilePath($viewId)
    {
        return $this->searchFileInPath($viewId, ViewType::$page);
    }

    /**
     * Resolves a layout file path and returns it
     * @param string $layoutId Name of the layout
     * @return string Path to the resolved layout file
     */
    public function getLayoutFilePath($layoutId)
    {
        return $this->searchFileInPath($layoutId, ViewType::$layout);
    }

    /**
     * Resolves a part file path
     * @param string $partName Name of the part to find
     * @return string The resolved part file path
     */
    public function getPartFilePath($partName)
    {
        return $this->searchFileInPath($partName, ViewType::$part);
    }

    /**
     * Renders a view with given variables
     * @param string $viewId Name of the view viewId to use for rendering
     * @param null|array $viewVariables List of variables to forward to the view
     * @return Renderable Rendering class used by the framework for handling the rendering pass
     */
    protected function render($viewId, $viewVariables = null)
    {
        $invocation = new Renderable();
        $invocation->viewVariables = $viewVariables;
        $invocation->viewId = $viewId;
        $this->invokeRenderable($invocation);
    }

    /**
     * Renders a view and returns the rendered content
     * @param string $viewId The ID of the view to render
     * @param null|array $viewVariables List of variables to pass to the view
     * @return mixed|null|string Rendered content
     */
    protected function getContents($viewId, $viewVariables = null)
    {
        return $this->renderingEngine->renderResult($viewId, $viewVariables);
    }

    /**
     * Renders response as text
     * @param string $result Result to render in the view
     * @return Renderable Rendering class used by the framework for handling the rendering pass
     */
    protected function renderText($result)
    {
        $invocation = new Renderable();
        $invocation->rawResult = $result;
        $invocation->responseType = 'text/plain';
        $this->invokeRenderable($invocation);
    }

    /**
     * Renders response as JSON
     * @param * $result Result to render in the view
     * @return Renderable Rendering class used by the framework for handling the rendering pass
     */
    protected function renderJSON($result)
    {
        $invocation = new Renderable();
        $invocation->rawResult = json_encode($result);
        $invocation->responseType = 'application/json'; // As of RFC-4627
        $this->invokeRenderable($invocation);
    }

    /**
     * Redirects user to provided viewType
     * @param string $path Path to redirect to
     * @param null|array $viewVariables List of variables to pass to the vie
     * @return Renderable Invocation object
     */
    protected function forward($path, $viewVariables = null)
    {
        $invocation = new Renderable();
        $invocation->viewVariables = $viewVariables;
        if (substr($path, 0, 1) != '/') {
            $path = '/' . $path;
        }
        $invocation->redirection = $path;
        $this->invokeRenderable($invocation);
    }

    /**
     * @param Renderable $invocation
     * @throws RenderException The renderable object will be contained in the exception
     */
    private function invokeRenderable($invocation)
    {
        $invocation->viewVariables = $this->mergeViewVariables($invocation->viewVariables);
        throw new RenderException($invocation);
    }

    /**
     * Triggers a 500 error
     * @param null|string $message Error message
     */
    protected function error($message = null)
    {
        $invocation = new Renderable();
        $invocation->httpCode = 500;
        $invocation->rawResult = $message;
        $this->invokeRenderable($invocation);
    }

    /**
     * Triggers a 404 page
     * @param null|string $message Error message
     */
    protected function notFound($message = null)
    {
        $invocation = new Renderable();
        $invocation->httpCode = 404;
        $invocation->rawResult = $message;
        $this->invokeRenderable($invocation);
    }

    /**
     * Triggers a 403 error
     * @param null|string $message Error message
     */
    protected function forbidden($message = null)
    {
        $invocation = new Renderable();
        $invocation->httpCode = 403;
        $invocation->rawResult = $message;
        $this->invokeRenderable($invocation);
    }

    /**
     * Adds a meta tag for the HTML header
     * @param string $metaName Name of the meta tag to add
     * @param string $metaContent Content of the meta tag
     */
    protected function setMeta($metaName, $metaContent)
    {
        $this->_metas[$metaName] = $metaContent;
    }

    /**
     * Resolves a file in the application's domain with a failover in PlayHP's domain if it's not found
     * @param string $viewId View name
     * @param ViewType $viewType Type of view to look for
     * @return string The view path file
     * @throws Exceptions\ViewNotFoundException When no file was found in either place
     */
    protected function searchFileInPath($viewId, ViewType $viewType)
    {
        $appViewFile = APP_VIEWS_PATH . $viewType->value . 's/' . $viewId . '.php';
        if (file_exists($appViewFile)) {
            return $appViewFile;
        }
        $appViewFile = P_VIEWS_PATH . $viewType->value . 's/' . $viewId . '.php';
        if (file_exists($appViewFile)) {
            return $appViewFile;
        }
        throw new ViewNotFoundException($viewId, $viewType);
    }

    /**
     * Validates a given object (usually fetched from the request) against defined validation annotations
     * @param null|array $profiles List of profiles to activate for this validation
     * @return array List of validation errors
     */
    protected function validate($profiles = null)
    {
        list(, $lastCall) = debug_backtrace();
        return $this->_validator->validateMethodCall($lastCall, $profiles);
    }

    /**
     * Validates a given object (usually fetched from the request) against defined validation annotations
     * @param mixed $object Object to validate, with validation annotations
     * @param null|array $profiles List of profiles to activate for this validation
     * @return array List of validation errors
     */
    protected function validateObject($object, $profiles = null)
    {
        if ($object === null) {
            list(, $lastCall) = debug_backtrace();
            return $this->_validator->validateMethodCall($lastCall, $profiles);
        }
        return $this->_validator->validateObject($object, $profiles);
    }

    /**
     * Shortcut to reverse a URL from given class method and parameters
     * @param string $method Class method to target
     * @param null|array $params The list of parameters to use for the reverse
     * @param null|string $class The class provided method belongs to
     * @return string The reversed URL
     */
    protected function reverse($method, $params = null, $class = null)
    {
        $r = PlayHP::router();
        if ($class === null) {
            $class = get_called_class();
        }
        return HTTP_BASE . $r->reverse($class, $method, $params);
    }

    /**
     * Merges view variables
     * @param array|null $viewVariables
     * @return array Merged view variables
     */
    public function mergeViewVariables($viewVariables)
    {
        if ($viewVariables === null) {
            $viewVariables = $this->_viewVariables;
        } else if ($this->_viewVariables !== null) {
            $viewVariables = array_merge($viewVariables, $this->_viewVariables);
        }
        return $viewVariables;
    }

    /**
     * @return HttpMethod The current HTTP Method
     */
    protected function getMethod()
    {
        switch ($_SERVER['REQUEST_METHOD']) {
            case 'POST':
                return HttpMethod::$POST;
            case 'GET':
                return HttpMethod::$GET;
            case 'DELETE':
                return HttpMethod::$DELETE;
            case 'PUT':
                return HttpMethod::$PUT;
        }
        return null;
    }
}

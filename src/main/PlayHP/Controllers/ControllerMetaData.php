<?php

namespace PlayHP\Controllers;

use PlayHP\Routing\Route;

/**
 * Metadata for controller classes
 */
class ControllerMetaData
{

    /**
     * @var Route[]
     */
    public $routes;

    /**
     * @var \ReflectionClass
     */
    public $class;

    /**
     * @var string
     */
    public $id;

    /**
     * Controller layout. Defaults to main
     * @var string
     */
    public $defaultLayout;

}

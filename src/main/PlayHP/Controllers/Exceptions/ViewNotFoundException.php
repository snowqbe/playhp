<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Controllers\Exceptions;

use Exception;
use PlayHP\Controllers\ViewType;

/**
 * Class ViewNotFoundException
 * Triggered when a view has not been found
 * @package PlayHP\Controllers\Exceptions
 */
class ViewNotFoundException extends Exception
{

    /**
     * @private
     * @var string Name of the view which was not found
     */
    private $_view;

    /**
     * @private
     * @var string Type of the view which was not found
     */
    private $_viewType;

    /**
     * @param string $view Name of the view which was not found
     * @param ViewType $viewType Type of the view which was not found
     * @param string $message Additionnal message
     * @param int $code Error code
     * @param Exception $previous Previous exception
     */
    public function __construct($view, $viewType, $message = "", $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->_view = $view;
        $this->_viewType = $viewType;
    }

    /**
     * @return string Name of the view which was not found
     */
    public function getView()
    {
        return $this->_view;
    }

    /**
     * @return string Type of the view which was not found
     */
    public function getViewType()
    {
        return $this->_viewType;
    }


}
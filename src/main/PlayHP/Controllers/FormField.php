<?php

namespace PlayHP\Controllers;

use PlayHP\Lang\Annotation;
use PlayHP\PlayHP;

/**
 * Form field
 */
class FormField
{

    /**
     * Parses given class form fields
     * @param \ReflectionClass|\ReflectionMethod $reflect Class info
     * @param array $initValues Initial values for the fields
     * @internal param string $className
     * @return FormField[] List of form fields
     */
    public static function parseFields($reflect, $initValues = array())
    {
        $fields = array();
        if (is_a($reflect, 'ReflectionClass')) {
            // Parse fields from a class
            $props = $reflect->getProperties();
            $fields = array();
            foreach ($props as $prop) {
                $field = new FormField();
                if ($field->parseProperty($prop)) {
                    self::addField($fields, $field, $initValues);
                }
            }
        } else if (is_a($reflect, 'ReflectionMethod')) {
            // Parse fields from a method
            $annotations = Annotation::parseAnnotations($reflect->getDocComment(), 'param');
            if (count($annotations) > 0) {
                // Field has comment
                foreach ($annotations as $annotation) {
                    switch ($annotation->name) {
                        case 'param':
                            $field = new FormField();
                            foreach ($annotation->params as $i => $paramValue) {
                                if (substr($paramValue, 0, 1) == '$') {
                                    $field->name = substr($paramValue, 1);
                                    $field->htmlName = $field->name;
                                } else if ($i == 0) {
                                    $field->flagType($paramValue);
                                }
                            }
                            if (!empty($annotation->subAnnotations)) {
                                $fieldAnnotation = current($annotation->subAnnotations);
                                $field->parseAnnotation($fieldAnnotation);
                            }
                            self::addField($fields, $field, $initValues);
                            break;
                    }
                }
            } else {
                $methodParams = $reflect->getParameters();
                /* @var \ReflectionProperty $methodParam */
                foreach ($methodParams as $methodParam) {
                    $field = new FormField();
                    $field->name = $methodParam->getName();
                    $field->htmlName = $methodParam->getName();
                    $field->setFlags(FormFlag::$String);
                    self::addField($fields, $field, $initValues);
                }

            }
        }
        return $fields;
    }

    /**
     * @private
     * Updates a fields collection with a new form field, initializing its value using the $initialValues array
     * @param array $fields Collection to update with new field
     * @param FormField $field Field to add to collection
     * @param array $initValues Initial values for fields
     */
    private static function addField(&$fields, $field, $initValues)
    {
        if (array_key_exists($field->htmlName, $initValues)) {
            $field->setValue($initValues[$field->htmlName]);
        }
        $fields[$field->htmlName] = $field;
    }

    /**
     * @var FormFlag[]
     */
    private $_flags = array();

    /**
     * Field name
     * @var string
     */
    public $name;

    /**
     * Source to use for filling the form field's value
     * @var FormFieldSource
     */
    public $source;

    /**
     * HTML code to use when rendering the field, allowing it to be fetched from its source
     * @var string
     */
    public $htmlName;

    /**
     * Field's value
     * @var mixed
     */
    private $_value;

    /**
     * Field name this field should be equal to
     * @var string
     */
    private $_equalsTo;

    /**
     * List of fields embedded in an object's for field
     * @var FormField[]
     */
    public $objectFields;

    /**
     * Constructor
     * @param string $src Source to use for filling the form field's value
     * @see #FormFieldSource
     * @see #source
     */
    public function __construct($src = 'REQUEST')
    {
        $this->source = FormFieldSource::parse($src);
    }

    /**
     * @param string $varTypesAsString Variable types as a string
     * @return mixed
     */
    public function flagType($varTypesAsString)
    {
        $varTypes = explode('|', trim($varTypesAsString));

        foreach ($varTypes as $varType) {
            switch (strtolower(trim($varType))) {
                case 'array':
                    $this->setFlags(FormFlag::$Array);
                    break;
                case 'pass':
                case 'password':
                    $this->setFlags(FormFlag::$String, FormFlag::$Password);
                    break;
                case 'email':
                    $this->setFlags(FormFlag::$String, FormFlag::$Email);
                    break;
                case 'null':
                case 'mixed':
                case 'string':
                    $this->setFlags(FormFlag::$String);
                    break;
                case 'boolean':
                case 'bool':
                    $this->setFlags(FormFlag::$Bool);
                    break;
                case 'integer':
                case 'int':
                case 'float':
                case 'double':
                case 'long':
                    $this->setFlags(FormFlag::$Numeric);
                    break;
                case '\simplexmlelement':
                case 'simplexmlelement':
                case 'xml':
                    $this->setFlags(FormFlag::$Object, FormFlag::$Xml);
                    break;
                case '\stdclass':
                case 'stdclass':
                case 'object':
                case 'json':
                    $this->setFlags(FormFlag::$Object, FormFlag::$Json);
                    break;
                default:
                    try {
                        echo '<pre>';
                        debug_print_backtrace();
                        echo '</pre>';
                        if (substr($varType, -2) == '[]') {
                            $varType = substr($varType, 0, -2);
                            $this->setFlags(FormFlag::$Array);
                        }
                        $reflect = PlayHP::classLoder()->getReflectionClass($varType);
                        $this->objectFields = FormField::parseFields($reflect);
                        $this->setFlags(FormFlag::$Object);
                    } catch (\Exception $exc) {
                        PlayHP::error("Form field $varType could not be parsed");
                    }
                    break;
            }
        }
    }

    /**
     * Parses provided comment for form field flags
     * @param string $comment Comment to parse for flags
     * @return bool Tells if property is a form field or not
     */
    public function parseFlags($comment)
    {
        $isField = false;
        $annotations = Annotation::parseAnnotations($comment);
        if (empty($annotations)) {
            return false;
        }
        foreach ($annotations as $annotation) {
            if (is_string($annotation)) {
                return false;
            }
            switch ($annotation->name) {
                default:
                    $flag = FormFlag::parse($annotation->name);
                    if ($flag !== null) {
                        $this->_flags[] = $flag;
                    }
                    break;
                case "Field":
                    $isField = true;
                    $this->parseAnnotation($annotation);
                    break;
                case "Name":
                    $this->htmlName = $annotation->params[0];
                    break;
                case "var":
                    $isField = true;
                    $varType = $annotation->params[0];
                    $this->flagType($varType);
                    break;
            }
        }
        return $isField;
    }

    /**
     * Initializes the field using its metadata
     * @param Annotation $prop Annotation to parse for initializing form field
     */
    public function parseAnnotation(Annotation $prop)
    {
        if ($prop->params === null) {
            return;
        }
        foreach ($prop->params as $key => $value) {
            switch (strtolower($key)) {
                case 'type':
                    $this->flagType($value);
                    break;
                case 'name':
                    $this->htmlName = $value;
                    break;
                case 'required':
                    if ($value == 'true' || $value == 'required') {
                        $this->_flags[] = FormFlag::$Required;
                    }
                    break;
                case 'equals':
                    $this->_flags[] = FormFlag::$Equals;
                    $this->_equalsTo = $value;
                    break;
            }
        }
    }

    /**
     * Initializes the field using its metadata
     * @param \ReflectionProperty $prop Property to parse for initializing form field
     * @return bool Tells if parsing revealed to be a field or not
     */
    public function parseProperty(\ReflectionProperty $prop)
    {
        $comment = $prop->getDocComment();
        $isField = $this->parseFlags($comment);
        if ($isField) {
            $this->name = $prop->getName();
            if ($this->htmlName === null) {
                $this->htmlName = $this->name;
            }
            return true;
        }
        return false;
    }

    /**
     * Validates the form field
     * @param string $pageId ID of the page using the field
     * @return bool|string TRUE if valid, Error message otherwise
     */
    public function validate($pageId)
    {
        $this->fillValue($pageId);
        $error = null;
        if ($this->flagged(FormFlag::$Required) && ($this->_value === null || strval($this->_value) == '')) {
            $error = _p("This field is required");
        } else if ($this->flagged(FormFlag::$Email) && !isEmailValid($this->_value)) {
            $error = _p("This email is incorrect");
        } else if ($this->flagged(FormFlag::$Numeric) && intval($this->_value) != $this->_value) {
            $error = _p("This field should be numeric");
        }
        if ($error === null) {
            return TRUE;
        }
        return $error;
    }

    /**
     * Checks for a flag
     * @param FormFlag $flag Flag to check
     * @return bool
     */
    public function flagged(FormFlag $flag)
    {
        return in_array($flag, $this->_flags);
    }

    /**
     * Fills the form field with its current value
     */
    private function fillValue($pageId)
    {
        $tab = null;
        switch ($this->source) {
            default:
            case FormFieldSource::$REQUEST:
                $tab = $_REQUEST;
                break;
            case FormFieldSource::$GET:
                $tab = $_GET;
                break;
            case FormFieldSource::$POST:
                $tab = $_POST;
                break;
        }
        $key = $pageId . '_form_' . $this->htmlName;
        $this->_value = isset($tab[$key]) ? $tab[$key] : null;
    }

    public function getSanitizedValue()
    {
        return htmlspecialchars(strval($this->_value), null, 'UTF-8');
    }

    public function getHtmlType()
    {
        if ($this->flagged(FormFlag::$Password)) {
            return "password";
        } else if ($this->flagged(FormFlag::$Bool)) {
            return "checkbox";
        }
        return "text";
    }

    /**
     * @param FormFlag $flag Flag to add to the field
     */
//    private function addFlag(FormFlag $flag)
//    {
//        if (!in_array($flag, $this->_flags)) {
//            if ($flag)
//            $this->_flags[] = $flag;
//        }
//    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->_value = $value;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        if ($this->flagged(FormFlag::$Numeric)) {
            return intval($this->_value);
        } else if ($this->flagged(FormFlag::$Bool)) {
            $compare = strtolower(strval($this->_value));
            return $compare === 'true' || $compare === '1';
        } else if ($this->flagged(FormFlag::$Json)) {
            return json_decode($this->_value);
        } else if ($this->flagged(FormFlag::$Xml)) {
            try {
                $ret = @simplexml_load_string($this->_value);
                if ($ret !== false) {
                    return $ret;
                }
            } catch (\Exception $e) {
                // TODO: handle exception?
            }
        }
        return $this->_value;
    }

    /**
     * Dynamically resets flags using passed arguments
     */
    private function setFlags()
    {
        $this->trimTypeFlags();
        $args = func_get_args();
        foreach ($args as $arg) {
            if (is_a($arg, '\PlayHP\Controllers\FormFlag') && !in_array($arg, $this->_flags)) {
                $this->_flags[] = $arg;
            }
        }
    }

    private function trimTypeFlags()
    {
        $result = array();
        foreach ($this->_flags as $flag) {
            if ($flag == FormFlag::$Required ||
                $flag == FormFlag::$Email ||
                $flag == FormFlag::$Array ||
                $flag == FormFlag::$Equals
            ) {
                $result[] = $flag;
            }
        }
        $this->_flags = $result;
    }
}

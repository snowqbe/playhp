<?php

namespace PlayHP\Controllers;

use PlayHP\Lang\Enum;

/**
 *
 */
class FormFieldSource extends Enum
{
    public static $GET;
    public static $POST;
    public static $REQUEST;
}

FormFieldSource::init();

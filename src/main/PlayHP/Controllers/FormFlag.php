<?php

namespace PlayHP\Controllers;

use PlayHP\Lang\Enum;

/**
 * Form field flags
 */
class FormFlag extends Enum
{
    public static $Password;
    public static $Required;
    public static $Email;
    public static $Equals;

    public static $Json;
    public static $Xml;
    public static $Object;
    public static $String;
    public static $Numeric;
    public static $Bool;
    public static $Array;
}

FormFlag::init();


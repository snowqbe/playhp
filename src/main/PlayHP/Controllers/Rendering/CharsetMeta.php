<?php

namespace PlayHP\Controllers\Rendering;

/**
 * Simple charset meta
 */
class CharsetMeta extends Meta
{

    /**
     * Defines the charset to use for the meta
     * @param string $charset Page charset
     */
    function __construct($charset = 'utf-8')
    {
        // TODO: check charset from ISO collection
        parent::__construct(null, null, null, $charset);
    }

}

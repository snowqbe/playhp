<?php

namespace PlayHP\Controllers\Rendering;

/**
 * Link extension dedicated to CSS inclusion for convenient stuff like conditionnal inclusion (IE, resolutions...)
 */
class Css extends Link
{

    /**
     * @var string
     */
    private $_ieCondition;

    /**
     * Media query min width
     * @var int
     */
    private $_minWidth;

    /**
     * Media query max width
     * @var int
     */
    private $_maxWidth;


    /**
     * @param string $href Link to the CSS
     * @param CssTarget $target The CSS's target
     * @param string $media Default media
     * @param string $hreflang Lang code
     * @param string $sizes Sizes
     */
    function __construct($href, $target = null, $media = 'all', $hreflang = null, $sizes = null)
    {
        parent::__construct($href, 'stylesheet', 'text/css', $media, $hreflang, $sizes);
        $this->setTarget($target);
    }

    /**
     * @param CssTarget $target
     */
    public function setTarget($target)
    {
        switch ($target) {
            case CssTarget::$LargeScreens:
                $this->_maxWidth = null;
                $this->_minWidth = 1200;
                break;
            case CssTarget::$Tablets:
                $this->_maxWidth = 979;
                $this->_minWidth = 768;
                break;
            case CssTarget::$Smartphones:
                $this->_maxWidth = 767;
                $this->_minWidth = null;
                break;
            default:
                break;
        }
    }

    /**
     * @param string $value IE conditionnal inclusion. Eg: '<= 8', '= 9', '> 6'...
     */
    public function setIECondition($value)
    {
        $this->_ieCondition = $value;
    }

    /**
     * @return string
     */
    public function getHTML()
    {
        if ($this->_minWidth !== null || $this->_maxWidth !== null) {
            $mediaQuery = 'screen';
            if ($this->_minWidth !== null) {
                $mediaQuery .= ' and (min-width: ' . $this->_minWidth . 'px)';
            }
            if ($this->_maxWidth !== null) {
                $mediaQuery .= ' and (max-width: ' . $this->_maxWidth . 'px)';
            }
            $this->setAttribute('media', $mediaQuery);
        }
        $standardCode = parent::getHTML();
        if ($this->_ieCondition !== null) {
            return '<!--[if IE ' . entities($this->_ieCondition) . ' ]>' . $standardCode . '<![endif]-->';
        }
        return $standardCode;
    }


}

<?php

namespace PlayHP\Controllers\Rendering;

use PlayHP\Lang\Enum;

/**
 * List of possible rendering targets for a CSS
 */
class CssTarget extends Enum
{

    public static $LargeScreens;
    public static $Tablets;
    public static $Smartphones;
    public static $Desktop;
}

CssTarget::init();

<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Controllers\Rendering\Flavors;

/**
 * Twitter bootstrap flavor implementation.
 * Renders elements using Twitter bootstrap classes and common usage
 *
 * @package PlayHP\Controllers\Rendering\Flavors
 */
class BootstrapFlavor extends Flavor
{

    function formInput($type, $name, $label, $value)
    {
        // TODO: Implement formInput() method.
    }

    function formSubmit($name, $label)
    {
        // TODO: Implement formSubmit() method.
    }

    function formReset($label)
    {
        // TODO: Implement formReset() method.
    }

    function errorsBlock($errors)
    {
        // TODO: Implement errorsBlock() method.
    }

    function success($message)
    {
        // TODO: Implement success() method.
    }

    function alert($message)
    {
        // TODO: Implement alert() method.
    }
}
<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Controllers\Rendering\Flavors;

/**
 * Default HTML flavor implementation
 * @package PlayHP\Controllers\Rendering\Flavors
 */
class DefaultFlavor extends Flavor
{

    /**
     * Renders an input field
     * @param string $type HTML input type attribute
     * @param string $name HTML input name attribute
     * @param string $label Label for the input field
     * @param string $value Initial value of the input field
     * @return string HTML code for the input field
     */
    function formInput($type, $name, $label, $value)
    {
        // TODO: Implement formInput() method.
    }

    /**
     * Provides a form submit button HTML code
     * @param string $name Name for the input attribute
     * @param string $label Label of the submit button
     * @return string The HTML code to render in the page
     */
    function formSubmit($name, $label)
    {
        // TODO: Implement formSubmit() method.
    }

    /**
     * Provides a form reset button HTML code
     * @param string $label Label of the reset button
     * @return string The HTML code to render in the page
     */
    function formReset($label)
    {
        // TODO: Implement formReset() method.
    }

    /**
     * Displays an error block to the user
     * @param array $errors List of errors to show
     * @return string HTML code to render
     */
    function errorsBlock($errors)
    {
        // TODO: Implement errorsBlock() method.
    }

    /**
     * Renders a success message to the user
     * @param string $message Success message
     * @return string HTML code to render
     */
    function success($message)
    {
        // TODO: Implement success() method.
    }

    /**
     * Renders an alert message to the user
     * @param string $message Alert message
     * @return string HTML code to render
     */
    function alert($message)
    {
        // TODO: Implement alert() method.
    }
}
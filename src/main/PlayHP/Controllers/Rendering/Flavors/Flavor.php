<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Controllers\Rendering\Flavors;


/**
 * Flavor are meant to hold basic HTML rendering according to a client-side framework like, for example, Twitter bootstrap.
 * It's been abstracted to help standardize the rendering of forms, tables, etc... using your favorite toolkit for
 * quick app prototyping.
 *
 * @package PlayHP\Controllers\Rendering\Flavors
 */
abstract class Flavor
{

    /**
     * Renders an input field
     * @param string $type HTML input type attribute
     * @param string $name HTML input name attribute
     * @param string $label Label for the input field
     * @param string $value Initial value of the input field
     * @return string HTML code for the input field
     */
    abstract function formInput($type, $name, $label, $value);

    /**
     * Provides a form submit button HTML code
     * @param string $name Name for the input attribute
     * @param string $label Label of the submit button
     * @return string The HTML code to render in the page
     */
    abstract function formSubmit($name, $label);

    /**
     * Provides a form reset button HTML code
     * @param string $label Label of the reset button
     * @return string The HTML code to render in the page
     */
    abstract function formReset($label);

    /**
     * Displays an error block to the user
     * @param array $errors List of errors to show
     * @return string HTML code to render
     */
    abstract function errorsBlock($errors);

    /**
     * Renders a success message to the user
     * @param string $message Success message
     * @return string HTML code to render
     */
    abstract function success($message);

    /**
     * Renders an alert message to the user
     * @param string $message Alert message
     * @return string HTML code to render
     */
    abstract function alert($message);

}
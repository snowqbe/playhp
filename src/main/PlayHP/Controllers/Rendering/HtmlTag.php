<?php

namespace PlayHP\Controllers\Rendering;

/**
 *
 */

use PlayHP\PlayHP;

class HtmlTag
{

    /**
     * @var string
     */
    public $tagName;

    /**
     * @var string
     */
    public $tagContent;

    /**
     * @var array
     */
    private $_attributes = array();

    function __construct()
    {
    }

    /**
     * @param string $name Attribute name
     * @param string $value Attribute value
     */
    public function setAttribute($name, $value)
    {
        if ($name === null || $value === null) {
            return;
        }
        if ($name == 'href' || $name == 'src') {
            $value = PlayHP::router()->url($value);
        }
        $this->_attributes[entities($name)] = entities($value);
    }

    /**
     * @return string
     */
    public function getHTML()
    {
        $code = '<' . $this->tagName;
        foreach ($this->_attributes as $attrName => $val) {
            $code .= ' ' . $attrName . '="' . $val . '"';
        }
        if ($this->tagContent !== null) {
            $code .= '>' . $this->tagContent . '</' . $this->tagName;
        }
        $code .= '>';
        return $code;
    }

    /**
     * @param string $attributeName Name of the attribute to check
     * @return bool true if tag has attribute named $attributeName
     */
    public function hasAttribute($attributeName)
    {
        return array_key_exists($attributeName, $this->_attributes);
    }

    /**
     * @param string $attributeName The attribute's name
     * @return string The attribute's associated value
     */
    public function getAttribute($attributeName)
    {
        if (!$this->hasAttribute($attributeName)) {
            return null;
        }
        return $this->_attributes[$attributeName];
    }
}

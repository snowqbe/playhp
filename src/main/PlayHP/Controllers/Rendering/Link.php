<?php

namespace PlayHP\Controllers\Rendering;

/**
 * Link HTML element
 */
class Link extends HtmlTag
{

    function __construct($href, $rel = 'stylesheet', $type = 'text/css', $media = 'all', $hreflang = null, $sizes = null)
    {
        $this->tagName = 'link';
        $this->setAttribute('href', $href);
        $this->setAttribute('rel', $rel);
        $this->setAttribute('type', $type);
        $this->setAttribute('media', $media);
        $this->setAttribute('hreflang', $hreflang);
        $this->setAttribute('sizes', $sizes);
    }
}

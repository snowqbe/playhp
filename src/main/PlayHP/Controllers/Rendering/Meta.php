<?php

namespace PlayHP\Controllers\Rendering;

/**
 * HTML Meta representation
 */
class Meta extends HtmlTag
{

    /**
     * @var string Meta name
     */
    public $name;

    /**
     * @var string Meta http-equiv
     */
    public $httpEquiv;

    /**
     * @var string Meta content
     */
    public $content;

    /**
     * @var string Meta charset
     */
    public $charset;

    /**
     * @param string $name Default name
     * @param string $content
     * @param string $httpEquiv Default HTTP equivalent
     * @param string $charset Default charset
     */
    function __construct($name = null, $content = null, $httpEquiv = null, $charset = null)
    {
        $this->tagName = 'meta';
        $this->setAttribute('name', $name);
        $this->setAttribute('http-equiv', $httpEquiv);
        $this->setAttribute('content', $content);
        $this->setAttribute('charset', $charset);
    }
}

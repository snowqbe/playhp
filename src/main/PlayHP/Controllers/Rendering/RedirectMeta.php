<?php

namespace PlayHP\Controllers\Rendering;

/**
 * Simple client redirect meta
 */
class RedirectMeta extends Meta
{

    /**
     * @param int $timeout Time to wait before redirecting
     * @param string $url URL to redirect user to
     */
    function __construct($timeout, $url)
    {
        parent::__construct(null, intval($timeout) . '; url=' . $url, 'refresh');
    }

}

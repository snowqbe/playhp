<?php

namespace PlayHP\Controllers\Rendering;

use PlayHP\PlayHP;
use PlayHP\Routing\Route;

/**
 * Represents a controller's method return value
 */
class Renderable
{

    /**
     * @var int HTTP code of the response (if error, not found, etc. ...)
     */
    public $httpCode = 200;

    /**
     * Route path used to redirect current user
     * @var string
     */
    public $redirection;

    /**
     * ID of the view file to use for rendering
     * @var string
     */
    public $viewId;

    /**
     * Variables to define in the view context
     * @var null|array
     */
    public $viewVariables;

    /**
     * Result to render directly as a response
     * @var *
     */
    public $rawResult;

    /**
     * @var \PlayHP\Routing\Route
     */
    public $route;

    /**
     * Type of response when rendering the raw result
     * @var string
     * @see #rawResult
     */
    public $responseType = "text/html";

    /**
     * Tells to return the result as string instead of outputing it in the response
     * @var bool
     */
    public $returnResult;

    /**
     * @return bool
     */
    public function isRaw()
    {
        return $this->rawResult !== null;
    }

    /**
     * Renders the renderable object
     * @param RenderingEngine $layout RenderingEngine to use for rendering
     */
    public function render(RenderingEngine $layout)
    {
        if ($this->isRaw()) {
            header("Content-type: $this->responseType");
            echo $this->rawResult;
            return;
        }
        if ($this->viewId === null) {
            PlayHP::error("No view file passed in $this");
        }
        $layout->render($this->viewId, $this->viewVariables);
    }
}

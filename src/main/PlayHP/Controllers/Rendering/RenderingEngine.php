<?php

namespace PlayHP\Controllers\Rendering;

use PlayHP\Controllers\Controller;
use PlayHP\Controllers\Exceptions\ViewNotFoundException;
use PlayHP\Controllers\Rendering\Flavors\DefaultFlavor;
use PlayHP\Controllers\Rendering\Flavors\Flavor;
use PlayHP\Controllers\Rendering\Meta;
use PlayHP\Controllers\Rendering\View;
use PlayHP\PlayHP;
use PlayHP\Security\SecurityService;

/**
 * RenderingEngine class in charge of the main rendering.
 * TODO: document more
 */
class RenderingEngine
{

    // ------------------------------------------------------------------------------------------------------------
    // CONSTANTS
    // ------------------------------------------------------------------------------------------------------------

    /**
     * Variable storing current view name (ideal for menus)
     */
    const VAR_VIEW = 'pview';

    /**
     * Variable storing currently authenticated user if any
     */
    const VAR_USER = 'puser';

    /**
     * Flavor variable passed to the view
     */
    const VAR_FLAVOR = 'f';

    /**
     * Title variable
     */
    const VAR_TITLE = 'title';

    // ------------------------------------------------------------------------------------------------------------
    // PRIVATE PROPERTIES
    // ------------------------------------------------------------------------------------------------------------

    /**
     * @var View
     */
    private $_view;

    /**
     * @var SecurityService Security service
     */
    private $_securityService;

    /**
     * @var array HTML meta set by the controller
     */
    private $_controllerMetas;

    /**
     * @var string File name, no beginning slash, no php extension
     */
    private $_layoutFile;

    /**
     * Main file to render
     * @var string
     */
    private $_viewFile;

    /**
     * List of view variables provided by the controller
     * @var array
     */
    private $_viewVariables = array();

    /**
     * List of included parts during the rendering pass
     * @var array
     */
    private $_renderedParts = array();

    /**
     * @var \PlayHP\Controllers\Controller
     */
    private $_controller;

    /**
     * Current layout flavor if any
     * @Inject
     * @var Flavor
     */
    public $flavor;

    // ------------------------------------------------------------------------------------------------------------
    // PUBLIC PROPERTIES
    // ------------------------------------------------------------------------------------------------------------

    /**
     * @Inject
     * @var \PlayHP\Routing\Route
     */
    public $currentRoute;

    /**
     * Renders the layout
     * @param string $viewFile File to include for rendering
     * @param array $viewVariables Variables to define
     * @return void
     */
    public function render($viewFile, $viewVariables)
    {
        $this->doRender($viewFile, $viewVariables, true);
    }

    /**
     * Updates the layout file during the rendering process
     * @param string $layoutFile The layout file to use for rendering the view
     */
    public function useLayout($layoutFile)
    {
        $this->_layoutFile = $layoutFile;
    }

    /**
     * @Inject
     * @param Controller $controller
     */
    public function setController(Controller $controller)
    {
        $this->_controller = $controller;
    }

    /**
     * @param null|string $partName The name of the part to render
     * @param bool $required Tells the part is required and triggers an error if not found
     */
    public function renderPart($partName, $required = false)
    {
        if (array_key_exists($partName, $this->_renderedParts)) {
            // Second pass, don't process the part again
            echo $this->_renderedParts[$partName];
            return;
        }
        // First pass, process the part and index contents
        if ($partName === null) {
            // Main body part is required
            try {
                $currentFilePath = $this->_controller->getViewFilePath($this->_viewFile);
            } catch (ViewNotFoundException $e) {
                PlayHP::error("Page view file was not found: " . $e->getView());
                return;
            }
        } else {
            try {
                // Try page override
                $currentFilePath = $this->_controller->getPartFilePath($partName . '_' . $this->_viewFile);
            } catch (ViewNotFoundException $e) {
                try {
                    // Try regular part
                    $currentFilePath = $this->_controller->getPartFilePath($partName);
                } catch (ViewNotFoundException $e) {
                    // No luck. Fail if required, don't display anything otherwise
                    if ($required) {
                        PlayHP::error('Part not found: ' . $e->getView());
                    }
                    return;
                }
            }
        }
        // Define variables for the template
        $this->_view->render($this->_viewVariables, $currentFilePath);
    }

    /**
     * @Inject
     * @param SecurityService $service Security service
     */
    public function setSecurityService($service)
    {
        $this->_securityService = $service;
    }

    /**
     * Initializes the default layout flavor to use
     */
    function __construct()
    {
        $this->flavor = new DefaultFlavor();
    }

    /**
     * @param Meta[] $metas List of meta tags to add
     */
    public function setMetas($metas)
    {
        $this->_controllerMetas = $metas;
    }

    /**
     * Returns a rendered view based on provided parameters
     * @param string $viewId View ID to render
     * @param null|array $viewVariables List of view variables to use while rendering
     * @return mixed|null|string The rendered content
     */
    public function renderResult($viewId, $viewVariables)
    {
        return $this->doRender($viewId, $viewVariables, false);
    }

    /**
     * Renders a given view
     * @param string $viewFile
     * @param null|array $viewVariables The list of view variables to pass to the view
     * @param bool $echo Should we echo the result or return it
     * @return mixed|null|string The rendered content, or null if already echoed
     */
    private function doRender($viewFile, $viewVariables, $echo)
    {
        if (is_array($viewVariables)) {
            $this->_viewVariables = array_merge($viewVariables, $this->_viewVariables);
        }
        $this->_viewFile = $viewFile;
        try {
            $mainFile = $this->_controller->getViewFilePath($viewFile);
        } catch (ViewNotFoundException $e) {
            return PlayHP::error('Page view not found: ' . $e->getView(), P_DEV, 500, $echo);
        }

        // Define view variables
        if (!array_key_exists(self::VAR_TITLE, $this->_viewVariables)) {
            $this->_viewVariables[self::VAR_TITLE] = $this->_controller->title;
        }
        $this->_viewVariables[self::VAR_FLAVOR] = $this->flavor;
        $this->_viewVariables[self::VAR_VIEW] = $this->_viewFile;
        $this->_viewVariables[self::VAR_USER] = $this->_securityService ? $this->_securityService->getAuthenticatedUser() : null;

        $this->_view = new View($this);

        // First pass initializing layout parameters, we hence don't use buffer output right away
        ob_start();
        $this->_view->render($this->_viewVariables, $mainFile);
        $originalOutput = ob_get_clean();

        // Store contents in cache for final output
        $this->_renderedParts[null] = $originalOutput;

        // Did main page define a layout file?
        if ($this->_layoutFile !== null) {
            ob_start();
            try {
                $layoutFile = $this->_controller->getLayoutFilePath($this->_layoutFile);
                $this->_view->render($this->_viewVariables, $layoutFile); // Original content will be processed as a $this->renderPart() call from the layout file
            } catch (ViewNotFoundException $e) {
                // OB is active
                echo PlayHP::error('Layout not found: ' . $e->getView(), P_DEV, 500, $echo);
            }
            $originalOutput = ob_get_clean();
        }
        if ($echo) {
            // No valid layout file defined, echo the original output
            echo $originalOutput;
        } else {
            return $originalOutput;
        }
        return null;
    }

    /**
     * Defines a view variable for rendering
     * @param string $varName The variable name to define
     * @param mixed $value The value to set
     */
    public function setViewVariable($varName, $value)
    {
        $this->_viewVariables[$varName] = $value;
    }
}

<?php

namespace PlayHP\Controllers\Rendering;

/**
 * Script tag
 */
class Script extends HtmlTag
{

    /**
     * List of scripts this script depends on
     * @var array
     */
    public $dependencies = null;

    /**
     * Name of this script
     * @var string
     */
    public $name;

    /**
     * Builds the script tag
     * @param string $name Name of the script
     * @param string $src Source of the script
     * @param string|null $content Contents of the script
     * @param string $type Type of the script (defaults to JS)
     */
    function __construct($name, $src, $content = null, $type = 'text/javascript')
    {
        $this->name = $name;
        $this->tagName = 'script';
        if ($src !== null) {
            $this->setAttribute('src', $src);
        }
        $this->setAttribute('type', $type);
        $this->tagContent = $content;
    }

    /**
     * Handles the singularity of the script tag
     * @return string HTML code for rendering the script tag
     */
    public function getHTML()
    {
        $code = parent::getHTML();
        if ($this->tagContent === null) {
            $code .= '</script>';
        }
        return $code;
    }

}

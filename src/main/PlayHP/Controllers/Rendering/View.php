<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Controllers\Rendering;

use PlayHP\Controllers\Rendering\Exception\ScriptOverrideException;
use PlayHP\Controllers\Rendering\RenderingEngine;
use PlayHP\PlayHP;

/**
 * Class holding view files inclusion.
 * Provides helper functions to simplify rendering
 * @package PlayHP\Controllers\Rendering
 */
class View
{
    // ------------------------------------------------------------------------------------------------------------
    // CONSTANTS
    // ------------------------------------------------------------------------------------------------------------

    /**
     * Separates two lines of HTML code in the header
     */
    const LINE_SEP = "\n    ";

    // ------------------------------------------------------------------------------------------------------------
    // PRIVATE PROPERTIES
    // ------------------------------------------------------------------------------------------------------------

    /**
     * Rendering engine
     * @var \PlayHP\Controllers\Rendering\RenderingEngine
     */
    private $_engine;

    /**
     * @var Meta[]
     */
    private $_metadatas;

    /**
     * List of CSS files to load.
     * CSS shall be loaded in the head of the HTML document
     * @var Link[]
     */
    private $_css = array();

    /**
     * List of scripts to include.
     * key = name of a list of scripts to include in the HTML document.
     * @var Script[]
     * @see #getScriptHtml
     */
    private $_scripts = array();

    // ------------------------------------------------------------------------------------------------------------
    // PUBLIC PROPERTIES
    // ------------------------------------------------------------------------------------------------------------

    /**
     * Title for the HTML page
     * @var string
     */
    public $title;

    /**
     * @param RenderingEngine $engine The main rendering engine orchestrating the output
     */
    function __construct(RenderingEngine $engine)
    {
        $this->_engine = $engine;
    }

    /**
     * Scopes the inclusion of a file in this class to avoid conflicting with the main rendering engine
     * @param array $viewVariables The list of variables to pass to the view
     * @param string $viewFile
     */
    public function render($viewVariables, $viewFile)
    {
        if (is_array($viewVariables)) {
            extract($viewVariables);
        }
        include($viewFile);
    }


    /**
     * Updates the layout file during the rendering process
     * @param string $layoutFile The layout file to use for rendering the view
     */
    public function useLayout($layoutFile)
    {
        $this->_engine->useLayout($layoutFile);
    }

    /**
     * Renders the main body part of the page.
     */
    public function renderBody()
    {
        $this->renderPart(null);
    }

    /**
     * @param null $partName The name of the part to render
     * @param bool $required Tells the part is required and triggers an error if not found
     */
    public function renderPart($partName, $required = false)
    {
        $this->_engine->renderPart($partName, $required);
    }

    /**
     * Configures a metadata for layout usage
     */
    private function addMeta(Meta $meta)
    {
        $this->_metadatas[] = $meta;
    }

    /**
     * Registers for a script in the page
     * @param Script $scriptTag JS script tag
     * @throws ScriptOverrideException
     */
    private function registerScript(Script $scriptTag)
    {
        if (array_key_exists($scriptTag->name, $this->_scripts)) {
            $existingScript = $this->_scripts[$scriptTag->name];
            if ($scriptTag->hasAttribute('src') != $existingScript->hasAttribute('src')) {
                throw new ScriptOverrideException('Script tag named ' . $scriptTag->name . ' already exists and its attributes don\'t match the override you\'re trying to make.');
            }
            if ($existingScript->hasAttribute('src')) {
                // Override source
                $existingScript->setAttribute('src', $scriptTag->getAttribute('src'));
            } else {
                // Override content
                $existingScript->tagContent .= PHP_EOL . $scriptTag->tagContent;
            }
            // Merge dependencies
            $existingScript->dependencies = array_merge($existingScript->dependencies, $scriptTag->dependencies);
            return;
        }
        if (!empty($scriptTag->dependencies)) {
            // Detect cycles
            $hasCycle = $this->detectCycles($scriptTag->name, $scriptTag);
            if ($hasCycle) {
                PlayHP::error('A cyclic dependency has been detected for this script\'s dependency ' . $scriptTag->name);
            }
        }
        $this->_scripts[$scriptTag->name] = $scriptTag;
    }

    /**
     * Registers a CSS as a link element
     * @param Link $cssLink Link element to add
     */
    private function registerLink(Link $cssLink)
    {
        $this->_css[] = $cssLink;
    }

    /**
     * @return string The HTML code for metadata
     */
    public function getMetaAsHtml()
    {
        if (!empty($this->_controllerMetas)) {
            foreach ($this->_controllerMetas as $name => $content) {
                $this->addMeta(new Meta($name, $content));
            }
        }
        if (empty($this->_metadatas)) {
            return '';
        }
        $code = '';
        foreach ($this->_metadatas as $meta) {
            $code .= $meta->getHTML() . self::LINE_SEP;
        }
        return $code;
    }

    /**
     * @return string The HTML code for registered CSS
     */
    public function getCssAsHtml()
    {
        $code = '';
        foreach ($this->_css as $cssLink) {
            $code .= $cssLink->getHTML() . self::LINE_SEP;
        }
        return $code;
    }

    /**
     * Renders a collection of JS script inclusions
     * @return string The HTML code for including the scripts in your page
     */
    public function getScriptAsHtml()
    {
        $generatedScripts = array();
        $toGenerate = $this->_scripts;
        $code = '';
        /* @var Script $scriptTag */
        while (count($toGenerate) > 0) {
            $scriptTag = array_shift($toGenerate);
            $generate = true;
            if (!empty($scriptTag->dependencies)) {
                foreach ($scriptTag->dependencies as $dependency) {
                    if (!in_array($dependency, $generatedScripts)) {
                        $generate = false;
                        break;
                    }
                }
            }
            if ($generate) {
                $code .= $scriptTag->getHTML() . self::LINE_SEP;
                $generatedScripts[] = $scriptTag->name;
            } else {
                // Back in the queue
                $toGenerate[$scriptTag->name] = $scriptTag;
            }
        }
        return $code;
    }

    /**
     * Renders the scripts in the page
     */
    public function renderScripts()
    {
        echo $this->getScriptAsHtml();
    }

    /**
     * @return string The HTML code for metadata
     */
    public function renderHeader()
    {
        $code = '<title>' . entities($this->title) . '</title>' . self::LINE_SEP;
        $code .= $this->getMetaAsHtml() . self::LINE_SEP;
        $code .= $this->getCssAsHtml();
        echo $code;
    }

    /**
     * Adds a redirect meta
     * @param int $timeout Redirection timeout
     * @param string $url Redirection URL
     */
    public function metaRedirect($timeout, $url)
    {
        $this->addMeta(new RedirectMeta($timeout, $url));
    }

    /**
     * Adds a generic meta tag
     * @param string $name Meta name
     * @param string $content Meta content
     * @param string|null $httpEquiv HTTP equivalent (optional)
     */
    public function meta($name, $content, $httpEquiv = null)
    {
        $this->addMeta(new Meta($name, $content, $httpEquiv));
    }

    /**
     * Adds the charset meta tag
     * @param string $charset Charset to use
     */
    public function charset($charset)
    {
        $this->addMeta(new CharsetMeta($charset));
    }

    /**
     * Registers a new script by its source
     * @param string $name Name of this script (for further dependencies reference)
     * @param string $src URI of the script
     * @param array $dependencies List of script's dependencies
     */
    public function scriptSrc($name, $src, $dependencies = array())
    {
        $src = $this->getStaticSrc($src);
        $scriptTag = new Script($name, $src);
        $scriptTag->dependencies = $dependencies;
        $this->registerScript($scriptTag);
    }

    /**
     * Registers a new script by its content
     * @param string $name Name of this script (for further dependencies reference)
     * @param string $content Script content
     * @param array $dependencies List of script's dependencies
     */
    public function script($name, $content, $dependencies = array())
    {
        $script = new Script($name, null, $content);
        $script->dependencies = $dependencies;
        $this->registerScript($script);
    }

    /**
     * Adds a link in the header
     * @param string $href Href attribute
     * @param string $rel Rel attribute
     * @param string $type Type attribute
     */
    public function addLink($href, $rel, $type)
    {
        $this->registerLink(new Link($href, $rel, $type));
    }

    /**
     * @param string $href URI of the CSS file to add
     * @param null $ieCondition Condition for Internet Explorer browsers
     * @param null|CssTarget $target Target (Desktop, Smartphone, Tablets, Large screens...). Use null for any
     */
    public function css($href, $ieCondition = null, $target = null)
    {
        $href = $this->getStaticSrc($href);
        $cssLink = new Css($href, $target);
        if ($ieCondition !== null) {
            $cssLink->setIECondition($ieCondition);
        }
        $this->registerLink($cssLink);
    }

    /**
     * Returns a resolved URL using the router
     * @param string $pathOrController Relative path (route) or controller to reverse (in this case you must provide the method parameter)
     * @param string $method Controller method to reverse
     * @param null $methodParams Method parameter for reverse mode
     * @return string URL to use
     */
    public function href($pathOrController, $method = null, $methodParams = null)
    {
        $router = PlayHP::router();
        if ($method !== null) {
            $path = $router->reverse($pathOrController, $method, $methodParams);
            return $router->url($path);
        }
        return $router->url($pathOrController);
    }

    /**
     * @private
     * Detects any Javascript cycle in dependencies to avoid deadlock
     * @param string $name Script name
     * @param Script $scriptTag Script tag to test
     * @return bool true if there is a cycle
     */
    private function detectCycles($name, $scriptTag)
    {
        if (empty($scriptTag->dependencies)) {
            return false;
        }
        foreach ($scriptTag->dependencies as $dependency) {
            if (strtolower($dependency) === $name) {
                return true;
            }
            if (array_key_exists($dependency, $this->_scripts) && $this->detectCycles($name, $this->_scripts[$dependency])) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns an URL you can use in AJAX
     * @param string $controller Controller to invoke
     * @param string $method Method from the controller to invoke
     * @return string The JS code to use for querying the controller using AJAX
     */
    public function ajaxURL($controller, $method)
    {
        $router = PlayHP::router();
        $template = $router->getURLTemplate($controller, $method);
        if ($template === null) {
            return null;
        }
        $hasParameters = !empty($template->routeParameters) || !empty($template->methodParams);
        if (empty($template->routeParameters)) {
            $jsRouteParameters = '[]';
        } else {
            $jsRouteParameters = '[\'' . implode('\',\'', $template->routeParameters) . '\']';
        }
        $url = BASE . $template->routePath;
//        if (!$hasParameters) {
//            $jsCode = "function(){return '$url';}";
//        } else {
        $jsCode =
            <<<EOL
function(argsObject, method) {
            var pathParams=$jsRouteParameters;
            var url='$url';
            var qs='';
            var params={};
            if (method == undefined) {
                method = 'POST';
            }
            for (var paramName in argsObject) {
                if (pathParams.indexOf(paramName) >= 0) {
                    url = url.replace('{' + paramName + '}', argsObject[paramName]);
                }
                else {
                    if (method == 'POST') {
                        params[paramName] = argsObject[paramName];
                    }
                    else {
                        qs += (qs == '' ? '?' : '&amp;') + encodeURIComponent(paramName) + '=' + encodeURIComponent(argsObject[paramName]);
                    }
                }
            }
            return {"url": url + qs, "params": params};
        }
EOL;
//        }
        return $jsCode;
    }

    /**
     * @param string $src Source to prefix with static base if necessary
     * @return string Update source
     */
    private function getStaticSrc($src)
    {
        if ($src !== null && strstr($src, '://', true) !== 'http') {
            $src = STATIC_BASE . $src;
            return $src;
        }
        return $src;
    }
}
<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Controllers;

use PlayHP\Lang\Enum;

/**
 * List of view types
 * @package PlayHP\Controllers
 */
class ViewType extends Enum
{

    /**
     * Page view type
     * @var ViewType
     */
    public static $page;

    /**
     * Part view type
     * @var ViewType
     */
    public static $part;

    /**
     * Layout view type
     * @var ViewType
     */
    public static $layout;

}

ViewType::init();
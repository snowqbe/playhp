<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\DB\Adapters;

use PlayHP\DB\DBAdapter;

class OracleAdapter implements DBAdapter
{
    private $defaultSchema;

    public function getEscapeChar($start = true)
    {
        return '';
    }

    public function supportsSequence()
    {
        return true;
    }

    public function supportsAutoIncrement()
    {
        return false;
    }

    /**
     * @return string The keyword to use in a query for the current date (NOW in oracle, GETDATE, DATE, etc. ...)
     */
    public function getNow()
    {
        return 'sysdate';
    }

    /**
     * @return string The name of the default schema to use if relevant
     */
    public function getDefaultSchema()
    {
        return $this->defaultSchema;
    }

    /**
     * @param string $schema Default schema name
     */
    public function setDefaultSchema($schema)
    {
        $this->defaultSchema = $schema;
    }

    /**
     * @param \DateTime $dateTime Date time object to format to string
     * @return string Formatted date time
     */
    public function formatDateTime(\DateTime $dateTime)
    {
        return $dateTime->format('Y-M-d h:i:s');
    }

    /**
     * @param string $sequenceName Name of the sequence
     * @throws \PlayHP\DB\DBAdapterException If sequences are not supported
     * @return string The SQL query to use for fetching next val from given sequence
     */
    public function getNextValQuery($sequenceName)
    {
        return 'SELECT {schema}' . $sequenceName . '.nextval from dual;';
    }

    /**
     * @param string $sequenceName Name of the sequence
     * @throws \PlayHP\DB\DBAdapterException If sequences are not supported
     * @return string The SQL query
     */
    public function getCurrValQuery($sequenceName)
    {
        return 'SELECT {schema}' . $sequenceName . '.currval from dual;';
    }

    /**
     * @return bool true if DBMS supports auto commit, false otherwise
     */
    public function supportsAutoCommit()
    {
        return true;
    }
}
<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\DB;

/**
 * DBMS adapter for handling platform specific features
 * @package PlayHP\DB
 */
interface DBAdapter
{

    /**
     * Used to escape table or column names in queries
     * @param bool $start true for the beginning of the word to escape, false for the end (MSSQL...)
     * @return string The corresponding escape character
     */
    public function getEscapeChar($start = true);

    /**
     * @return bool true if the DBMS supports sequences (Oracle, Postgresql...), false otherwise (SQL Server, MySQL...)
     */
    public function supportsSequence();

    /**
     * @param string $sequenceName Name of the sequence
     * @throws \PlayHP\DB\DBAdapterException If sequences are not supported
     * @return string The SQL query to use for fetching next val from given sequence
     */
    public function getNextValQuery($sequenceName);

    /**
     * @param string $sequenceName Name of the sequence
     * @throws \PlayHP\DB\DBAdapterException If sequences are not supported
     * @return string The SQL query
     */
    public function getCurrValQuery($sequenceName);

    /**
     * @return bool true if the DBMS supports auto increment (SQL Server, MySQL...), false otherwise (Oracle, Postgresql...)
     */
    public function supportsAutoIncrement();

    /**
     * @return bool true if DBMS supports auto commit, false otherwise
     */
    public function supportsAutoCommit();

    /**
     * @return string The keyword to use in a query for the current date (NOW in oracle, GETDATE, DATE, etc. ...)
     */
    public function getNow();

    /**
     * @return string The name of the default schema to use if relevant
     */
    public function getDefaultSchema();

    /**
     * @param \DateTime $dateTime Date time object to format to string
     * @return string Formatted date time
     */
    public function formatDateTime(\DateTime $dateTime);
}
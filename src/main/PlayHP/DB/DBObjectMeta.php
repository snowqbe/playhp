<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\DB;
use PlayHP\Lang\Annotation;
use PlayHP\Lang\ClassMeta;
use PlayHP\PlayHP;
use ReflectionClass;


/**
 * Represents DB mapping metadata
 * @package PlayHP\DB
 */
class DBObjectMeta
{

    /**
     * Table name
     * @var string
     */
    public $tableName;

    /**
     * Mapped class name
     * @var string
     */
    public $className;

    /**
     * List of properties and corresponding DB information
     * @var DBProperty[]
     */
    public $propertiesMap;

    /**
     * @var string
     * Clause to use in SQL queries
     */
    public $pkClause = '';

    /**
     * @var int
     * Number of primary keys detected
     */
    public $nbPK;

    /**
     * @return array List of SQL column based on metadata
     */
    public function getSQLColumns()
    {
        $columns = array();
        foreach ($this->propertiesMap as $propMetadata) {
            $columns[] = $propMetadata->columnName;
        }
        return $columns;
    }

    /**
     * @return DBProperty The found property if mapping defines only one primary key column, null otherwise
     */
    public function getUniqueId()
    {
        $result = null;
        $count = 0;
        foreach ($this->propertiesMap as $prop) {
            if ($prop->isInPK) {
                $count++;
                $result = $prop;
            }
        }
        if ($count == 1) {
            return $result;
        }
        return null;
    }

    /**
     * @param string $filterProperty Name of the property
     * @return string Name of corresponding column
     */
    public function getColumn($filterProperty)
    {
        foreach ($this->propertiesMap as $propMetaData) {
            if ($propMetaData->propertyName == $filterProperty) {
                return $propMetaData->columnName;
            }
        }
        return null;
    }

    /**
     * @param string $filterProperty Name of the property
     * @return DBProperty Requested DB properties
     */
    public function getDBProperty($filterProperty)
    {
        foreach ($this->propertiesMap as $propMetaData) {
            if ($propMetaData->propertyName == $filterProperty) {
                return $propMetaData;
            }
        }
        return null;
    }

    /**
     * Parses DB object meta data
     * @param string|ReflectionClass|mixed $modelClassName Class name or reflection class to parse
     * @return DBObjectMeta Parsed meta data
     */
    public static function getMeta($modelClassName)
    {
        $classLoader = PlayHP::classLoder();
        if (is_a($modelClassName, '\ReflectionClass')) {
            $reflect = $modelClassName;
            $className = $reflect->name;
        } else if (is_object($modelClassName)) {
            $className = get_class($modelClassName);
            $reflect = $classLoader->getReflectionClass($className);
        } else {
            $className = (string)$modelClassName;
            $reflect = $classLoader->getReflectionClass($className);
        }
        $classMeta = $classLoader->getMeta(ClassMeta::$DB, $className);
        if ($classMeta !== null) {
            return $classMeta;
        }

        // Handle inheritance
        $currentReflect = $reflect;
        /* @var \PlayHP\DB\DBObjectMeta[] $parentMetas */
        $parentMetas = array();
        while ($currentReflect->getParentClass() !== false) {
            array_unshift($parentMetas, self::getMeta($currentReflect->getParentClass()));
            $currentReflect = $currentReflect->getParentClass();
        }

        $tableName = null;
        $propertiesMap = array();
        if (!empty($parentMetas)) {
            foreach ($parentMetas as $parentMeta) {
                // Merge to keep the most up to date elements
                $propertiesMap = array_merge($propertiesMap, $parentMeta->propertiesMap);
                $tableName = $parentMeta->tableName;
            }
        }
        $classMeta = new DBObjectMeta();
        // Class level annotation for table name
        $classMeta->tableName = Annotation::getAnnotationDefaultParam($reflect->getDocComment(), 'Table', 'name');
        $classMeta->className = $reflect->getName();
        if ($classMeta->tableName === null) {
            if ($tableName === null) {
                // No annotation here nor on parent class
                return null;
            }
            // Defaults to inherited table name
            $classMeta->tableName = $tableName;
        }

        // Current class definition parsing
        $nbPK = 0;
        $classMeta->propertiesMap = $propertiesMap;
        /* @var \ReflectionProperty[] $props */
        $props = $reflect->getProperties();
        foreach ($props as $prop) {
            $propAnnotations = Annotation::parseAnnotations($prop->getDocComment(), array('Id', 'Column', 'var', 'Fetch'));
            if ($propAnnotations === null) {
                continue;
            }
            $metaData = new DBProperty();
            $metaData->propertyName = $prop->name;
            // ID column
            foreach ($propAnnotations as $propAnnotation) {
                switch ($propAnnotation->name) {
                    case 'Fetch':
                        // Fetchable relation
                        $metaData->fetchQuery = $propAnnotation->getDefaultParam('sql');
                        break;
                    case 'Id':
                        // Id column info
                        $metaData->columnName = $propAnnotation->getDefaultParam('name', $prop->name);
                        $metaData->isInPK = true;
                        $metaData->isAutoIncremented = $propAnnotation->getOptionalParam('autoIncrement', false) == 'true';
                        $metaData->nullable = false;
                        $metaData->sequenceName = $propAnnotation->getOptionalParam('sequenceName');
                        $nbPK++;
                        break;
                    case 'Column':
                        // Regular column info
                        $metaData->columnName = $propAnnotation->getDefaultParam('name', $prop->name);
                        $metaData->columnType = $propAnnotation->getOptionalParam('type');
                        $metaData->columnLength = intval($propAnnotation->getOptionalParam('length'));
                        $metaData->isAutoIncremented = $metaData->isAutoIncremented || $propAnnotation->getOptionalParam('autoIncrement', false) == 'true';
                        $metaData->nullable = $propAnnotation->getOptionalParam('nullable', 'true') == 'true';
                        break;
                    case 'var':
                        // Type was not explicitly defined, get it from var type
                        if (!$propAnnotation->hasParams()) {
                            // TODO: log error ? guess generic type like binary or something?
                            break;
                        }
                        $metaData->propertyType = $propAnnotation->params[0];
                        if ($metaData->columnType === null) {
                            $metaData->columnType = $propAnnotation->params[0];
                        }
                        break;
                }
            }
            // PK clause template
            if ($metaData->isInPK) {
                $classMeta->pkClause .= empty($classMeta->pkClause) ? '' : ' AND ';
                $classMeta->pkClause .= $metaData->columnName . ' = :' . $metaData->propertyName;
            }
            if ($metaData->columnName !== null) {
                $classMeta->propertiesMap[$metaData->propertyName] = $metaData;
            }
        }
        $classMeta->nbPK = $nbPK;
        $classLoader->setMeta(ClassMeta::$DB, $className, $classMeta);
        return $classMeta;
    }

    /**
     * @return bool true if object has defined a primary key, false otherwise
     */
    public function hasPK()
    {
        foreach ($this->propertiesMap as $columnMeta) {
            if ($columnMeta->isInPK) {
                return true;
            }
        }
        return false;
    }


    /**
     * Returns the parameters to use in an SQL query using only primary key in the where clause
     * @param mixed $model Model instance
     * @return array PDO parameters array containing only primary keys
     */
    public function getPKParams($model)
    {
        $meta = self::getMeta($model);
        $params = array();
        foreach ($meta->propertiesMap as $propName => $metaData) {
            if ($metaData->isInPK) {
                $classVar = $propName;
                $params[':' . $classVar] = $model->$classVar;
            }
        }
        return $params;
    }
}
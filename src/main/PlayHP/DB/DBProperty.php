<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\DB;

/**
 * Metadata information for a mapping between a database column and a class property
 *
 * @package PlayHP\DB
 */
class DBProperty
{

    /**
     * @var boolean
     * Tells whether the column is in the primary key or not
     */
    public $isInPK;

    /**
     * @var string
     * Name of the database column
     */
    public $columnName;

    /**
     * @var string
     * Type of the database column
     */
    public $columnType;

    /**
     * @var string
     * Type of the object property
     */
    public $propertyType;

    /**
     * @var int
     * Length of the database column
     */
    public $columnLength;

    /**
     * @var string
     * Name of the DB property
     */
    public $propertyName;

    /**
     * @var string
     * Name of the sequence to use when auto generating column values
     */
    public $sequenceName;

    /**
     * @var bool
     * Flag telling the property can be null
     */
    public $nullable;

    /**
     * @var bool
     * Flag indicating if column is auto incremented
     */
    public $isAutoIncremented;

    /**
     * @var string
     * The SQL query to fetch the property (used for relationships)
     */
    public $fetchQuery;

}
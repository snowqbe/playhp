<?php

namespace PlayHP\DB;

use PDO;
use PDOException;
use PlayHP\DB\Adapters\MSSQLAdapter;
use PlayHP\DB\Adapters\MysqlAdapter;
use PlayHP\DB\Adapters\OracleAdapter;
use PlayHP\DB\Adapters\PostgresAdapter;
use PlayHP\DB\Adapters\SqliteAdapter;
use PlayHP\Lang\Annotation;
use PlayHP\PlayHP;

/**
 * Base class for easy database access
 */
class DBService
{
    /**
     * @var bool
     */
    private $_inTransaction;

    /**
     * Query parser used internally
     * @var QueryParser
     */
    private $_parser;

    /**
     * @var DBAdapter
     */
    private $_adapter;

    /**
     * PDO connection
     * @var PDO
     */
    public $pdo;

    /**
     * Creates the service and connects to given database
     * @param string $dsn DSN to connect to the database
     * @param string $user Database user
     * @param string $pass Password
     */
    public function __construct($dsn, $user, $pass)
    {
        try {
            $testDsn = strtolower($dsn);
            if (strpos($testDsn, 'mysql:') !== false) {
                $this->_adapter = new MysqlAdapter();
            } else if (strpos($testDsn, 'pgsql:') !== false) {
                $this->_adapter = new PostgresAdapter();
            } else if (strpos($testDsn, 'sqlite:') !== false) {
                $this->_adapter = new SqliteAdapter();
            } else if (strpos($testDsn, 'sqlsrv:') !== false) {
                $this->_adapter = new MSSQLAdapter();
            } else if (strpos($testDsn, 'oci:') !== false) {
                $this->_adapter = new OracleAdapter();
            }
            $this->_parser = new QueryParser($this->_adapter);
            $this->pdo = new PDO($dsn, $user, $pass);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            if ($this->_adapter->supportsAutoCommit()) {
                // Disable autocommit for handling executions within transactions
                $this->pdo->setAttribute(PDO::ATTR_AUTOCOMMIT, false);
            }
        } catch (PDOException $e) {
            $this->logPdoError($e);
        }
    }

    /**
     * Begins a new transaction
     * @param bool $strict true to throw an exception when a transaction is already in progress
     * @throws DBTransactionException When, in strict mode, a transaction is already in progress
     */
    public function beginTransaction($strict = false)
    {
        if ($this->_inTransaction) {
            // Already in transaction
            if ($strict) {
                throw new DBTransactionException('A transaction is already in progress');
            }
        }
        $this->_inTransaction = $this->pdo->beginTransaction();
    }

    /**
     * @return bool Commit status OK or KO
     */
    public function commit()
    {
        $result = false;
        if ($this->_inTransaction) {
            $result = $this->pdo->commit();
            $this->_inTransaction = false;
        }
        return $result;
    }

    /**
     * @return bool Rollback status OK or KO
     */
    public function rollback()
    {
        $result = false;
        if ($this->_inTransaction) {
            $result = $this->pdo->rollBack();
            $this->_inTransaction = false;
        }
        return $result;
    }

    /**
     * @return DBAdapter The DBMS adapter used by the service
     */
    public function getAdapter()
    {
        return $this->_adapter;
    }

    /**
     * Prepares an SQL query for execution
     * @param string $sql SQL query to use for preparing the statement
     * @return \PDOStatement Created statement
     */
    protected function prepareStatement($sql)
    {
        $this->replaceSchema($sql);
        try {
            $q = $this->pdo->prepare($sql);
            $this->checkForError();
            return $q;
        } catch (\PDOException $e) {
            $this->logPdoError($e);
        }
        return null;
    }

    /**
     * Removes provided Model from database
     * @param mixed $dbObject Model to delete
     * @return int Number of removed rows
     */
    public function delete($dbObject)
    {
        $meta = DBObjectMeta::getMeta($dbObject);
        $sql = 'DELETE FROM {schema}' . $meta->tableName . ' WHERE ' . $meta->pkClause;
        return $this->executeInTransaction($sql, $meta->getPKParams($dbObject));
    }


    /**
     * Increments given sequence and returns the generated number
     * @param string $sequenceName Sequence to use
     * @return int Next ID to use
     */
    public function getNextSequenceValue($sequenceName)
    {
        try {
            $q = $this->prepareStatement($this->getAdapter()->getNextValQuery($sequenceName));
            $this->checkForError();
            $nextId = $q->fetch(\PDO::FETCH_COLUMN);
            return $nextId;
        } catch (\PDOException $e) {
            $this->logPdoError($e);
        }
        return null;
    }

    /**
     * Retrieves a given sequence value
     * @param string $sequenceName Sequence to query
     * @return int Current ID of the sequence
     */
    public function getCurrentSequenceValue($sequenceName)
    {
        try {
            $q = $this->prepareStatement($this->getAdapter()->getCurrValQuery($sequenceName));
            $this->checkForError();
            $currentId = $q->fetch(\PDO::FETCH_COLUMN);
            return $currentId;
        } catch (\PDOException $e) {
            $this->logPdoError($e);
        }
        return null;
    }

    /**
     * Inserts a Model in database
     * @param mixed $dbObject Object to insert
     * @return int 1 if INSERT was OK, 0 otherwise
     * @throws DBAdapterException If adapter does not support the ID generation mechanism described in Model's metadata
     */
    public function insert($dbObject)
    {
        $params = array();
        $meta = DBObjectMeta::getMeta($dbObject);

        // Single column in PK check
        $idColMetaData = $meta->getUniqueId();
        if ($idColMetaData !== null) {
            // ID column or equivalent
            if ($idColMetaData->sequenceName !== null && !$idColMetaData->isAutoIncremented && !$this->_adapter->supportsSequence()) {
                throw new DBAdapterException('Adapter does not support sequences although mapping defines one. Unable to insert');
            } else if ($idColMetaData->isAutoIncremented && !$this->_adapter->supportsAutoIncrement()) {
                throw new DBAdapterException('Adapter does not support auto increment although no sequence has been defined. Unable to insert');
            }
        }
        $i = 0;
        $colClause = '';
        $valClause = '';
        $autoIncrementedProp = null;
        foreach ($meta->propertiesMap as $propName => $colMetaData) {
            $columnName = $colMetaData->columnName;
            $paramValue = $dbObject->$propName;
            if ($paramValue === null) {
                // Auto generation of column value
                if ($this->_adapter->supportsAutoIncrement() && $colMetaData->isAutoIncremented && $colMetaData->isInPK && $meta->nbPK == 1) {
                    $autoIncrementedProp = $colMetaData->propertyName;
                    continue;
                }
                if ($this->_adapter->supportsSequence() && $colMetaData->sequenceName !== null) {
                    $paramValue = $this->getNextSequenceValue($colMetaData->sequenceName);
                    $dbObject->$propName = $paramValue;
                }
            }
            list(, $realValue) = $this->_parser->handleParam($paramValue);
            $params[':' . $columnName] = $realValue;
            $colClause .= ($i > 0 ? ", " : "") . $this->_adapter->getEscapeChar(true) . $columnName . $this->_adapter->getEscapeChar(false);
            $valClause .= ($i++ > 0 ? ", :" : ":") . $columnName;
        }
        $sql = 'INSERT INTO {schema}' . $meta->tableName . ' (' . $colClause . ') VALUES (' . $valClause . ')';
        $result = $this->executeInTransaction($sql, $params);
        if ($autoIncrementedProp !== null) {
            $generatedId = $this->pdo->lastInsertId();
            $dbObject->$autoIncrementedProp = $generatedId;
        }
        // TODO: insert dependencies annotated with @Insert
        return $result;
    }

    /**
     * Updates an object in database
     * @param mixed $dbObject Object to update
     * @return int 1 if update succeeded, 0 otherwise
     */
    public function update($dbObject)
    {
        $meta = DBObjectMeta::getMeta($dbObject);
        $sql = "UPDATE {schema}" . $meta->tableName . " SET ";
        $i = 0;
        $params = $meta->getPKParams($dbObject);
        foreach ($meta->propertiesMap as $propName => $columnMetaData) {
            $columnName = $columnMetaData->columnName;
            if ($columnMetaData->isInPK) {
                continue;
            }
            $paramValue = $dbObject->$propName;
            list(, $realValue) = $this->_parser->handleParam($paramValue);
            $params[':' . $columnName] = $realValue;
            $sql .= ($i++ > 0 ? ", " : "") . $this->_adapter->getEscapeChar(true) . $columnName . $this->_adapter->getEscapeChar(false) . ' = :' . $columnName;
        }
        $sql .= ' WHERE ' . $meta->pkClause;
        $result = $this->executeInTransaction($sql, $params);
        // TODO: update dependencies using @Delete and @Insert annotations
        return $result;
    }

    /**
     * Executes the SQL query (INSERT, UPDATE or DELETE) and returns the number of affected rows
     * @param string $sql SQL query to execute
     * @param array $params Parameters for the query (optional)
     * @return int Number of affected rows
     */
    public function execute($sql, $params = null)
    {
        $this->replaceSchema($sql);
        try {
            $q = $this->pdo->prepare($sql);
            $this->checkForError();
            $q->execute($params);
            $this->checkForError();
            return $q->rowCount();
        } catch (PDOException $e) {
            $this->rollback();
            $this->logPdoError($e);
        }
        return null;
    }

    /**
     * Executes the SQL query (SELECT) and returns the result
     * @param string $sql SQL query to execute
     * @param array $params Parameters for the query (optional)
     * @param callable|null $handler Handler to call for mapping each row
     * @return array The query result to use for mapping
     */
    public function sqlSelect($sql, $params = null, $handler = null)
    {
        $this->replaceSchema($sql);
        try {
            $q = $this->pdo->prepare($sql);
            $this->checkForError();
            $q->execute($params);
            if ($handler == null) {
                $results = $q->fetchAll(PDO::FETCH_ASSOC);
            } else {
                $results = array();
                while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                    $results[] = call_user_func($handler, $row);
                }
            }
            $this->checkForError();
            return $results;
        } catch (\PDOException $e) {
            $this->logPdoError($e);
        }
        return null;
    }

    /**
     * Maps SQL results into an array of objects.
     *
     * @todo WARNING: only SELECT queries are translated. Performance issue with complex queries containing nested subqueries
     *
     * @param string $pql Pseudo SQL query to translate and execute
     * @param array $params Pseudo SQL query parameters
     * @param string|null $resultClass Class to use for mapping results. Null for automatic detection (simple queries only)
     * @internal param \PDOStatement $q Requête SQL à exécuter
     * @return array La liste de résultats transformés
     */
    public function pqlSelect($pql, $params = null, $resultClass = null)
    {
        try {
            $sql = $this->_parser->parse($pql);
            if ($resultClass === null) {
                $resultClass = $this->_parser->lastParsedClass;
            }
            $q = $this->prepareStatement($sql);
            $q->execute($params);
            $results = array();
            while (false !== ($res = $q->fetch(\PDO::FETCH_ASSOC))) {
                $reflect = PlayHP::classLoder()->getReflectionClass($resultClass);
                $dbObject = $reflect->newInstance();
                $this->rowToObject($res, $dbObject);
                $results[] = $dbObject;
            }
            return $results;
        } catch (\PDOException $e) {
            $this->logPdoError($e);
        }
        return null;
    }

    /**
     * @param string $resultClass Class of the DB object to return
     * @param array $filters Filters (key: property name, value: filter value, prefix keys with '!' for not, and '~' for like)
     * @param string|array $orders Sort properties (key: property name, value: true for sorting asc, false for desc)
     * @return mixed The populated result
     */
    public function fetchFirst($resultClass, $filters = array(), $orders = array())
    {
        $dbObject = null;
        try {
            list($sql, $params) = $this->_parser->generateSQLQuery($resultClass, $filters, $orders);
            $this->replaceSchema($sql);
            $q = $this->prepareStatement($sql);
            $q->execute($params);
            if (false !== ($res = $q->fetch(\PDO::FETCH_ASSOC))) {
                $reflect = PlayHP::classLoder()->getReflectionClass($resultClass);
                $dbObject = $reflect->newInstance();
                $this->rowToObject($res, $dbObject);
                return $dbObject;
            }
        } catch (\PDOException $e) {
            $this->logPdoError($e);
        }
        return $dbObject;
    }


    /**
     * Fetches all objects of given class
     * @param string $resultClass DB object class name
     * @param array $filters Filters (key: property name, value: filter value, prefix keys with '!' for not, and '~' for like)
     * @param array $orders Sort properties (key: property name, value: true for sorting asc, false for desc)
     * @return array The mapped result list
     */
    public function fetchAll($resultClass, $filters = array(), $orders = array())
    {
        try {
            $results = array();
            list($sql, $params) = $this->_parser->generateSQLQuery($resultClass, $filters, $orders);
            $q = $this->prepareStatement($sql);
            $q->execute($params);
            while (false !== ($res = $q->fetch(\PDO::FETCH_ASSOC))) {
                $reflect = PlayHP::classLoder()->getReflectionClass($resultClass);
                $dbObject = $reflect->newInstance();
                $this->rowToObject($res, $dbObject);
                $results[] = $dbObject;
            }
            return $results;
        } catch (\PDOException $e) {
            $this->logPdoError($e);
        }
        return null;
    }

    /**
     * Fills a DB object instance provided it has mapping annotations and its primary key fields are valued
     * @param mixed $object The object to hydrate
     */
    public function hydrate(&$object)
    {
        $meta = DBObjectMeta::getMeta($object);
        if ($meta === null) {
            return;
        }
        $adapter = $this->getAdapter();
        $sql = 'SELECT * from {schema}' . $adapter->getEscapeChar(true) . $meta->tableName . $adapter->getEscapeChar(false) . ' WHERE ' . $meta->pkClause;
        try {
            $q = $this->prepareStatement($sql);
            $q->execute($meta->getPKParams($object));
            if (false !== ($res = $q->fetch(\PDO::FETCH_ASSOC))) {
                $this->rowToObject($res, $object);
            }
        } catch (PDOException $e) {
            $this->logPdoError($e);
        }
    }

    /**
     * Counts existing DB objects in database
     * @param string $dbObjectClass Class name of the DB object to count
     * @param array $filters List of filters to apply on the query
     * @return int Number of rows in Model's table
     */
    public function count($dbObjectClass, $filters = array())
    {
        list($sql, $params) = $this->_parser->generateSQLQuery($dbObjectClass, $filters, null, true);
        $result = $this->sqlSelect($sql, $params);
        return intval($result[0]['nb']);
    }

    /**
     * Checks whether last call was in error
     */
    protected function checkForError()
    {
        $errInfo = $this->pdo->errorInfo();
        if (is_array($errInfo) && count($errInfo) > 1 && $errInfo[1] !== null) {
            $this->logPdoError();
        }
    }

    /**
     * Logs DBMS errors
     * @param \PDOException $e Error to log
     */
    protected function logPdoError(\PDOException $e = null)
    {
        if ($e != null) {
            trace('PDO error: ' . $e->getMessage());
        } else {
            $errors = $this->pdo->errorInfo();
            if ($errors !== null && count($errors) > 2) {
                trace($errors[2]);
            }
        }
    }


    /**
     * Initializes the object using the results of given SQL query
     * @param mixed $model Object to map results into
     * @param array $result Result of an SQL query (FETCH_ASSOC)
     */
    public function rowToObject($result, $model)
    {
        $meta = DBObjectMeta::getMeta($model);
        foreach ($result as $colName => $value) {
            foreach ($meta->propertiesMap as $propName => $columnMeta) {
                if ($columnMeta->columnName != $colName) {
                    continue;
                }
                if (strtolower($columnMeta->columnType) == 'enum') {
                    // Enum parsing
                    try {
                        $enumClass = PlayHP::classLoder()->getReflectionClass($columnMeta->propertyType);
                        if ($enumClass->getParentClass()->name === 'PlayHP\Lang\Enum') {
                            // Map enum
                            $value = $enumClass->getStaticPropertyValue($value);
                        }
                    } catch (\Exception $e) {
                        // Default behavior
                    }
                } else {
                    switch ($columnMeta->propertyType) {
                        case '\DateTime':
                        case 'DateTime':
                        case 'date':
                            // TODO: check date time parsing depending on adapter?
                            $value = new \DateTime($value);
                            break;
                        case 'integer':
                        case 'int':
                            $value = intval($value);
                            break;
                        case 'double':
                        case 'float':
                            $value = floatval($value);
                            break;
                        case 'boolean':
                        case 'bool':
                            $value = $value == 1 || strtolower($value) == 'true';
                            break;
                        default:
                            break;
                    }
                }
                $model->$propName = $value;
                break;
            }
        }
    }

    /**
     * Creates an array of parameters based on the object.
     * @param mixed $model The annotated model object instance
     * @param bool $stripPK Flag telling to strip primary key column values from the result array
     * @return array Query parameters compliant with PDO, based on current object
     */
    public function getAsQueryParameters($model, $stripPK = false)
    {
        $meta = DBObjectMeta::getMeta($model);
        $parameters = array();
        foreach ($meta->propertiesMap as $classVar => $metaData) {
            if ($stripPK && $metaData->isInPK) {
                continue;
            }
            $colName = $metaData->columnName;
            $parameters[':' . $colName] = $model->$classVar;
        }
        return $parameters;
    }

    /**
     * Translates a pseudo SQL query into a real SQL query.
     * @todo WARNING: only SELECT queries are translated. Performance issue with complex queries containing nested subqueries
     * @param string $pql Pseudo SQL query to translate
     * @return string Translated query
     */
    public function getSQLQuery($pql)
    {
        $sql = $this->_parser->parse($pql);
        $this->replaceSchema($sql);
        return $sql;
    }

    /**
     * @private
     * Adds the schema prefix to the table name
     * @param string &$sql SQL query to prepare
     * @return void
     */
    private function replaceSchema(&$sql)
    {
        $adapter = $this->getAdapter();
        $defaultSchema = $adapter->getDefaultSchema();
        if (!empty($defaultSchema)) {
            $defaultSchema .= '.';
        }
        $sql = str_replace('{schema}', $defaultSchema, $sql);
    }

    /**
     * Fetches a relation described by an SQL query and initializes the object's property with mapped results.
     * @param mixed $object Object to fetch relation from
     * @param string $propertyName The property to use for getting relation values
     * @return array|null The relation results
     */
    public function fetchRelation($object, $propertyName)
    {
        $classLoader = PlayHP::classLoder();
        $meta = DBObjectMeta::getMeta($object);
        $property = $meta->getDBProperty($propertyName);

        // Extract query parameters
        $params = null;
        if (preg_match('~:([^ ]+)~i', $property->fetchQuery, $paramMatches)) {
            $params = array();
            foreach ($paramMatches as $i => $paramName) {
                if ($i > 0) {
                    $params[':' . $paramName] = $object->$paramName;
                }
            }
        }
        $sql = $property->fetchQuery;
        $this->replaceSchema($sql);
        try {
            $q = $this->pdo->query($sql);
            $this->checkForError();
            if ($params !== null) {
                foreach ($params as $paramName => $paramValue) {
                    $q->bindParam($paramName, $paramValue);
                }
            }
            $results = array();
            while (false !== ($result = $q->fetch(\PDO::FETCH_ASSOC))) {
                if (count($result) > 1) {
                    // Multiple columns
                    $propertyTypeMeta = DBObjectMeta::getMeta($property->propertyType);
                    if ($propertyTypeMeta !== null) {
                        // Relation is mapped
                        $reflect = $classLoader->getReflectionClass($propertyTypeMeta->className);
                        $mappedProperty = $reflect->newInstance();
                        $this->rowToObject($result, $mappedProperty);
                        $results[] = $mappedProperty;
                    } else {
                        // Simply use the associative array
                        $results[] = $result;
                    }
                } else {
                    // Single column
                    $results = current($result);
                }
            }
            // Update object
            $object->$propertyName = $results;
            return $results;
        } catch (PDOException $e) {
            $this->logPdoError($e);
        }
        return null;
    }

    /**
     * Executes a given query within existing transaction, or creates a new one if required
     * @param string $sql Query to execute
     * @param array $params Query parameters
     * @return int Result of the query
     */
    protected function executeInTransaction($sql, $params)
    {
        if (!$this->_inTransaction) {
            // No transaction, create one and commit after execution
            $this->beginTransaction();
            $result = $this->execute($sql, $params);
            $this->commit();
            return $result;
        } else {
            $result = $this->execute($sql, $params);
            return $result;
        }
    }

    /**
     * Saves an object
     * @param mixed $object Annotated object to save
     * @return bool Save status
     */
    public function save($object)
    {
        $meta = DBObjectMeta::getMeta($object);
        if ($meta == null) {
            return false;
        }
        $nb = $this->count($meta->className, $meta->getPKParams($object));
        if ($nb == 0) {
            $nb = $this->insert($object);
        } else {
            $nb = $this->update($object);
        }
        return $nb > 0;
    }
}

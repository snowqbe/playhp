<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\DB;
use Exception;
use PlayHP\PlayHP;


/**
 * Parses pseudo queries to replace with mapping properties.
 *
 * TODO: Need to optimize the way queries are parsed. This is way too slow for now with complex queries containing nested subqueries. The main problem is to reconstitute the whole SQL after having translated each part.
 *
 * @package PlayHP\DB
 */
class QueryParser
{

    /**
     * @var array List of SQL keywords to ignore
     */
    private static $keywords = array(
        'inner',
        'left',
        'right',
        'join',
        'on',
        '=',
        '!=',
        'like',
        'is',
        'null',
        'not',
        'and',
        'order',
        'asc',
        'desc',
        'as',
        'or',
    );

    /**
     * Last detected class name in parsed query. Useful for simple queries with automatic mapping
     * @var string
     */
    public $lastParsedClass;

    /**
     * @var DBAdapter
     */
    private $_adapter;


    /**
     * @param DBAdapter $adapter Adapter to use for translating pseudo queries
     */
    function __construct($adapter)
    {
        $this->_adapter = $adapter;
    }


    /**
     * @private
     * Parses brackets to isolate subqueries
     * @param string $string String to analyze
     * @param array $subQueries Resulting list of subqueries
     * @param int $level
     */
    private function getSubQueries($string, &$subQueries, $level = 0)
    {
        if (preg_match_all('/\((([^()]*|(?R))*)\)/', $string, $matches)) {
            for ($i = 0; $i < count($matches[1]); $i++) {
                if (is_string($matches[1][$i])) {
                    if (strlen($matches[1][$i]) > 0) {
                        $subQueries[] = array($level, $matches[1][$i]);
                        $this->getSubQueries($matches[1][$i], $subQueries, $level + 1);
                    }
                }
            }
        }
    }

    private function sortSubqueries($a, $b)
    {
        list($levelA,) = $a;
        list($levelB,) = $b;
        if ($levelA > $levelB) {
            return -1;
        }
        if ($levelB > $levelA) {
            return 1;
        }
        return 0;
    }

    /**
     * Parses a pseudo query and returns the native SQL equivalent
     * @param string $sql SQL query to prepare
     * @return string Parsed query
     */
    public function parse($sql)
    {
        $subQueries = array();
        $this->getSubQueries($sql, $subQueries);

        if (!empty($subQueries)) {
            foreach ($subQueries as $i => $subQuery) {
                if (stripos($subQuery[1], 'select') === false) {
                    unset($subQueries[$i]);
                }
            }
            usort($subQueries, array($this, 'sortSubqueries'));
            $parsedQueries = array();
            foreach ($subQueries as $subQueryAndLevel) {
                list($level, $subQuery) = $subQueryAndLevel;
                $tokenMap = array();
                $tokenIndex = 0;
                $tokenedSubQuery = $subQuery;
                foreach ($parsedQueries as $prevQuery => $prevQueryResult) {
                    if (strpos($subQuery, $prevQuery)) {
                        $tokenIndex++;
                        $token = '{{' . $tokenIndex . '}}';
                        $tokenedSubQuery = str_replace($prevQuery, $token, $tokenedSubQuery);
                        $tokenMap[$token] = $prevQueryResult;
                    }
                }
                $parsedQuery = $this->parseQuery($tokenedSubQuery);
                foreach ($tokenMap as $token => $prevQueryResult) {
                    $parsedQuery = str_replace($token, $prevQueryResult, $parsedQuery);
                }
                $parsedQueries[$subQuery] = $parsedQuery;
                if ($level === 0) {
                    $sql = str_replace($subQuery, $parsedQuery, $sql);
                }
            }
            foreach ($parsedQueries as $prevQuery => $prevQueryResult) {
                $sql = str_replace($prevQuery, $prevQueryResult, $sql);
            }
        }
        return $this->parseQuery($sql);
    }

    /**
     * @private
     * Parses a pseudo query and returns the native SQL equivalent (subqueries not
     * @param string $sql Pseudo query to parse
     * @return string Parsed query
     * @throws DBQueryException
     */
    private function parseQuery($sql)
    {
        $adapter = $this->_adapter;
        $classLoader = PlayHP::classLoder();
        $matches = array();
        $sql = trim($sql);
        if (preg_match('/^(\(*)(select.*)(\)*)$/i', $sql, $matches)) {
            $prefix = $matches[1];
            $sql = $matches[2];
            $suffix = $matches[3];
            preg_match('/(.*)\s+FROM(\s+.*)/i', $sql, $matches);
            $select = $matches[1];
            $from = $matches[2];
            $where = '';
            if (preg_match('/(.*)\s+WHERE(\s+.*)/i', $from, $matches)) {
                $from = $matches[1];
                $where = $matches[2];
            }

            // Handle from clause to get class names to handle
            $aliasPattern = '(\s+(as )?((?!(inner|left|right|join|,|\s))))?';
            $tableNamePattern = '([^\s,]+)';
            $joinPattern = '((inner|left|right)?\s+join\s+)';
            $joinClausePattern = '(on\s+(?!(inner|left|right|join|,))\s+)';
            $tablePattern = $tableNamePattern . $aliasPattern;
            if (!preg_match_all('/(' . $tablePattern . ')(' . $joinPattern . $tablePattern . $joinClausePattern . ')*,?/', $from, $matchFrom, PREG_SET_ORDER)) {
                throw new DBQueryException('Malformed query, no table defined between brackets in the FROM clause: ' . $sql);
            }
            $classes = array();
            $nbMatches = count($matchFrom);
            for ($i = 0; $i < $nbMatches; $i++) {
                $part = $matchFrom[$i][2];
                try {
                    $classLoader->getReflectionClass($part);
                } catch (Exception $e) {
                    // Nothing to do
                    continue;
                }
                $className = $part;
                $meta = DBObjectMeta::getMeta($className);
                $classes[$className] = $meta;
                $tableAlias = null;
                if ($nbMatches > $i + 1) {
                    $tableAlias = $matchFrom[$i + 1][2];
                    if (in_array($tableAlias, self::$keywords)) {
                        // Keyword detected, not an alias
                        $tableAlias = null;
                    }
                    // Jump the next row as it corresponds either to table alias or to an SQL keyword
                    $i++;
                }
                $this->lastParsedClass = $meta->className;
                $from = $this->replaceColumns($from, $meta, $tableAlias, $className);
                $select = $this->replaceColumns($select, $meta, $tableAlias, $className);
                if ($where !== '') {
                    $where = $this->replaceColumns($where, $meta, $tableAlias, $className);
                }
                $tableName = $adapter->getEscapeChar(true) . $meta->tableName . $adapter->getEscapeChar(false);
                $from = str_replace($className, $tableName, $from);
            }
            return $prefix . trim($select . ' FROM ' . trim($from) . ($where === '' ? '' : ' WHERE ' . trim($where))) . $suffix;
        } else if (stripos($sql, 'update')) {
            preg_match('/UPDATE\s+(.*)\s+SET\s+(.*)/i', $sql, $matches);
            $table = trim($matches[1]);
            $updateFields = trim($matches[2]);
            $where = '';
            if (preg_match('/(.*)\s+WHERE\s+(.*)/i', $updateFields, $matches)) {
                $updateFields = trim($matches[1]);
                $where = trim($matches[2]);
            }
            // TODO: update
            return $sql;
        } else if (stripos($sql, 'insert')) {
            preg_match('/INSERT INTO\s+(.*)\s+VALUES\s+(.*)/i', $sql, $matches);
            $table = trim($matches[1]);
            $insertFields = '';
            if (preg_match('/(.*)\s+\(([^\)]+)\)/i', $table, $matches)) {
                $table = trim($matches[1]);
                $insertFields = trim($matches[2]);
            }
            // TODO: insert
            return $sql;
        }
        return false;
    }


    /**
     * @private
     * @param string $subject Part of the query where the replacement should take place
     * @param DBObjectMeta $meta Meta data of the class
     * @param null|string $tableAlias Table alias, or null if no table alias
     * @param null|string $className Class name
     * @return mixed|string Subject with transformed columns
     */
    private function replaceColumns($subject, $meta, $tableAlias, $className)
    {
        $adapter = $this->_adapter;
        $propPrefix = '(' . ($tableAlias === null ? $className : $tableAlias) . '\.|\s+|\()';
        $pattern = '/' . $propPrefix . '(' . implode('|', array_keys($meta->propertiesMap)) . ')([^A-Za-z0-9_]{1})?/';
        if (preg_match_all($pattern, $subject, $matchColumns, PREG_SET_ORDER)) {
            foreach ($matchColumns as $elements) {
                $finalAlias = ($tableAlias === null ? $meta->tableName : $tableAlias) . '.';
                $aliasOrSep = $elements[1];
                $propName = $elements[2];
                $columnName = $meta->propertiesMap[$propName]->columnName;
                $replacement = ($aliasOrSep === '(' ? '(' : ' ') . $finalAlias . $adapter->getEscapeChar(true) . $columnName . $adapter->getEscapeChar(false) . '$2';
                $replacePattern = '/' . preg_quote($aliasOrSep) . '(' . $propName . ')([^A-Za-z0-9_]{1})?/';
                $subject = preg_replace($replacePattern, $replacement, ' ' . $subject);
            }
        }
        return $subject;
    }

    /**
     * Generates an SQL query based on provided parameters
     * @param string $dbObjectClass Database object class name
     * @param array $filters Filters (key: property name, value: filter value, prefix keys with '!' for not, and '~' for like)
     * @param string|array $orders Sort properties (key: property name, value: true for sorting asc, false for desc)
     * @param bool $count true to asc for count only, false for selecting all
     * @return array Generated SQL query and params
     */
    public function generateSQLQuery($dbObjectClass, $filters = array(), $orders = array(), $count = false)
    {
        $params = array();
        $meta = DBObjectMeta::getMeta($dbObjectClass);
        $sql = 'SELECT ' . ($count ? 'count(*) as "nb"' : '*') . ' FROM {schema}' . $meta->tableName . ' ';
        if ($filters !== null && is_array($filters) && !empty($filters)) {
            $sql .= 'WHERE ';
            $first = true;
            foreach ($filters as $filterProperty => $filterValue) {
                $notIndex = strpos($filterProperty, '!');
                if ($notIndex === 0) {
                    $filterProperty = substr($filterProperty, 1);
                }
                $tildaIndex = strpos($filterProperty, '~');
                if ($tildaIndex === 0) {
                    $filterProperty = substr($filterProperty, 1);
                }
                $useLike = ($tildaIndex === 0 || strpos($filterValue, '%') !== false);
                $filterColumn = $meta->getColumn($filterProperty);
                if (!$first) {
                    $sql .= ' AND ';
                } else {
                    $first = false;
                }
                $sql .= ' ' . $this->_adapter->getEscapeChar(true) . $filterColumn . $this->_adapter->getEscapeChar(false);
                if ($filterValue === null) {
                    $sql .= $notIndex === 0 ? ' IS NOT NULL' : ' IS NULL';
                } else {
                    $sql .= $useLike ? (($notIndex === 0 ? ' NOT' : '') . ' LIKE ') : (($notIndex === 0 ? ' !' : ' ') . '= ');
                    list($handled, $realValue) = $this->handleParam($filterValue);
                    if ($handled) {
                        $sql .= $realValue;
                    } else {
                        $sql .= ':' . $filterColumn;
                        $params[':' . $filterColumn] = $realValue;
                    }
                }
            }
        }
        if ($orders !== null && !empty($orders)) {
            $sql .= ' ORDER BY ';
            foreach ($orders as $orderProp => $orderAsc) {
                $orderByColumn = $meta->getColumn($orderProp);
                if ($orderByColumn !== null) {
                    $sql .= $this->_adapter->getEscapeChar(true) . $orderByColumn . $this->_adapter->getEscapeChar(false) . ($orderAsc ? ' ASC' : ' DESC');
                }
            }
        }
        return array($sql, $params);
    }


    /**
     * Utility for handling specific SQL keywords
     * @param mixed $paramValue Value of the parameter to integrate
     * @return array First element: is the value SQL specific or not? Second element: the SQL value to use in the query
     */
    public function handleParam($paramValue)
    {
        if ($paramValue === true) {
            return array(true, 1);
        }
        if ($paramValue === false) {
            return array(true, 0);
        }
        switch ($paramValue) {
            case "{now}":
                return array(true, $this->_adapter->getNow());
            default:
                if (is_a($paramValue, '\DateTime')) {
                    return array(true, $this->_adapter->formatDateTime($paramValue));
                }
                return array(false, $paramValue);
        }
    }
}
<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Exceptions;

/**
 * Thrown whenever an expected fil is missing
 *
 * @package PlayHP\Security\Providers
 */
class MissingFileException extends \Exception
{

}
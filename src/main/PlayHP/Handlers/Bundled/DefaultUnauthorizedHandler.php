<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Handlers\Bundled;

use PlayHP\Handlers\Handler;
use PlayHP\Handlers\HandlerType;
use PlayHP\PlayHP;
use PlayHP\Security\SecurityService;

class DefaultUnauthorizedHandler extends Handler
{

    /**
     * @Inject
     * @var SecurityService Service handling security
     */
    public $securityService;

    /**
     * @return HandlerType The place where the handler should be invoked in the process
     */
    public function getType()
    {
        return HandlerType::$Unauthorized;
    }

    /**
     * Implement your own behavior in this method.
     * You may indicat to PlayHP not to proceed with its default behavior afterwards
     * @return bool Indicate whether or not default behavior should be done (true is for proceeding with default behavior, false otherwise)
     */
    public function invoke()
    {
        $user = $this->securityService->getAuthenticatedUser();
        if ($user === null) {
            // Login
            $router = PlayHP::router();
            $path = $router->getCurrentPath();
            PlayHP::forward('@login/' . $path);
            return false;
        }
        // Unauthorized to currently logged on user
        return true;
    }
}
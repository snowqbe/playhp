<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Handlers;


abstract class Handler
{

    /**
     * @var bool Flag telling to automatically bind the handler if declared in the object pool
     */
    public $autoBind = true;

    /**
     * @return HandlerType The place where the handler should be invoked in the process
     */
    public abstract function getType();

    /**
     * Implement your own behavior in this method.
     * You may indicat to PlayHP not to proceed with its default behavior afterwards
     * @return bool Indicate whether or not default behavior should be done (true is for proceeding with default behavior, false otherwise)
     */
    public abstract function invoke();

}
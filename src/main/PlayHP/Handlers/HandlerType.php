<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Handlers;

use PlayHP\Lang\Enum;

/**
 * List of possible PlayHP handlers
 * @package PlayHP\Handlers
 */
class HandlerType extends Enum
{
    /**
     * Triggered before PlayHP retrieves the controller and invokes the rendering
     * @var HandlerType
     */
    public static $PreInit;

    /**
     * Triggered before invoking the route's target
     * @var HandlerType
     */
    public static $PreInvoke;

    /**
     * Triggered before rendering pass
     * @var HandlerType
     */
    public static $PreRender;

    /**
     * Triggered after content was rendered
     * @var HandlerType
     */
    public static $PostRender;

    /**
     * Triggered on PlayHP unauthorized route access
     * @var HandlerType
     */
    public static $Unauthorized;

    /**
     * Triggered when PlayHP will trigger a 404 error
     * @var HandlerType
     */
    public static $NotFound;

    /**
     * Triggered on PlayHP errors
     * @var HandlerType
     */
    public static $Error;
}

HandlerType::init();
<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\I18n;


/**
 * Locale definition
 * @package PlayHP\I18n
 */
class Locale
{

    /**
     * Full locale code (e.g. en_US)
     * @var string
     */
    private $_code;

    /**
     * 2-letters locale code (e.g. en)
     * @var string
     */
    private $_shortCode;

    /**
     * Date format string for PHP date function
     * @var string
     */
    private $_dateFormat = 'm/d/Y';

    /**
     * @param string $code Locale code to use
     */
    function __construct($code)
    {
        $this->setCode($code);
    }

    public function getCode()
    {
        return $this->_code;
    }

    public function setCode($code)
    {
        $this->_code = $code;
        if (strlen($code) > 2) {
            $this->_shortCode = strtolower(substr(trim($code), 0, 2));
        } else {
            $this->_shortCode = strtolower(trim($code));
        }
        if ($this->_shortCode == 'fr') {
            $this->_dateFormat = 'd/m/Y';
        }
    }

    public function getDateFormat()
    {
        return $this->_dateFormat;
    }

    public function getShortCode()
    {
        return $this->_shortCode;
    }

}
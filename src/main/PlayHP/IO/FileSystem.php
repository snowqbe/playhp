<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\IO;


/**
 * File system utility functions
 * @package PlayHP\IO
 */
class FileSystem
{

    /**
     * Adds given folder contents to given $files_array. Path will begin by given $base parameter
     * @param string $dir_path Path to the folder to scan
     * @param array $files_array Array which will contain files path
     * @param string $base Path that will precede scanned files paths
     * @param string[] $excludes List of paths to exclude
     */
    public static function listPath($dir_path, &$files_array, $base = "", $excludes = array())
    {
        if (substr($dir_path, -1) != '/') {
            $dir_path .= '/';
        }
        if (strlen($base) > 0 && substr($base, -1) != '/') {
            $base .= '/';
        }
        $dir_var = dir($dir_path);
        while (false !== ($entry = $dir_var->read())) {
            if (substr($entry, 0, 1) != ".") {
                if (!is_dir($dir_path . $entry)) {
                    $files_array[] = $base . $entry;
                } else if (!in_array($base . $entry . '/', $excludes)) {
                    self::listPath($dir_path . $entry . '/', $files_array, $base . $entry . '/');
                }
            }
        }
        $dir_var->close();
    }

    /**
     * Removes all contents from given path, excepted elements given in the $except array.
     * WARNING: Please note that the folder will also be destroyed if not given in the $except array.
     * @param string $directoryPath Path to clean
     * @param array $except (Optional) List of elements to keep
     */
    public static function clearDir($directoryPath, $except = array())
    {
        if (substr($directoryPath, -1) != "/")
            $directoryPath .= "/";
        if (!is_dir($directoryPath))
            return;
        $dir_var = dir($directoryPath);
        while (false !== ($entry = $dir_var->read())) {
            if ($entry != '.' && $entry != '..') {
                if (!is_dir($directoryPath . $entry)) {
                    if (!in_array($directoryPath . $entry, $except)) {
                        unlink($directoryPath . $entry);
                    }
                } else {
                    self::clearDir($directoryPath . $entry . '/', $except);
                }
            }
        }
        $dir_var->close();
        if (!in_array($directoryPath, $except)) {
            rmdir($directoryPath);
        }
    }

    /**
     * Creates all necessary folders to access given path. Example "./dir1/dir2/dir3/" will trigger the creation of dir1, then dir2 then dir3 etc. ...
     * @param string $pathName The awaited path
     */
    public static function createDir($pathName)
    {
        $actual_path = '';
        $dirs = explode('/', $pathName);
        foreach ($dirs as $directoryName) {
            $actual_path .= $directoryName . '/';
            if (!empty($directoryName) && substr($directoryName, 0, 1) != '.' && !is_dir($actual_path)) {
                mkdir($actual_path, 0755);
            }
        }
    }

    /**
     * Copies the entire $source folder contents into the $destination folder.
     * @param string $source Source folder (with or without ending slash "/")
     * @param string $destination Destination folder (with or without ending slash "/")
     * @return array The copied files list (whole destination path)
     */
    public static function copyPath($source, $destination)
    {
        $files = array();

        if (!is_dir($destination))
            self::createDir($destination);

        if (substr($source, -1) != '/')
            $source = $source . '/';

        if (substr($destination, -1) != '/')
            $destination = $destination . '/';

        $d = dir($source);
        while (false !== ($entry = $d->read())) {
            if ($entry != '.' && $entry != '..') {
                if (is_dir($source . $entry)) {
                    array_merge($files, self::copyPath($source . $entry, $destination . $entry)); // It's a subfolder: recurse
                } else {
                    copy($source . $entry, $destination . $entry); // It's a file: copy it to destination
                    $files[] = $destination . $entry;
                }
            }
        }
        $d->close();
        return $files;
    }


    /**
     * @param string $path Path to transform
     * @return mixed|string Transformed path
     */
    public static function safePath($path)
    {
        if ($path === null) {
            return '';
        }
        $path = str_replace("\\", "/", $path);
        if (substr($path, -1) != '/') {
            $path .= '/';
        }
        return $path;
    }

    /**
     * Lists all folders present within $source into the $result array
     * @param string $source Source folder (with or without ending slash "/")
     * @param array $result Results array
     * @param array $excludes List of paths to exclude from result
     * @param string $initPath Base path used to process relative path. Will be initialized with $source if not given
     */
    public static function getDirs($source, &$result, $excludes = array(), $initPath = "")
    {
        $source = self::safePath($source);
        if (empty($initPath)) {
            $initPath = $source;
        } else {
            $initPath = self::safePath($initPath);
        }
        for ($i = 0; $i < count($excludes); $i++) {
            $excludes[$i] = self::safePath($excludes[$i]);
        }

        $d = dir($source);
        while (false !== ($entry = $d->read())) {
            $relativePath = substr($source . $entry, strlen($initPath));
            if ($entry != "." && $entry != "..") {
                if (is_dir($source . $entry) && !in_array($source . $entry . "/", $excludes)) {
                    $result[$relativePath] = $initPath . $relativePath . '/';
                    self::getDirs($source . $entry, $result, $excludes, $initPath);
                }
            }
        }
        $d->close();
    }

    /**
     * @param string $absoluteFilePath The file path to extract
     * @return string The file name
     */
    public static function fileName($absoluteFilePath)
    {
        $path = str_replace('\\', '/', $absoluteFilePath);
        return substr(strrchr($path, '/'), 1);
    }

    /**
     * @param string $absoluteFilePath The file path to extract
     * @param bool $lowerCase true to return the extension in lower case, false to keep original format
     * @return string The file extension
     */
    public static function fileExt($absoluteFilePath, $lowerCase = true)
    {
        $ext = substr(strrchr($absoluteFilePath, '.'), 1);
        return $lowerCase ? strtolower($ext) : $ext;
    }

}
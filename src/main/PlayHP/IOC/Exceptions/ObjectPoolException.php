<?php

namespace PlayHP\IOC\Exceptions;

/**
 * Object pool exception triggerd whenever any IOC error occurs
 */
class ObjectPoolException extends \Exception
{

}

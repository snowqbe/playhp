<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Ioc;


/**
 * Defines how an injection should be made by the object pool
 * @package PlayHP\Ioc
 */
class InjectionDefinition
{

    /**
     * @var string ID of the object to inject
     */
    public $injectId;

    /**
     * @var string Type of variable
     */
    public $varType;

    /**
     * @param string $varType Type of variable
     * @param string $injectId ID of the object to inject
     */
    function __construct($varType, $injectId)
    {
        $this->varType = $varType;
        $this->injectId = $injectId;
    }


}
<?php

namespace PlayHP\Ioc;

/**
 * Represents object metadata for pooling purposes
 */
class ObjectMetaData
{

    /**
     * ID of the object in the pool
     * @var string
     */
    public $objectId;

    /**
     * Class name
     * @var string
     */
    public $className;

    /**
     * Initialization method to call after instanciation and injections
     * @var string
     */
    public $initMethod;

    /**
     * List of objects this object's creation will depend on
     * @var array
     */
    public $constructorArgs = array();

    /**
     * List of object's properties
     * @var array
     */
    public $properties = array();

    /**
     * Flag indicating autowiring is activated for this object definition
     * @var bool
     */
    public $autowire;

    /**
     * List of injections to do in method calls
     * @var InjectionDefinition[]
     */
    public $methodInjections = array();

    /**
     * @param string $objectId ID of the object in the pool
     * @param string $className Name of the class
     */
    function __construct($objectId, $className)
    {
        $this->className = $className;
        $this->objectId = $objectId;
    }

}

<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Ioc;


class ObjectRef
{

    /**
     * @var string Id of the reference
     */
    public $objectId;

    function __construct($objectId = null)
    {
        $this->objectId = $objectId;
    }


}
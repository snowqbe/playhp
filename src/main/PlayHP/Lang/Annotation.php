<?php

namespace PlayHP\Lang;

/**
 * Utility class for annotation handling
 */
class Annotation
{
    /**
     * Regex to use for parsing annotations
     */
    const ANNOT_REGEX = '#^[^@\n]*@([A-Za-z_0-9]+)([^\n]*)$#sm';

    /**
     * Start of the regex to use for parsing annotations with filter
     */
    const ANNOT_REGEX_FILTER_START = '#^[^@\n]*@(';

    /**
     * End of the regex to use for parsing annotations with filter
     */
    const ANNOT_REGEX_FILTER_END = ')([^\n]*)$#sm';

    /**
     * Annotation's name
     * @var string
     */
    public $name;

    /**
     * List of params (key => value)
     * @var array
     */
    public $params;

    /**
     * List of native annotations
     * @var array
     */
    static $NATIVE_ANNOTATIONS = array(
        'param',
        'deprecated',
        'inheritDoc',
        'return',
        'see',
        'link',
    );

    /**
     * List of sub annotations
     * @var Annotation[]
     */
    public $subAnnotations;

    /**
     * @param string $comment Comment to parse
     * @param null|array $filter List of searched annotations (others will be ignored)
     * @param bool $parseSubAnnotations Flag telling to parse annotations declared within an annotated line of comment
     * @return Annotation[] List of parsed annotations
     */
    public static function parseAnnotations($comment, $filter = null, $parseSubAnnotations = true)
    {
        $annotations = array();
        $matches = array();
        if ($filter === null) {
            $nb = preg_match_all(self::ANNOT_REGEX, $comment, $matches);
        } else {
            $nb = preg_match_all(self::ANNOT_REGEX_FILTER_START . implode('|', $filter) . self::ANNOT_REGEX_FILTER_END, $comment, $matches);
        }
        $annotNames = $matches[1];
        for ($i = 0; $i < $nb; $i++) {

            // Simple annotation
            $annotation = new Annotation();
            $annotName = $annotNames[$i];
            $value = trim($matches[2][$i]);
            $annotation->name = $annotName;
            $nbParams = preg_match_all('#^\(\s*([^)]+)\s*,?\s*\)$#im', $value, $valueMatches);
            $nextAnnotations = array();
            if ($nbParams > 0) {
                $val = $valueMatches[1][0];
                self::parseAnnotationExpression($val, $annotation);
            } else if ($value != '') {
                $firstSpace = strpos($value, ' ');
                if ($firstSpace > 0) {
                    // First one is type or comment
                    $annotation->params[] = substr($value, 0, $firstSpace);
                    $value = trim(substr($value, $firstSpace + 1));
                    $firstSpace = strpos($value, ' ');
                    if ($firstSpace > 0) {
                        $annotation->params[] = substr($value, 0, $firstSpace);
                        $value = trim(substr($value, $firstSpace + 1));
                        // Check for inner annotation
                        $arobasePos = strpos($value, '@');
                        if ($arobasePos > 0) {
                            $annotation->params[] = substr($value, 0, $arobasePos - 1);
                            $value = substr($value, $arobasePos);
                            if ($parseSubAnnotations)
                                $annotation->subAnnotations = self::parseAnnotations($value);
                        } else if ($arobasePos === 0) {
                            if ($parseSubAnnotations)
                                $annotation->subAnnotations = self::parseAnnotations($value);
                        } else {
                            $annotation->params[] = $value;
                        }
                    } else {
                        $annotation->params[] = $value;
                    }
                } else if (strpos($value, '@') !== 0) {
                    $annotation->params[] = $value;
                } else {
                    $nextAnnotations = self::parseAnnotations($value);
                }
            }
            $annotations[] = $annotation;
            foreach ($nextAnnotations as $nextAnnotation) {
                $annotations[] = $nextAnnotation;
            }
        }
        return $annotations;
    }

    /**
     * Looks for a given annotation within provided comment
     * @param string $comment Comment to search annotations
     * @param string $annotationName Name of the annotation to look for
     * @return null|Annotation The annotation found, null if not found
     */
    public static function getAnnotation($comment, $annotationName)
    {
        $annotations = self::parseAnnotations($comment, array($annotationName));
        foreach ($annotations as $annotation) {
            return $annotation;
        }
        return null;
    }

    /**
     * Looks for a given annotation within provided comment and returns its default param value
     * @param string $comment Comment to search annotations
     * @param string $annotationName Name of the annotation to look for
     * @param string $defaultParamName The name of the default parameter in the searched annotation
     * @return null|Annotation The annotation found, null if not found
     */
    public static function getAnnotationDefaultParam($comment, $annotationName, $defaultParamName = 'value')
    {
        $annotation = self::getAnnotation($comment, $annotationName);
        if ($annotation === null) {
            return null;
        }
        return $annotation->getDefaultParam($defaultParamName);
    }

    /**
     * @param string $val Value to parse
     * @param Annotation $annotation Target annotation
     */
    private static function parseAnnotationExpression($val, $annotation)
    {
        $params = explode(",", $val);
        foreach ($params as $paramExpr) {
            $nbKeys = preg_match_all('#\s*([a-z0-9._-]+)\s*=\s*([^,\)?]+)\s*#i', $paramExpr, $paramsMatches);
            if ($nbKeys > 0) {
                // Multiple params
                for ($j = 0; $j < $nbKeys; $j++) {
                    $annotation->params[$paramsMatches[1][$j]] = preg_replace('#[\'"]#', '', $paramsMatches[2][$j]);
                }
            } else {
                $exploded = explode(' ', trim($paramExpr));
                $annotation->params[$exploded[0]] = $exploded[0];
            }
        }
    }

    /**
     * @param string $paramName Default parameter's name
     * @param null $defaultValue
     * @return mixed The default parameter's value or null if no parameters or invalid
     */
    public function getDefaultParam($paramName = 'value', $defaultValue = null)
    {
        $nbParams = count($this->params);
        if ($nbParams == 0) {
            return $defaultValue;
        }
        $found = null;
        foreach ($this->params as $key => $val) {
            if ($found === null) {
                $found = $val;
            }
            if ($key == $paramName) {
                $found = $val;
                break;
            }
        }
        return $found;
    }

    /**
     * Lists the parameter types in a method's comment. The resulting hashmap has parameter names as keys
     * @param string $methodComment The method comment
     * @return array The map of parameter names with their according type name
     */
    public static function getParamTypes($methodComment)
    {
        $result = array();
        $varAnnotations = self::parseAnnotations($methodComment, array('param'), false);
        foreach ($varAnnotations as $varAnnotation) {
            $count = count($varAnnotation->params);
            if ($count >= 2) {
                $paramName = $varAnnotation->params[1];
                $paramType = $varAnnotation->params[0];

            } else if ($count >= 1) {
                // Default type
                $paramName = $varAnnotation->params[0];
                $paramType = '\stdClass';
            } else {
                // Error: var annotation without any other indication, skipping...
                continue;
            }
            $result[substr($paramName, 1)] = $paramType;
        }
        return $result;
    }

    /**
     * @param Annotation $annotation Param annotation to parse for retrieving the parameter name
     * @return null|string The parameter name
     */
    public static function getParamName($annotation)
    {
        for ($i = 0; $i < count($annotation->params); $i++) {
            $param = $annotation->params[$i];
            if (substr($param, 0, 1) == '$') {
                return substr($param, 1);
            }
        }
        return null;
    }

    /**
     * @return bool true if annotation has parameters, false otherwise
     */
    public function hasParams()
    {
        return count($this->params) > 0;
    }

    /**
     * Returns a parameter's value, or a default value if no parameter is defined with provided name
     * @param string $paramName Name of the parameter we want to get the value of
     * @param mixed $defaultValue Value to use if the parameter is not defined
     * @return mixed The value of the parameter if it exists, or $defaultValue otherwise
     */
    public function getOptionalParam($paramName, $defaultValue = null)
    {
        if ($this->params === null) {
            return $defaultValue;
        }
        if (array_key_exists($paramName, $this->params)) {
            return $this->params[$paramName];
        }
        return $defaultValue;
    }

    /**
     * Fetches a subannotation or returns false if none found
     * @param string $annotationName Name of the subannotation to fetch
     * @return bool|Annotation Annotation to retrieve
     */
    public function getSubAnnotation($annotationName)
    {
        if (empty($this->subAnnotations)) {
            return false;
        }
        foreach ($this->subAnnotations as $subAnnotation) {
            if ($subAnnotation->name == $annotationName) {
                return $subAnnotation;
            }
        }
        return false;
    }

}

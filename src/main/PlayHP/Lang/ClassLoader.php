<?php

namespace PlayHP\Lang;
use Exception;
use PlayHP\IO\FileSystem;

/**
 * PlayHP class loader.
 * Handles caching of reflection class and method data, as well as shortcuts from unqualified class names to
 * qualified ones.
 */
class ClassLoader
{

    /**
     * List of shortcuts to qualified class names.
     * key = shortcut, value = qualified class name
     * @var array
     */
    private $_shortcuts = array(
        'ClassLoader' => 'PlayHP\ClassLoader',
    );

    /**
     * Cache of already queried reflection classes
     * @var array
     */
    private $_reflectionClasses = array();

    /**
     * List of blacklisted shortcuts
     * @var array
     */
    private $_badShortcuts = array();

    /**
     * List of files in the classpath
     * @var array
     */
    private $_lookupPathFiles = array();

    /**
     * Metadata information of parsed Model's
     * @var array
     */
    private $_classMetaData = array();

    /**
     * @var ClassLoader
     */
    private static $_instance = null;

    /**
     * Autoloader
     * @param string $class Class name to load
     */
    public static function AUTO_LOAD($class)
    {
        self::inst()->autoLoad($class);
    }

    /**
     * @return ClassLoader
     */
    public static function inst()
    {
        if (self::$_instance === null) {
            self::$_instance = new ClassLoader();
        }
        return self::$_instance;
    }

    private function __construct()
    {
    }

    /**
     * List of paths to lookup
     * @param \string[] $paths Path to fetch controller definitions from
     */
    public function init(array $paths)
    {
        spl_autoload_register(__NAMESPACE__ . '\ClassLoader::AUTO_LOAD');

        foreach ($paths as $basePath) {
            $basePath = str_replace('\\', '/', $basePath);
            if (substr($basePath, -1) != '/') {
                $basePath .= '/';
            }
            $classes = array();
            FileSystem::listPath($basePath, $classes, $basePath);
            $this->_lookupPathFiles[$basePath] = $classes;
        }
    }

    /**
     * Autoloader for PlayHP
     * @param string $class Class name to find
     */
    public function autoLoad($class)
    {
        foreach ($this->_lookupPathFiles as $basePath => $classes) {
            foreach ($classes as $relativePath) {
                $ext = strrchr($relativePath, '.');
                $extLen = strlen($ext);
                $qualifiedClassName = str_replace('/', '\\', substr($relativePath, strlen($basePath), -$extLen));
                $className = substr($relativePath, strrpos($relativePath, '/') + 1, -$extLen);
                if ($class == $qualifiedClassName || $class == $className) {
                    // Found it
                    require_once $relativePath;

                    // TODO: ovewrite existing shortcuts or keep existing ones?
                    $this->_shortcuts[$className] = $qualifiedClassName;
                    return;
                }
            }
        }
    }

    /**
     * Retrieves meta data of a given type
     * @param string $className Type to get
     * @throws \Exception When provided type is unknown
     * @return \ReflectionClass The class information
     */
    public function getReflectionClass($className)
    {
        return $this->internalGetReflectionClass($className);
    }

    /**
     * @private
     * Internal process of reflection class retrieval
     * @param string $className
     * @param bool $recursed If we're trying to redo process after an autoload
     * @param string $className Type to get
     * @throws \Exception When provided type is unknown
     * @return \ReflectionClass The class information
     */
    private function internalGetReflectionClass($className, $recursed = false)
    {
        if (empty($className)) {
            return null;
        }

        $reflect = null;
        if (array_key_exists($className, $this->_reflectionClasses)) {
            // In cache
            return $this->_reflectionClasses[$className];
        }

        if (strpos($className, '\\') !== FALSE) {
            // Provided qualified name
            $reflect = new \ReflectionClass($className);
            $this->_reflectionClasses[$className] = $reflect;
            return $reflect;
        } else if (array_key_exists($className, $this->_shortcuts)) {
            // Try to look into shortcuts
            $qualifiedName = $this->_shortcuts[$className];
            if (strpos($qualifiedName, '\\') == 0) {
                $qualifiedName = substr($qualifiedName, 1);
            }
            $reflect = new \ReflectionClass($qualifiedName);
            $this->_reflectionClasses[$qualifiedName] = $reflect;
            $this->_reflectionClasses[$className] = $reflect;
            return $reflect;
        } else if (!$recursed) {
            // Try autoload
            $this->autoLoad($className);
            return $this->internalGetReflectionClass($className, true);
        }
        // Unable to find
        $this->_badShortcuts[] = $className;
        throw new Exception("$className is unknown");
    }

    /**
     * Retrieves custom meta data previously associated to class name using #setMeta
     * @param ClassMeta $metaType Type of metadata associated to class name
     * @param string $className Name of the class we want to fetch associated metadata
     * @return mixed Stored metadata of given type for the class name
     */
    public function getMeta(ClassMeta $metaType, $className)
    {
        if (!array_key_exists($metaType->value, $this->_classMetaData)) {
            $this->_classMetaData[$metaType->value] = array();
            return null;
        }
        if (array_key_exists($className, $this->_classMetaData[$metaType->value])) {
            return $this->_classMetaData[$metaType->value][$className];
        }
        return null;
    }

    /**
     * Defines custom meta data to class name using
     * @param ClassMeta $metaType Type of metadata associated to class name
     * @param string $className Name of the class we want to fetch associated metadata
     * @param mixed $classMeta Meta data object to associate to class name for given type
     * @see #getMeta
     */
    public function setMeta(ClassMeta $metaType, $className, $classMeta)
    {
        if (!array_key_exists($metaType->value, $this->_classMetaData)) {
            $this->_classMetaData[$metaType->value] = array($className => $classMeta);
        } else {
            $this->_classMetaData[$metaType->value][$className] = $classMeta;
        }
    }

    /**
     * Lists the class names affected by a given meta data type
     * @param ClassMeta $classMeta Type of metadata for which we want to list the affected class names
     * @return array List of class names having a meta data of type $classMeta defined
     */
    public function getClassNamesWithMeta(ClassMeta $classMeta)
    {
        if (array_key_exists($classMeta->value, $this->_classMetaData)) {
            return array_keys($this->_classMetaData[$classMeta->value]);
        }
        return array();
    }
}

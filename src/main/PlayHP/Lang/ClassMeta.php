<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Lang;


/**
 * Types of metadata which can be defined on class names
 * @package PlayHP\Lang
 */
class ClassMeta extends Enum
{

    /**
     * @var ClassMeta DB meta type
     */
    static $DB;

    /**
     * @var ClassMeta Validation meta type
     */
    static $Validation;
}

ClassMeta::init();
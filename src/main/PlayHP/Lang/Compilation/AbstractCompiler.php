<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Lang\Compilation;
use PlayHP\IO\FileSystem;


/**
 * Base class for compilers.
 *
 * @package PlayHP\Lang\Compilation
 */
abstract class AbstractCompiler
{

    /**
     * @var string Target path of compilation
     */
    protected $targetPath;

    /**
     * Compiles a given file.
     * @param string $sourceFile Source file to compile
     * @throws CompilerException Whenever a compilation error occurs
     */
    abstract protected function compileFile($sourceFile);

    /**
     * @return string[] The list of file extensions understood by the compiler, IN LOWER CASE.
     */
    abstract protected function getAuthorizedExtensions();

    /**
     * @return string The target file's extension
     */
    abstract protected function getTargetExtension();

    /**
     * @param string $targetPath The target path to define for the compiler
     * @throws CompilerException
     */
    function __construct($targetPath)
    {
        if (!is_dir($targetPath)) {
            throw new CompilerException('Target path ' . $targetPath . ' does not exist');
        }
        $this->targetPath = FileSystem::safePath($targetPath);
    }

    /**
     * Compiles sources and place results in target path.
     * @param string[] $sourcePaths List of paths to look for sources to compile
     * @param string[] $excludes Paths to exclude
     * @param bool $rebuild true to force compilation, false to avoid recompiling if target file already exists
     */
    function compile($sourcePaths, $excludes = array(), $rebuild = false)
    {
        $authorizedExts = $this->getAuthorizedExtensions();
        foreach ($sourcePaths as $sourcePath) {
            $files = array();
            FileSystem::listPath($sourcePath, $files, $sourcePath, $excludes);
            foreach ($files as $file) {
                if (in_array(strtolower(strrchr($file, '.')), $authorizedExts)) {
                    $targetFile = $this->getTargetFilePath($file);
                    if (!file_exists($targetFile) || $rebuild) {
                        $this->compileFile($file);
                    }
                }
            }
        }
    }

    /**
     * @param string $sourceFile Source file to use to guess target file name
     * @return string Target file name in the target path
     * @throws CompilerException If source file doesn't exist
     */
    protected function getTargetFilePath($sourceFile)
    {
        if (!is_file($sourceFile)) {
            throw new CompilerException('File ' . $sourceFile . ' cannot be compiled because it does not exist.');
        }
        $sourceFileName = FileSystem::fileName($sourceFile);
        $targetFile = $this->targetPath . substr($sourceFileName, 0, strrpos($sourceFileName, '.')) . $this->getTargetExtension();
        return $targetFile;
    }

    /**
     * @param string[] $importDir Directory to consider as a library
     */
    public function setImportDirs(array $importDir)
    {
        // Implement if you need
    }
}
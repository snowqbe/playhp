<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Lang\Compilation;

use lessc;
use PlayHP\IO\FileSystem;

/**
 * Enables compilation of LESS sources from within a given application, depending on run mode.
 *
 * @package PlayHP\Lang\Compilation
 */
class LessCompiler extends AbstractCompiler
{

    /**
     * @var lessc LESS compiler
     */
    private $_compiler;

    /**
     * @inheritdoc
     */
    function __construct($targetPath)
    {
        parent::__construct($targetPath);
        $this->_compiler = new lessc();
    }

    /**
     * @inheritdoc
     */
    public function setImportDirs($importDirs)
    {
        $this->_compiler->setImportDir($importDirs);
    }


    /**
     * @inheritdoc
     */
    protected function compileFile($sourceFile)
    {
        $targetFile = $this->getTargetFilePath($sourceFile);
        try {
            $this->_compiler->compileFile($sourceFile, $targetFile);
        } catch (\Exception $e) {
            throw new CompilerException('Compilation error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    protected function getAuthorizedExtensions()
    {
        return array('.less');
    }

    /**
     * @return string The target file's extension
     */
    protected function getTargetExtension()
    {
        return '.css';
    }
}
<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Lang\Compilation;

use PlayHP\IO\FileSystem;
use scssc;

/**
 * Enables compilation of SASS sources from within a given application, depending on run mode.
 *
 * @package PlayHP\Lang\Compilation
 */
class SassCompiler extends AbstractCompiler
{

    private $_compiler;

    function __construct($targetPath)
    {
        parent::__construct($targetPath);
        $this->_compiler = new scssc();
    }

    /**
     * @inheritdoc
     */
    protected function compileFile($sourceFile)
    {
        $targetFile = $this->getTargetFilePath($sourceFile);
        try {
            $sourceContent = file_get_contents($sourceFile);
            $result = $this->_compiler->compile($sourceContent, FileSystem::fileName($sourceFile));
            file_put_contents($targetFile, $result);
        } catch (\Exception $e) {
            throw new CompilerException('Compilation error: ' . $e->getMessage());
        }
    }

    /**
     * @inheritdoc
     */
    protected function getAuthorizedExtensions()
    {
        return array('.sass', '.scss');
    }

    /**
     * @return string The target file's extension
     */
    protected function getTargetExtension()
    {
        return '.css';
    }
}
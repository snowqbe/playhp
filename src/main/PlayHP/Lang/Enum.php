<?php

namespace PlayHP\Lang;

/**
 * Enumeration
 */
class Enum
{
    /**
     * @param mixed $enum Value of the enumeration
     * @return Enum Parsed enumeration
     */
    public static function parse($enum)
    {
        $class = get_called_class();
        $vars = get_class_vars($class);
        if (array_key_exists($enum, $vars)) {
            return $vars[$enum];
        }
        return null;
    }

    /**
     * Initializes enumeration values
     */
    public static function init()
    {
        $className = get_called_class();
        $consts = get_class_vars($className);
        foreach ($consts as $constant => $value) {
            if ($className::$$constant === null) {
                $constantValue = $constant;
                $constantValueName = $className . '::' . $constant . '_VALUE';
                if (defined($constantValueName)) {
                    $constantValue = constant($constantValueName);
                }
                $className::$$constant = new $className($constantValue);
            }
        }
    }

    /**
     * @param mixed $value Enumeration value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }
}

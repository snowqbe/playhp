<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Lang;


/**
 * PHP 5.3.0 SessionHandler equivalent for creating session managers
 * @package PlayHP\Lang
 */
abstract class SessionManager
{
    function __construct()
    {
        $this->register();
    }

    protected function register()
    {
        session_set_save_handler(
            array($this, 'open'),
            array($this, 'close'),
            array($this, 'read'),
            array($this, 'write'),
            array($this, 'destroy'),
            array($this, 'gc')
        );
    }

    abstract function open($savePath, $sessionName);

    abstract function close();

    abstract function read($sessionId);

    abstract function write($sessionId, $data);

    abstract function destroy($sessionId);

    abstract function gc($lifetime);
}
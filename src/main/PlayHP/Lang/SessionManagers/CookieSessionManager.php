<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Lang\SessionManagers;


use PlayHP\Lang\SessionManager;
use PlayHP\PlayHP;

class CookieSessionManager extends SessionManager
{

    public $sessionName;

    public function open($savePath, $sessionName)
    {
        return true;
    }

    public function close()
    {
        return true; // Nothing to do
    }

    public function read($sessionId)
    {
        if (isset($_COOKIE[$sessionId])) {
            return $_COOKIE[$sessionId];
        }
        return null;
    }

    public function write($sessionId, $data)
    {
        if (!isset($_COOKIE[$sessionId])) {
            $settings = PlayHP::settings();
            $expiration = $settings->sessionTimeout * 60;
            setcookie($sessionId, $data, time() + $expiration);
        } else {
            $_COOKIE[$sessionId] = $data;
        }
    }

    public function destroy($sessionId)
    {
        setcookie($sessionId);
    }

    public function gc($lifetime)
    {
        return true; // Nothing to do
    }
}
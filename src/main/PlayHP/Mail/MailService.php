<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Mail;

use SimpleXMLElement;

class MailService
{

    /**
     * @var \SimpleXMLElement XML configuration
     */
    private $_conf;

    private $_from;
    private $_fromName;
    private $_replyTo;
    private $_replyToName;
    private $_sendMethod;
    private $_smtpHosts;
    private $_smtpUser;
    private $_smtpPass;
    private $_smtpPort;
    private $_smtpEnc;

    function __construct(SimpleXMLElement $conf)
    {
        $this->_conf = $conf;
        $this->initDefaults($conf);
    }


    /**
     * Sends an email
     * @param string $to To address
     * @param string $subject
     * @param string $body Message body in HTML, UTF-8 encoded
     * @param string $textAlt Message in text format, UTF-8 encoded
     * @param string $basedir Base directory for fetching images from mails. Defaults to APP_ROOT_PATH
     * @param null $bcc BCC recipient. Null for none.
     * @param array $attachements List of mail attachments. Nothing by default
     * @param string $from From address. Null for default configuration
     * @param string $fromName From displayed name. Null for default configuration
     */
    public function send($to, $subject, $body, $textAlt, $basedir = null, $bcc = null, $attachements = array(), $from = null, $fromName = null)
    {
        $mail = new \PHPMailer();
        if ($basedir === null) {
            $basedir = APP_ROOT_PATH;
        }
        $this->setDefaults($mail, $from, $fromName);
        $mail->AddAddress($to);
        $mail->Subject = $subject;
        //$mail->Encoding = 'UTF-8';
//        $mail->IsHTML();
        $mail->MsgHTML($body, $basedir);
        $mail->AltBody = $textAlt;
        if ($bcc !== null) {
            $mail->AddBCC($bcc);
        }
        if (is_array($attachements)) {
            foreach ($attachements as $attachment) {
                $mail->AddAttachment($attachment);
            }
        }
        $mail->Send();
    }

    /**
     * @param \PHPMailer $mail
     * @param string $from
     * @param string $fromName
     */
    private function setDefaults($mail, $from = null, $fromName = null)
    {
        $mail->From = $from !== null ? $from : $this->_from;
        $mail->FromName = $fromName !== null ? $fromName : $this->_fromName;
        $mail->AddReplyTo($this->_replyTo, $this->_replyToName);
        switch ($this->_sendMethod) {
            case 'sendmail':
                $mail->IsSendmail();
                break;
            case 'smtp':
                $mail->IsSMTP();
                if ($this->_smtpEnc) {
                    $mail->SMTPSecure = $this->_smtpEnc;
                }
                $mail->Host = implode(';', $this->_smtpHosts);
                if ($this->_smtpUser) {
                    $mail->Username = $this->_smtpUser;
                    $mail->Password = $this->_smtpPass;
                }
                break;
            default:
                break;
        }
    }

    /**
     * @param SimpleXMLElement $conf
     */
    private function initDefaults(SimpleXMLElement $conf)
    {
        $this->_from = (string)$conf->from['address'];
        $this->_fromName = (string)$conf->from['name'];
        $this->_replyTo = (string)$conf->replyTo['address'];
        $this->_replyToName = (string)$conf->replyTo['name'];
        if ($conf->smtp) {
            $this->_sendMethod = 'smtp';
            if ($conf->smtp->encryption) {
                $this->_smtpEnc = (string)$conf->smtp->encryption;
            }
            if ($conf->smtp->port) {
                $this->_smtpPort = (int)$conf->smtp->port;
            } else {
                switch ($this->_smtpEnc) {
                    case 'tls':
                        $this->_smtpPort = 587;
                        break;
                    case 'ssl':
                        $this->_smtpPort = 465;
                        break;
                    default:
                        $this->_smtpPort = 25;
                        break;
                }
            }
            $this->_smtpUser = (string)$conf->smtp->username;
            $this->_smtpPass = (string)$conf->smtp->password;
            $this->_smtpHosts = array();
            foreach ($conf->smtp->host as $hostXml) {
                $this->_smtpHosts[] = (string)$hostXml;
            }
        } else if ($conf->smtp) {
            $this->_sendMethod = 'sendmail';
        } else {
            $this->_sendMethod = 'default';
        }
    }

}
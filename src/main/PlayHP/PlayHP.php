<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP;


use PlayHP\Controllers\Rendering\Renderable;
use PlayHP\Handlers\Handler;
use PlayHP\Handlers\HandlerType;
use PlayHP\I18n\Locale;
use PlayHP\IO\FileSystem;
use PlayHP\Ioc\ObjectPool;
use PlayHP\Lang\ClassLoader;
use PlayHP\Lang\Compilation\CompilerException;
use PlayHP\Lang\Compilation\LessCompiler;
use PlayHP\Lang\Compilation\SassCompiler;
use PlayHP\Lang\SessionManager;
use PlayHP\Mail\MailService;
use PlayHP\Routing\Route;
use PlayHP\Routing\Router;
use PlayHP\Security\Crypto\Crypto;
use PlayHP\Security\SecurityException;

// This files contains utility methods used by PlayHP

require_once dirname(__FILE__) . '/../../_utilities.php';

/**
 * Main class, in charge of loading all that's needed and initializing the framework parts
 */
class PlayHP
{

    //--------------------------------------------------------------------------------------------------------
    // STATIC DECLARATIONS
    //--------------------------------------------------------------------------------------------------------

    /**
     * gettext domain for PlayHP strings
     * @see #_p
     */
    const GETTEXT_DOMAIN = 'playhp';
    const PROFILE_TOP_NB = 5;

    /**
     * @var bool Flag telling that the application is in profile mode
     */
    static $_isProfiling = true;

    /**
     * @var \SimpleXMLElement
     */
    static $_conf;

    /**
     * @var PlayHP
     */
    private static $_inst = null;

    /**
     * Flag indicating that PlayHP is initialized
     * @var bool
     */
    private static $_isInitialized = false;

    /**
     * Flag telling if PlayHP has begun rendering or not
     * @var bool
     */
    private static $_isRendering = false;

    /**
     * @var array List of traces to output in DEV mode
     */
    private static $_traces = array();


    /**
     * List of time profiling times indexed by step name
     * @var array
     */
    private static $_profileTicks = array();

    /**
     * Retrieves play configuration information
     * @return PlayHPSettings
     */
    public static function settings()
    {
        $inst = self::inst();
        return $inst->_settings;
    }

    /**
     * Traces a message in DEV mode
     * @param mixed $msg Object to trace
     */
    public static function trace($msg)
    {
        $toTrace = '';
        $bufferingActive = count(ob_list_handlers()) > 0;
        if (defined('P_MODE') && P_MODE == 'prod') {
            return;
        }
        $result = debug_backtrace(false);
        if (count($result) > 2) {
            list(, $traceCall, $callerInfos) = $result;
        } else {
            $traceCall = $result[0];
            $callerInfos = $result[0];
        }
        $toTrace .= substr($traceCall['file'], strrpos($traceCall['file'], '/') + 1) . ':' . $traceCall['line'] . ' in ' . $callerInfos['function'] . '()';
        $previous = '';
        if ($bufferingActive) {
            $previous = ob_get_clean();
        }
        ob_start();
        var_dump($msg);
        $toTrace .= ob_get_clean();
        echo $toTrace;
        if ($bufferingActive) {
            ob_start();
            echo $previous;
        }
    }

    /**
     * Triggers an error
     * @param mixed $msg Data to display to user
     * @param string $level
     * @param int $httpStatus
     * @param bool $echo Echo output or return result
     * @return mixed|null Content to render
     */
    public static function error($msg, $level = P_DEV, $httpStatus = 500, $echo = true)
    {
        $fileTemplate = P_VIEWS_PATH . "default.html";
        switch ($httpStatus) {
            case 404:
                if (!self::callHandlers(HandlerType::$NotFound)) {
                    return null;
                }
                header("HTTP/1.1 404 Not Found");
                if (file_exists(APP_VIEWS_PATH . 'pages/404.php')) {
                    $fileTemplate = APP_VIEWS_PATH . 'pages/404.php';
                }
                break;
            case 503:
                if (!self::callHandlers(HandlerType::$Unauthorized)) {
                    return null;
                }
                header("HTTP/1.1 503 Forbidden");
                if (file_exists(APP_VIEWS_PATH . 'pages/503.php')) {
                    $fileTemplate = APP_VIEWS_PATH . 'pages/503.php';
                }
                break;
            case 500:
                if (!self::callHandlers(HandlerType::$Error)) {
                    return null;
                }
                header("HTTP/1.1 500 Internal Server Error");
                if (file_exists(APP_VIEWS_PATH . 'pages/500.php')) {
                    $fileTemplate = APP_VIEWS_PATH . 'pages/500.php';
                }
                break;
            case 301:
                header("HTTP/1.1 301 Moved Permanently");
                break;
        }
        // TODO: provide an easy way to customize error pages
        $debugContent = ob_get_clean();
        $defaultPageContent = file_get_contents($fileTemplate);
        $content = str_replace("{TITLE}", "Error", $defaultPageContent);
        $msg = "<div class='error'>$msg</div>";
        if (PlayHP::isDevMode() && !empty($debugContent)) {
            $msg .= '<br/><h1>Debug content</h1>' . $debugContent;
        }
        $content = str_replace("{CONTENT}", $msg, $content);
        if ($level == P_USER || self::isDevMode()) {
            if ($echo) {
                echo $content;
                exit;
            }
        }
        if (!$echo) {
            return $content;
        }
        exit;
    }


    /**
     * Checks for PHP config compatibility issues and defines all path variables
     * @return array The list of check errors
     */
    public static function initCheck()
    {
        if (defined('P_TEST_MODE')) {
            return array();
        }
        // No extension to check for now
        $appPath = dirname($_SERVER["SCRIPT_FILENAME"]);

        // File structure integrity
        $pathsToCheck = array(
            "views",
            "conf",
            "static",
            "src",
        );
        $appPathsToCheck = array(
            "Controllers",
            "Models",
        );
        $errors = array();
        if (!is_file("$appPath/conf/application.xml")) {
            $errors[] = 'No configuration file found: ' . $appPath . '/conf/application.xml';
            return $errors;
        }
        self::$_conf = simplexml_load_file($appPath . '/conf/application.xml');
        $appId = self::$_conf['appId'];
        define('APP_ID', $appId);
        foreach ($pathsToCheck as $path) {
            if (!is_dir("$appPath/$path")) {
                $errors[] = "$path path is missing";
            } else if ($path == "src") {
                if (!is_dir("$appPath/src/main")) {
                    $errors[] = "No main source path found!";
                } else if (!is_dir("$appPath/src/test")) {
                    $errors[] = "No test source path found!";
                }
                if ($appId != 'PlayHP') {
                    foreach ($appPathsToCheck as $appSubFolder) {
                        if (!is_dir("$appPath/src/main/$appId/$appSubFolder")) {
                            $errors[] = "$path/$appSubFolder path is missing";
                        }
                    }
                }
                if (is_dir("$appPath/src/test/")) {
                    $appContents = array();
                    FileSystem::getDirs("$appPath/src/test/", $appContents, array(), "$appPath/src/test/");
                    if (count($appContents) == 0) {
                        $errors[] = "No main test folder in the /src/test path is present. You must create a base folder representing your app's ID, usually using the 'Test' suffix to avoid confusion";
                    } else {
                        $appId = key($appContents);
                        define('APP_TEST_ID', $appId);
                    }
                }
            }
        }

        // Configuration file
        if (empty($errors)) {
            define('CONF_PATH', $appPath . '/conf/');
            $confFile = CONF_PATH . 'application.xml';
            $routesFile = CONF_PATH . 'routes.xml';
            if (!file_exists($confFile)) {
                $errors[] = 'Configuration file ' . CONF_PATH . 'playhp.xml is missing';
            } else {
                // TODO: validate configuration using an XSD schema
                define('CONF_FILE', $confFile);
                define('ROUTES_FILE', $routesFile);
                define('APP_STATIC_PATH', $appPath . '/static/');
                define('APP_ROOT_PATH', $appPath . '/');
                define('APP_SRC_PATH', $appPath . '/src/main/');
                define('APP_TEST_PATH', $appPath . '/src/test/');
                define('APP_TMP_PATH', $appPath . '/tmp');
                define('APP_VIEWS_PATH', $appPath . '/views/');
                define('APP_LAYOUTS_PATH', APP_VIEWS_PATH . 'layouts/');
                define('APP_PAGES_PATH', APP_VIEWS_PATH . 'pages/');
                define('APP_PARTS_PATH', APP_VIEWS_PATH . 'parts/');
                define('APP_PATH', APP_SRC_PATH . $appId . '/');
                define('APP_CONTROLLERS_PATH', APP_PATH . 'controllers/');
                define('APP_MODELS_PATH', APP_PATH . 'models/');

                $prefix = $_SERVER['SCRIPT_NAME'];
                $indexPos = strpos($prefix, '/index.php');
                if ($indexPos > 0) {
                    $prefix = substr($prefix, 0, $indexPos);
                }
                $http = Router::getHttpProtocol();
                define('HTTP_BASE', $http . '://' . $_SERVER['HTTP_HOST'] . $prefix);
                define('BASE', $prefix);
                define('STATIC_BASE', $prefix . '/static/');

                // Initialize the class loader first
                ClassLoader::inst()->init(array(
                    P_SRC_PATH,
                    APP_SRC_PATH,
                    APP_TEST_PATH,
                ));
            }
        } else {
            PlayHP::error(implode('<br/>', $errors));
        }

        return $errors;
    }

    /**
     * Returns current instance
     * @return PlayHP
     */
    static function inst()
    {
        if (self::$_inst === null) {
            self::profileStep('Init check');
            $errors = self::initCheck();
            if (!empty($errors)) {
                trace($errors);
                self::error($errors);
                exit;
            } else {
                self::$_isInitialized = true;
                self::profileStep('Instanciation');
                self::$_inst = new PlayHP();
                self::$_inst->init();
            }
        }
        return self::$_inst;
    }

    /**
     * Launches app
     */
    public static function run()
    {
        $inst = self::inst();
        self::$_isProfiling = false;
        $inst->render();
    }

    /**
     * Launches app in profile mode
     */
    public static function profile()
    {
        $inst = self::inst();
        self::$_isProfiling = true;
        $inst->render();
    }

    /**
     * Changes target page to render
     * @param string $path New page ID to use for rendering
     */
    public static function forward($path)
    {
        $inst = self::inst();
        if (substr($path, 0, 1) != '/') {
            $path = '/' . $path;
        }
        $inst->render($path);
    }

    /**
     * Returns the PlayHP router
     * @return Routing\Router
     */
    public static function router()
    {
        $inst = self::inst();
        return $inst->getRouter();
    }

    /**
     * Returns the classloader
     * @return ClassLoader
     */
    public static function classLoder()
    {
        return ClassLoader::inst();
    }

    /**
     * Returns the PlayHP object pool
     * @return Ioc\ObjectPool
     */
    public static function pool()
    {
        $inst = self::inst();
        return $inst->getPool();
    }

    public static function devRoutes()
    {
        $runRoute = new Route();
        $runRoute->setPathAndTarget('/@runTests', 'PlayHP\Testing\Tester.runTestSuite');
        $showRoute = new Route();
        $showRoute->setPathAndTarget('/@tests', 'PlayHP\Testing\Tester.showTests');
        return array(
            '/@tests' => $showRoute,
            '/@runTests' => $runRoute,
        );
    }

    private static function addDevRoutes()
    {
        $router = self::router();
        $devRoutes = self::devRoutes();
        foreach ($devRoutes as $rt) {
            $router->addRoute($rt);
        }
    }

    public static function isDevMode()
    {
        return defined('P_MODE') && (P_MODE == 'dev' || P_MODE == 'playhp_test');
    }

    /**
     * Gets or sets an app setting
     * @param null|string $key Configuration key
     * @param null $setValue Value to set
     * @return string|array()
     */
    public static function config($key = null, $setValue = null)
    {
        $inst = self::$_inst;
        if ($setValue !== null) {
            $inst->setAppSetting($key, $setValue);
        } else {
            return $inst->getAppSettings($key);
        }
    }

    /**
     * Launch PlayHP in test mode (for unit testing rendering results only)
     */
    public static function test()
    {
        $router = self::router();
        // Routes declared for CURL based testing
        $router->addRoute('/test/defaultRender', 'GET', 'PlayHPTests\Controllers\Dummies\TestController.defaultRender');
        $router->addRoute('/test/layoutRender', 'GET', 'PlayHPTests\Controllers\Dummies\TestController.layoutRender');
        $router->addRoute('/test/textRender', 'GET', 'PlayHPTests\Controllers\Dummies\TestController.textRender');
        $router->addRoute('/test/jsonRender', 'GET', 'PlayHPTests\Controllers\Dummies\TestController.jsonRender');
        $router->addRoute('/test/putOperation', 'PUT', 'PlayHPTests\Controllers\Dummies\TestController.doPut');
        $router->addRoute('/test/delOperation', 'DELETE', 'PlayHPTests\Controllers\Dummies\TestController.doDelete');
        $router->addRoute('/test/postOperation', 'POST', 'PlayHPTests\Controllers\Dummies\TestController.doPost');
        self::run();
    }

    //--------------------------------------------------------------------------------------------------------
    // INSTANCE DECLARATIONS
    //--------------------------------------------------------------------------------------------------------

    /**
     * @var Router
     */
    private $_router;

    /**
     * Pool of objects to enable dependency injection
     * @var ObjectPool
     */
    private $_pool;

    /**
     * Current route
     * @var Route
     */
    private $_currentRoute;

    /**
     * Language code
     * @var string
     */
    private $_locale;

    /**
     * Session manager
     * @var SessionManager
     */
    private $_sessionManager;

    /**
     * PlayHP configuration
     * @var PlayHPSettings
     */
    private $_settings;

    /**
     * List of PlayHP handlers
     * @var Handler[]
     */
    private $_handlers = array();

    /**
     * Initializes the configuration
     */
    private function __construct()
    {
        if (self::$_isInitialized) {
            $this->_configKeys = array();
            $results = self::$_conf->xpath('//config/entry');
            foreach ($results as $entry) {
                $this->_configKeys[strval($entry['key'])] = strval($entry['value']);
            }
            $this->routesConfig = simplexml_load_file(ROUTES_FILE);
        }
    }

    /**
     * Initialize variables
     */
    private function init()
    {
        self::profileStep('Initializing session and languages');
        $appId = trim(strval(self::$_conf['appId']));

        // Initialize settings
        $this->_settings = new PlayHPSettings(self::$_conf['salt']);
        $this->_settings->appId = $appId;
        $httpsPort = intval(self::$_conf['httpsPort']);
        if ($httpsPort > 0) {
            $this->_settings->httpsPort = $httpsPort;
        }
        $httpPort = intval(self::$_conf['httpPort']);
        if ($httpPort > 0) {
            $this->_settings->httpPort = $httpPort;
        }
        $this->_settings->sessionTimeout = intval(self::$_conf['sessionTimeout']);
        $this->_settings->crypto = (string)self::$_conf['crypto'];

        // Session management (default's to cookie)
        // TODO: correct the cookie session manager
//        $this->_sessionManager = new CookieSessionManager();

        // Session
        $sessionName = 'PlayHP-' . $appId;
        session_name($sessionName);
        session_start();

        // Language
        $this->initLanguage();

        // Compile
        $this->compile();
    }

    /**
     * Renders a page
     */
    public function render($path = null)
    {
        self::profileStep('start');
        self::$_isRendering = false;

        // Call pre-initialization handlers
        self::callHandlers(HandlerType::$PreInit);

        // Get route according to current situation
        /* @var Route $route */
        $route = null;
        try {
            if ($path !== null) {
                $route = $this->_router->getRoute($path);
            } else {
                $route = $this->_router->guessCurrentRoute();
            }
        } catch (SecurityException $e) {
            // User has no right to access the route
            self::error("User is not allowed to access route $path", P_USER, 503);
            return;
        }
        if ($route === null) {
            self::error("Page not found $path", P_USER, 404);
            return;
        }

        /* @var Renderable $renderable */
        $renderable = $route->invoke();

        self::profileStep('Controller invoked');

        // Invoke the pre-invoke handlers
        self::callHandlers(HandlerType::$PreRender);

        // TODO: only close session if session managed by cookie
        //session_write_close(); // To enable cookie managed session

        self::$_isRendering = true; // Will directly output traces
        if (self::isDevMode()) {
            foreach (self::$_traces as $trace) {
                echo $trace;
            }
        }
        if ($renderable->httpCode == 200) {
            $this->_currentRoute = $renderable->route;
            $renderable->render($this->_currentRoute->renderingEngine);
        } else {
            self::error($renderable->rawResult, P_USER, $renderable->httpCode);
        }
        self::profileStep('Rendered');

        // Call post render handlers
        self::callHandlers(HandlerType::$PostRender);

        $this->renderProfilingInfo();
    }

    /**
     * @param string $lang Language code to use
     * @param string $appDomain Configured application language domain
     */
    private function setLanguage($lang, $appDomain)
    {
        $this->_locale = new Locale($lang);
        $lang .= ".UTF-8";
        putenv("LANG=$lang");
        setlocale(LC_ALL, $lang);

        // Initialize gettext
        $domains = array(self::GETTEXT_DOMAIN);
        if ($appDomain !== null) {
            $domains[] = $appDomain;
        }
        foreach ($domains as $domain) {
            bindtextdomain($domain, P_ROOT . '/locale');
            bind_textdomain_codeset($domain, "UTF-8");
        }
        if ($appDomain !== null) {
            textdomain($appDomain);
        }
    }

    /**
     * Initializes the language
     */
    private function initLanguage()
    {
        $fallbackLanguage = null;
        $serverLanguage = 'en_US';
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $serverLanguage = str_replace('-', '_', substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 5));
        }
        $lang = isset($_REQUEST["lang"]) ? $_REQUEST["lang"] : $serverLanguage;
        $appDomain = isset(self::$_conf->languages["domainName"]) ? strval(self::$_conf->languages["domainName"]) : null;
        $languagesConf = self::$_conf->languages->language;
        if ($languagesConf !== null) {
            foreach ($languagesConf as $language) {
                if ($language['code'] == $lang) {
                    $this->setLanguage($lang, $appDomain);
                    $lang = null;
                    break;
                } else if (strtolower(substr($language['code'], 0, 2)) == strtolower($lang) && $fallbackLanguage === null) {
                    $fallbackLanguage = $language['code'];
                }
            }
        }
        if ($lang !== null) {
            // Fallback language
            if ($fallbackLanguage === null) {
                $fallbackLanguage = 'en_US';
            }
            $this->setLanguage($fallbackLanguage, $appDomain);
        }
    }

    /**
     * Initializes the router
     */
    private function compile()
    {
        self::profileStep('Initializing router');

        // Fetch routes configuration
        $this->_router = new Router();
        $this->_router->init($this->routesConfig);

        $crypto = new Crypto($this->_settings->getSalt(), $this->_settings->crypto);

        // Add test routes (prior to parsing to avoid conflicts)
        if (self::isDevMode()) {
            self::addDevRoutes();
        }

        // Parse configured routes
        try {
            $this->_router->parseRoutes();
        } catch (\Exception $e) {
            self::error("Error while parsing routes. Failed with message:\n" . fmt($e));
        }
        self::profileStep('Initializing object pool');

        // Mail
        /* @var \SimpleXMLElement $mailConf */
        $mailConf = self::$_conf->mail;
        $mailService = new MailService($mailConf);

        // Initialize object pool and add default objects
        if (is_file(CONF_PATH . 'pool.xml')) {
            $poolConf = simplexml_load_file(CONF_PATH . 'pool.xml');
        } else {
            $poolConf = self::$_conf->pool;
        }
        $this->_pool = new ObjectPool($poolConf);
        try {
            $this->_pool->initializePool();
            self::profileStep('Attaching locale, class loader and router to pool');
            $this->_pool->attach('PlayHP\Locale', $this->_locale);
            $this->_pool->attach('PlayHP\ClassLoader', ClassLoader::inst());
            $this->_pool->attach('PlayHP\Router', $this->_router);
            $this->_pool->attach('PlayHP\Crypto', $crypto);
            $this->_pool->attach('PlayHP\MailService', $mailService);

            // Automatically register handlers declared in the object pool
            $poolHandlers = $this->_pool->getObjects('PlayHP\Handlers\Handler');
            foreach ($poolHandlers as $poolHandler) {
                if ($poolHandler->autoBind) {
                    $this->bind($poolHandler);
                }
            }
            self::profileStep('Handlers bound');
        } catch (\Exception $e) {
            self::error("Error while initializing object pool. Failed with message:\n" . fmt($e));
        }

        // Launch compilers depending on run mode
        self::profileStep('Compiling LESS/SASS sources...');
        try {
            // Launch every time
            $lessSrc = APP_ROOT_PATH . 'src/less/';
            if (is_dir($lessSrc)) {
                $lessTarget = APP_ROOT_PATH . 'static/css/';
                $lessComp = new LessCompiler($lessTarget);
                $lessImports = $lessSrc . 'imports/';
                $lessComp->setImportDirs(array($lessImports));
                $lessComp->compile(array($lessSrc), array($lessImports));
            }
            $sassSrc = APP_ROOT_PATH . 'src/sass/';
            if (is_dir($sassSrc)) {
                $sassTarget = APP_ROOT_PATH . 'src/sass/';
                $sassComp = new SassCompiler($sassTarget);
                $sassImports = $sassSrc . 'imports/';
                $sassComp->setImportDirs(array($sassImports));
                $sassComp->compile(array($sassSrc), array($sassImports));
            }
        } catch (CompilerException $ex) {
            self::error($ex->getMessage());
        }
        self::profileStep('LESS/SASS compilation finished');
    }

    /**
     * Invoke all handlers of given type
     * @param HandlerType $handlerType Type of handlers to invoke
     * @return bool true to proceed with default behavior, false otherwise
     */
    public static function callHandlers(HandlerType $handlerType)
    {
        if (!self::$_isInitialized) {
            return true;
        }
        $inst = self::inst();
        $handlers = $inst->getHandlers($handlerType);
        $proceedWithDefault = true;
        foreach ($handlers as $handler) {
            $proceedWithDefault = $proceedWithDefault && $handler->invoke();
        }
        return $proceedWithDefault;
    }

    /**
     * Unbinds a handler to PlayHP
     * @param Handler $handler Handler to unbind
     * @see Handler
     */
    public function unbind(Handler $handler)
    {
        if (($key = array_search($handler, $this->_handlers)) !== false) {
            unset($this->_handlers[$key]);
        }
    }

    /**
     * Binds a handler to PlayHP
     * @param Handler $handler Handler to bind
     * @see Handler
     */
    public function bind(Handler $handler)
    {
        $this->_handlers[] = $handler;
    }

    /**
     * @return Routing\Router
     */
    public function getRouter()
    {
        return $this->_router;
    }

    /**
     * @return ObjectPool
     */
    public function getPool()
    {
        return $this->_pool;
    }

    /**
     * The key value pairs of defined configuration
     * @param null|string $key Key to get from configuration
     * @return string|array
     */
    public function getAppSettings($key = null)
    {
        if ($key === null) {
            return $this->_configKeys;
        } else if (array_key_exists($key, $this->_configKeys)) {
            return $this->_configKeys[$key];
        }
        return null;
    }

    /**
     * The key value pairs of defined configuration
     * @param null|string $key Key to get from configuration
     * @param mixed $value App setting to set
     */
    public function setAppSetting($key, $value)
    {
        $this->_configKeys[$key] = $value;
    }

    /**
     * Returns all handlers of given type. Use null for getting all handler types.
     * @param null|HandlerType $handlerType Type of the handlers to return
     * @return Handler[] The list of registered handlers of given type
     */
    public function getHandlers($handlerType = null)
    {
        if ($handlerType === null) {
            return $this->_handlers;
        }
        $result = array();
        foreach ($this->_handlers as $handler) {
            if ($handler->getType() === $handlerType) {
                $result[] = $handler;
            }
        }
        return $result;
    }

    /**
     * Adds a step in the profiling queue to monitor performance
     * @param string $stepName Name of the profiling step to save
     */
    public static function profileStep($stepName)
    {
        if (self::$_isProfiling) {
            self::$_profileTicks[$stepName] = microtime(true) * 1000;
        }
    }

    /**
     * Renders the profiling information
     */
    protected function renderProfilingInfo()
    {
        if (self::$_isProfiling) {
            echo '<pre>';
            echo "PROFILING INFO:\n-----------------\n";
            echo sprintf("Time taken before business code : %.3f ms\n-----------------\n", self::$_profileTicks['Route/RouteTarget/ControllerCall/Invoking controller'] - self::$_profileTicks['Init check']);
            $lastTime = 0;
            $firstTime = 0;
            $lastTitle = '';
            foreach (self::$_profileTicks as $title => $time) {
                if ($lastTime == 0) {
                    echo $title . "\n";
                    $lastTitle = $title;
                    $lastTime = $time;
                    $firstTime = $time;
                } else {
                    $timeMs = $time;
                    $elapsedTime = $timeMs - $lastTime;
                    $topSteps[$lastTitle] = $elapsedTime;
                    echo sprintf("... %.3f ms ...\n[%.3f ms] -- %s\n", $elapsedTime, ($timeMs - $firstTime), $title);
                    $lastTime = $timeMs;
                    $lastTitle = $title;
                }
            }
            arsort($topSteps);
            echo "-----------------\nTop " . self::PROFILE_TOP_NB . " steps in the process:\n-----------------\n";
            $i = 1;
            foreach ($topSteps as $title => $time) {
                echo sprintf("  $i) [%.3f ms] %s\n", $time, $title);
                if (++$i > self::PROFILE_TOP_NB) {
                    break;
                }
            }
            echo '</pre>';
        }
    }


}
<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP;

/**
 * Main PlayHP application settings
 * @package PlayHP
 */
class PlayHPSettings
{
    /**
     * The application's ID
     * @var string
     */
    public $appId;

    /**
     * The session timeout
     * @var int
     */
    public $sessionTimeout;

    /**
     * The crypto algorithm to use
     * @var string
     */
    public $crypto;

    /**
     * @var int HTTPS port for redirecting
     */
    public $httpsPort = 443;

    /**
     * @var int HTTP port for redirecting properly
     */
    public $httpPort = 80;

    /**
     * The salt to use for encryption
     * @var string
     * @private
     */
    private $_salt;

    /**
     * @param string $salt The salt to use for encryption
     */
    function __construct($salt)
    {
        $this->_salt = $salt;
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        return $this->_salt;
    }

}
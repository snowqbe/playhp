<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Routing;

use Exception;
use PlayHP\Controllers\Controller;
use PlayHP\Controllers\Rendering\RenderingEngine;
use PlayHP\Lang\Annotation;
use PlayHP\PlayHP;
use PlayHP\Routing\Exceptions\RenderException;
use PlayHP\Routing\Exceptions\RoutingException;
use ReflectionClass;

/**
 * Metadata to use for calling a route target
 */
class ControllerCall
{
    /**
     * Query string of the url
     * @var string
     */
    private $_queryString;

    /**
     * The URL which corresponds to controller's call, relative to the application's base URL
     * @var string
     */
    private $_url;

    /**
     * @var Controller The controller instance
     */
    private $_controllerInstance;

    /**
     * @var \ReflectionClass
     */
    public $controllerClass;

    /**
     * @var \ReflectionMethod
     */
    public $controllerMethod;

    /**
     * @var array
     */
    public $methodParams = array();

    /**
     * Invokes the call
     */
    public function invoke(RenderingEngine $engine)
    {
        $controller = $this->getController();
        $controller->renderingEngine = $engine;
        // Prepare method parameters
        $params = array();
        $methodParameters = $this->controllerMethod->getParameters();
        foreach ($methodParameters as $reflectParam) {
            $reflectParamName = $reflectParam->getName();
            if (array_key_exists($reflectParamName, $this->methodParams)) {
                $params[] = $this->methodParams[$reflectParamName];
            } else {
                $params[] = null;
            }
        }

        PlayHP::profileStep('Route/RouteTarget/ControllerCall/Invoking controller');
        $result = null;

        // Pre-render
        try {
            $controller->preinvoke();
        } catch (RoutingException $routingEx) {
            // Break or continue?
            switch ($routingEx->getType()) {
                case RoutingExceptionType::$cancel:
                    return $result;
                case RoutingExceptionType::$continue:
                    break;
            }
        }
        catch (Exception $e) {
            return $result;
        }

        // Invoke
        try {
            $this->controllerMethod->invokeArgs($controller, $params);
        } catch (RenderException $renderException) {
            $result = $renderException->renderable;
        }

        // Post-invocation: prerender
        try {
            $controller->prerender();
            $result->viewVariables = $controller->mergeViewVariables($result->viewVariables);
        } catch (RoutingException $routingEx) {
            // Cancel rendering?
            if ($routingEx->getType() == RoutingExceptionType::$cancel) {
                $result = null;
            }
        }

        return $result;
    }

    /**
     * @return Controller The controller instance
     */
    public function getController()
    {
        if ($this->_controllerInstance === null) {
            $this->_controllerInstance = $this->controllerClass->newInstance();
        }
        return $this->_controllerInstance;
    }

    /**
     * @return string The controller method name
     */
    public function getControllerMethodName()
    {
        if ($this->controllerMethod) {
            return $this->controllerMethod->name;
        }
        return null;
    }

    /**
     * @return string The controller class name
     */
    public function getControllerClassName()
    {
        if ($this->controllerClass) {
            return $this->controllerClass->name;
        }
        return null;
    }

    /**
     * Smooth comparison between provided class name and method name
     * @param string $targetClassName Qualified (or not) class name
     * @param string $targetClassMethod Method name
     * @return bool true if current call matches provided parameters, false otherwise
     */
    public function compareWith($targetClassName, $targetClassMethod)
    {
        $testController = $this->getControllerClassName();
        if (strpos($targetClassName, '\\') == FALSE) {
            $testController = substr($testController, strrpos($testController, '\\') + 1);
        }
        return $testController == $targetClassName && $targetClassMethod == $this->getControllerMethodName();
    }

    /**
     * Updates method parameters with ones parsed from the request
     * @param array $requestParams Parsed request parameters
     */
    public function updateMethodParams($requestParams)
    {
        $paramTypes = Annotation::getParamTypes($this->controllerMethod->getDocComment());
        $parameters = $this->controllerMethod->getParameters();
        foreach ($parameters as $parameter) {
            $paramName = $parameter->name;
            if (array_key_exists($paramName, $requestParams)) {
                $value = $requestParams[$paramName];
                if (array_key_exists($paramName, $paramTypes)) {
                    $this->methodParams[$paramName] = $this->transcodeParam($paramTypes[$paramName], $value);
                } else {
                    $this->methodParams[$paramName] = $value;
                }
            }
        }
    }

    /**
     * @private
     * @todo Move this to ClassLoader?
     * @param ReflectionClass $paramClass
     * @param mixed $object Object to update
     * @param string $propName Name of the property to set
     * @param mixed $propValue Value of the property
     */
    private function setProperty($paramClass, $object, $propName, $propValue)
    {
        $setterName = 'set' . ucfirst($propName);
        if ($paramClass->hasMethod($setterName)) {
            $setter = $paramClass->getMethod($setterName);
            if ($setter->getNumberOfRequiredParameters() == 1) {
                $object->$setterName($propValue);
                return;
            }
        }
        $object->$propName = $propValue;
    }


    /**
     * @private
     * @todo Move this to ClassLoader?
     * Translates an array into a typed object
     * @param string $targetType Target type
     * @param array $arrayValues List of values for the object
     * @return mixed
     */
    private function transcodeParam($targetType, $arrayValues)
    {
        $classLoader = PlayHP::classLoder();
        try {
            $paramClass = $classLoader->getReflectionClass($targetType);
            $newValue = $paramClass->newInstance();
            foreach ($arrayValues as $propName => $propValue) {
                $this->setProperty($paramClass, $newValue, $propName, $propValue);
            }
            return $newValue;
        } catch (Exception $e) {
            return $arrayValues;
        }
    }

    /**
     * @param string $routePath The route path
     * @param string $queryString The query string
     */
    public function setUrl($routePath, $queryString)
    {
        $this->_queryString = ($queryString !== null ? $queryString : '');
        $this->_url = $routePath . $this->_queryString;
    }

    /**
     * @return string The URL query string
     */
    public function getQueryString()
    {
        return $this->_queryString;
    }

    /**
     * @return string The call URL
     */
    public function getUrl()
    {
        return $this->_url;
    }
}

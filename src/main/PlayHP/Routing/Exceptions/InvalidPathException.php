<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Routing\Exceptions;
/**
 * Thrown when route path is invalid
 */
class InvalidPathException extends \Exception
{

    private $_path;

    public function getPath()
    {
        return $this->_path;
    }

    public function __construct($path, $message = "", $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->_path = $path;
    }


}

<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Routing\Exceptions;
/**
 * Thrown when route target is invalid
 */
class InvalidTargetException extends \Exception
{

    private $_target;

    public function getTarget()
    {
        return $this->_target;
    }

    public function __construct($target, $message = "", $code = 0, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->_target = $target;
    }


}

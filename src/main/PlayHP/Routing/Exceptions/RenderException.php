<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Routing\Exceptions;

use PlayHP\Controllers\Rendering\Renderable;

/**
 * Thrown to trigger rendering
 */
class RenderException extends \Exception
{

    /**
     * @var \PlayHP\Controllers\Rendering\Renderable
     */
    public $renderable;

    /**
     * Builds a render exception with the results of an invocation
     * @param \PlayHP\Controllers\Rendering\Renderable $invocationResult
     */
    public function __construct(Renderable $invocationResult)
    {
        $this->renderable = $invocationResult;
    }

}

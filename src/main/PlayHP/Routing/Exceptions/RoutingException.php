<?php

namespace PlayHP\Routing\Exceptions;


use Exception;
use PlayHP\Routing\RoutingExceptionType;

/**
 * Used to change the routing behavior during the prerender pass
 *
 * @package PlayHP\Routing\Exceptions
 */
class RoutingException extends \Exception
{

    /**
     * @var RoutingExceptionType
     */
    private $_type;

    public function __construct(RoutingExceptionType $type)
    {
        parent::__construct('Routing exception: ' . $type->value);
        $this->_type = $type;
    }

    /**
     * @return \PlayHP\Routing\RoutingExceptionType
     */
    public function getType()
    {
        return $this->_type;
    }

}
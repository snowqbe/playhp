<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Routing;

use PlayHP\Lang\Enum;

/**
 * List of supported HTTP methods
 */
class HttpMethod extends Enum
{

    public static $GET;
    public static $POST;
    public static $PUT;
    public static $DELETE;
}

HttpMethod::init();

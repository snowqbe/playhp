<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Routing;


use PlayHP\Routing\Exceptions\InvalidPathException;

/**
 * Parses request to initialize the PlayHP path, http method and request parameters.
 * Request parameters like object.id=value are processed to have an 'object' parameter, whose 'id' property equals 'value'
 * @package PlayHP\Routing
 */
class RequestParser
{

    /**
     * The detected path
     * @var string
     */
    private $_path;

    /**
     * The computed parameters from both request and path
     * @var array
     */
    private $_parameters = array();

    /**
     * The request method
     * @var string
     */
    private $_method;

    /**
     * Detects the path
     * Parses parameters of current request
     * @throws InvalidPathException When invalid parameters query string is provided in detected path (URL is likely invalid)
     */
    public function parseCurrentRequest()
    {
        $path = $this->computePath();
        $method = $_SERVER['REQUEST_METHOD'];
        // Add request params (GET or POST depending on request method)
        switch ($method) {
            default:
                $params = $_GET;
                break;
            case 'POST':
                $params = $_POST;
                break;
        }
        $this->parse($path, $method, $params);
    }

    /**
     * Parses parameters given provided parameters
     * @param string $path The path to parse
     * @param string|null $method The method to use
     * @param array|null $params An initial collection of parameters to use
     */
    public function parse($path, $method = null, $params = null)
    {
        $this->_method = $method;
        $this->_path = $this->parsePathParams($path);

        // Format parameters and update the _parameters collection
        foreach ($params as $paramName => $paramValue) {
            $this->parseRequestParam($paramName, $paramValue);
        }
    }

    /**
     * Updates the parameters collection based on paramName and paramValue
     * @param string $paramName Name of the parameter
     * @param string $paramValue Value of the parameter
     */
    protected function parseRequestParam($paramName, $paramValue)
    {
        $paramMatches = array();
        if (preg_match('|([^\]]+)\[([^\]]*)\]|i', $paramName, $paramMatches)) {
            // There's an array notation
            list(, $paramName, $key) = $paramMatches;
            if (!array_key_exists($paramName, $this->_parameters)) {
                // First element
                $this->_parameters[$paramName] = array();
            }
            if ($key == '') {
                // If no key, just push
                $this->_parameters[$paramName][] = $paramValue;
            } else {
                // Key exists, use it
                $this->_parameters[$paramName][] = $paramValue;
            }
        } else if (preg_match('|([^.]+)_([a-zA-Z0_]{1}[a-zA-Z0-9_]*)|i', $paramName, $paramMatches)) {
            // There's an object notation, set it as an untyped one first
            list(, $methodParamName, $objPropName) = $paramMatches;
            if (!array_key_exists($methodParamName, $this->_parameters)) {
                // First time
                $this->_parameters[$methodParamName] = array();
            }
            $this->_parameters[$methodParamName][$objPropName] = $paramValue;
        } else {
            // Regular parameter
            $this->_parameters[$paramName] = $paramValue;
        }
    }

    /**
     * @return string The currently queried path
     */
    public function computePath()
    {
        $path = false;
        if (isset($_SERVER['REDIRECT_URL'])) {
            $path = substr($_SERVER['REDIRECT_URL'], strlen(BASE));
        } else if (isset($_SERVER['REQUEST_URI'])) {
            $path = substr($_SERVER['REQUEST_URI'], strlen(BASE));
        }
        if ($path === false || $path === '/index.php')
            return '/';
        return $path;
    }

    /**
     * @param string $path Path to parse for getting parameters
     * @throws Exceptions\InvalidPathException
     * @return string The updated path
     */
    private function parsePathParams($path)
    {
        if (strpos($path, '?') > 0) {
            $parsed = parse_url($path);
            if ($parsed === false) {
                throw new InvalidPathException('Invalid path parameters were provided for path: ' . $path);
            }
            $path = $parsed['path'];
            $matches = array();
            $nb = preg_match_all('~([^?&=]+)=([^&]+)*~i', $parsed['query'], $matches);
            if ($nb > 0) {
                foreach ($matches[1] as $i => $paramName) {
                    $paramValue = $matches[2][$i];
                    $this->parseRequestParam($paramName, $paramValue);
                }
            }
        }
        return $path;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->_path;
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->_parameters;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->_method;
    }

}
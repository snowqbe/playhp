<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Routing;

use PlayHP\Controllers\Rendering\Renderable;
use PlayHP\Controllers\Rendering\RenderingEngine;
use PlayHP\Handlers\HandlerType;
use PlayHP\Ioc\ObjectPool;
use PlayHP\PlayHP;
use PlayHP\Routing\Exceptions\InvalidPathException;
use PlayHP\Routing\Exceptions\InvalidTargetException;

/**
 * Controller route to a method execution
 */
class Route
{
    /**
     * Validates a route target
     */
    const ROUTE_TARGET_PATTERN = '#^([\\a-z\{\}_0-9]+)\.([a-z\{\}_0-9]+)$#mi';

    /**
     * Default route to use if non defined
     * @var Route
     */
    private static $DEFAULT_ROUTE;

    /**
     * Path to current route
     * @var string
     */
    public $path;

    /**
     * List of validation errors
     * @var array
     */
    public $validationErrors = array();

    /**
     * Rendered layout
     * @var \PlayHP\Controllers\Rendering\RenderingEngine
     */
    public $renderingEngine;

    /**
     * List of profiles who are allowed to access this route
     * @var array
     */
    public $allowed;

    /**
     * List of profiles who are not allowed to access this route
     * @var array
     */
    public $denied;

    /**
     * @var bool Forces route to be accessed through HTTPS protocol
     */
    public $forceHttps = false;

    /**
     * All detected path parameters
     * @var array
     * @private
     */
    private $pathParams;

    /**
     * HTTP Method for this route
     * @var HttpMethod
     */
    private $httpMethod;

    /**
     * Target for the route
     * @var RouteTarget
     */
    private $routeTarget;

    function __construct()
    {
    }

    /**
     * Set the HTTP method
     * @param HttpMethod $httpMethod HTTP method type
     */
    public function setHttpMethod(HttpMethod $httpMethod)
    {
        $this->httpMethod = $httpMethod;
    }

    /**
     * Sets the route path
     * @param string $routePath The route path
     * @param string $target The route's target
     * @throws Exceptions\InvalidPathException
     * @throws Exceptions\InvalidTargetException
     */
    public function setPathAndTarget($routePath, $target)
    {
        $this->path = $routePath;
        $this->pathParams = array();

        // Check for params
        $matches = array();
        $nbParams = preg_match_all('#\{([a-zA-Z_0-9]+)\}#', $routePath, $matches);
        if ($nbParams) {
            for ($i = 0; $i < count($matches[1]); $i++) {
                $paramName = $matches[1][$i];
                if (in_array($paramName, $this->pathParams)) {
                    throw new InvalidPathException($routePath, "Two parameters with the same name were found in provided path");
                }
                $this->pathParams[] = $paramName;
            }
        }
        $this->routeTarget = new RouteTarget();
        $this->routeTarget->setTargetPath($this->pathParams, $target);
    }

    /**
     * Creates the controller in charge of prerendering
     * @return \PlayHP\Controllers\Controller Created controller class
     */
    public function getController()
    {
        return $this->routeTarget->getController();
    }

    /**
     * Invokes the route to handle treatments and render the result
     * @param null|array $chainedVariables Variables defined from the previous controller invocation in the routing chain
     * @throws \Exception When invalid result is retrieved from controller function invocation
     * @return Renderable The effective element to render after redirections
     */
    public function invoke($chainedVariables = null)
    {
        PlayHP::profileStep('Route/Attaching route, controller and layout to object pool');
        // Initialize pool with standard elements
        $pool = PlayHP::pool();
        $pool->attach('PlayHP\Route', $this);

        $controller = $this->getController();
        $pool->attach('PlayHP\Controller', $controller);

        // Compute layout
        $this->renderingEngine = new RenderingEngine();
        $this->renderingEngine->setMetas($controller->getMetas());
        $pool->attach('PlayHP\RenderingEngine', $this->renderingEngine);

        PlayHP::profileStep('Route/Invoking route target');


        // Invoke the pre-invoke handlers
        PlayHP::callHandlers(HandlerType::$PreInvoke, array($controller));

        // Invoke the target
        /* @var \PlayHP\Controllers\Rendering\Renderable $renderable */
        $renderable = $this->routeTarget->invoke($this->renderingEngine);
        if ($renderable === null) {
            throw new \Exception("Invalid render method result for route");
        }
        if ($renderable->redirection !== null) {
            PlayHP::profileStep('Route/Handling forward');
            $router = PlayHP::router();
            $redirectedRoute = $router->getRoute($renderable->redirection);
            return $redirectedRoute->invoke($pool, $chainedVariables);
        } else {
            PlayHP::profileStep('Route/Returning');
            $renderable->route = $this;
            return $renderable;
        }
    }

    /**
     * @return array The detected path parameters
     */
    public function getPathParams()
    {
        return $this->pathParams;
    }

    /**
     * @return \PlayHP\Routing\Route The default route to use if none defined
     */
    public static function DEFAULT_ROUTE()
    {
        if (self::$DEFAULT_ROUTE === null) {
            self::$DEFAULT_ROUTE = new Route();
            self::$DEFAULT_ROUTE->setPathAndTarget('/', 'PlayHP\Controllers\Bundled\WelcomeController.welcome');
            self::$DEFAULT_ROUTE->setHttpMethod(HttpMethod::$GET);
        }
        return self::$DEFAULT_ROUTE;
    }

    /**
     * Matches provided path against the path pattern and returns the result
     * @param string $path Path to match
     * @param string $method HTTP method the route should match
     * @param array $params List of parameters for calling the route
     * @return ControllerCall valid object to use for initializing the route target if the route matches, false otherwise
     */
    public function matches($path, $method = null, $params = array())
    {
        // Check request method (easy)
        if ($method !== null && $this->httpMethod !== null && strtoupper($method) != strtoupper($this->httpMethod->value)) {
            return false;
        }

        $routeTargetParams = array();

        // Parameter match?
        if ($path != $this->path) {
            // Create a regexp pattern from the path to look for parameters
            $pattern = '#^' . preg_quote($this->path) . '$#im';
            preg_match_all('#(\\\{[^}]+\\\})#', $pattern, $matches);
            $paramsPositions = array();
            foreach ($matches[1] as $i => $toReplace) {
                $paramsPositions[$i] = preg_replace('#[\\\{\}]*#', '', $toReplace);
                $pattern = str_replace($toReplace, '([[:print:]]+)', $pattern);
            }

            // Check for parameters declared in the path
            $nb = preg_match($pattern, $path, $requestMatches);
            if ($nb > 0) {
                unset($requestMatches[0]);
                foreach ($requestMatches as $i => $paramValue) {
                    $paramName = $paramsPositions[$i - 1];
                    $routeTargetParams[$paramName] = $paramValue;
                }
            } else {
                return false;
            }
        }

        if ($params !== null) {
            foreach ($params as $paramName => $paramValue) {
                // Regular parameter
                $routeTargetParams[$paramName] = $paramValue;
            }
        }

        // Try getting a call based on found parameters
        try {
            $routeTargetCall = $this->routeTarget->getCall($this->path, $routeTargetParams);
        } catch (\Exception $e) {
            // Error while trying
            return false;
        }
        // OK, found it
        return $routeTargetCall;
    }

    /**
     * The route's target
     * @return RouteTarget
     */
    public function getTarget()
    {
        return $this->routeTarget;
    }

    /**
     * The route's HTTP method
     * @return HttpMethod
     */
    public function getHttpMethod()
    {
        return $this->httpMethod;
    }
}

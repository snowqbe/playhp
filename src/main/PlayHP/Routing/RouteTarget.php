<?php

namespace PlayHP\Routing;

use PlayHP\Controllers\FormField;
use PlayHP\Controllers\Rendering\Renderable;
use PlayHP\Controllers\Rendering\RenderingEngine;
use PlayHP\Lang\Annotation;
use PlayHP\PlayHP;
use PlayHP\Routing\ControllerCall;
use PlayHP\Routing\Exceptions\InvalidParamException;
use PlayHP\Routing\Exceptions\InvalidTargetException;

/**
 * Represents a route target, invoked when the route is triggered
 */
class RouteTarget
{

    /**
     * Controller call without all method parameters defined.
     * Used to initialize the definitive call object once route parameters will be computed
     * @var ControllerCall
     */
    private $controllerCallTemplate;

    /**
     * The final call to use when invoking the target. Must be prepared using {@link PlayHP\Routing\Route.prepareRoute()}
     * @var ControllerCall
     */
    private $definitiveCall;

    /**
     * Route target as string
     * @var string
     */
    private $target;

    /**
     * List of target parameter names for dynamic invocation
     * @var array
     * @private
     */
    private $targetParams = array();

    function __construct()
    {
    }

    /**
     * Defines the route target
     * @param array $routePathParams
     * @param string $target Target for the route
     * @throws Exceptions\InvalidTargetException
     */
    public function setTargetPath($routePathParams, $target)
    {
        $call = new ControllerCall();
        $this->target = $target;
        $this->targetParams = array();

        $nb = preg_match(Route::ROUTE_TARGET_PATTERN, $target, $controllerMatches);
        if ($nb == 0) {
            throw new InvalidTargetException($target, "Invalid target pattern");
        }

        $matches = array();
        $nb = preg_match_all('#\{([a-zA-Z_0-9]+)\}#', $target, $matches);
        if ($nb > 0) {
            $targetNames = array();
            for ($i = 0; $i < count($matches[1]); $i++) {
                $targetNames[] = $matches[1][$i];
            }
            $call->methodParams = array();
            foreach ($routePathParams as $pathParam) {
                if (in_array($pathParam, $targetNames)) {
                    // Listed as a parameter in the route path: it means that the target's name and/or method will be dynamically invoked
                    $this->targetParams[$pathParam] = null;
                } else {
                    $call->methodParams[$pathParam] = null;
                }
            }
        }

        if (!$this->isDynamicallyInvoked()) {
            // Static route binding
            $controllerClassName = trim($controllerMatches[1]);
            $methodName = trim($controllerMatches[2]);
            $this->initCallControllerAndMethod($controllerClassName, $methodName, $call);
            $this->definitiveCall = $call;
        }
        $this->controllerCallTemplate = $call;
    }

    /**
     * Tell whether the route will be invoked dynamically or not
     * @return bool True if target has pattern parameters, false otherwise
     */
    public function isDynamicallyInvoked()
    {
        return count($this->targetParams) > 0;
    }

    /**
     * @return \PlayHP\Controllers\Controller
     */
    public function getController()
    {
        return $this->definitiveCall->getController();
    }

    /**
     * @param RenderingEngine $engine
     * @return Renderable The final renderable object to use for rendering
     */
    public function invoke(RenderingEngine $engine)
    {
        $router = PlayHP::router();
        $requestParams = $router->getRequestParams();

        // Replace method params with field values
        if ($requestParams !== null && !empty($requestParams)) {
            $this->definitiveCall->updateMethodParams($requestParams);
        }

        /* @var Renderable $renderable */
        $renderable = $this->definitiveCall->invoke($engine);
        return $renderable;
    }

    /**
     * @private
     * Replaces an expression from the target using parsed target parameters
     * @param string $expression Expression to replace
     * @param array $targetParams List of target parameters initialized from the callTemplate
     * @return string Replaced expression to use as controller name or controller method name
     */
    private function replaceParams($expression, array $targetParams)
    {
        $matches = array();
        if (preg_match_all('#\{([^\}]+)\}#', $expression, $matches)) {
            foreach ($matches[1] as $paramName) {
                if (array_key_exists($paramName, $targetParams)) {
                    $expression = str_replace('{' . $paramName . '}', $targetParams[$paramName], $expression);
                }
            }
        }
        return $expression;
    }

    /**
     * @param ControllerCall $call Call to use when route is invoked
     */
    public function setDefinitiveCall(ControllerCall $call)
    {
        $this->definitiveCall = $call;
    }

    /**
     * Computes call informations based on provided parameters
     * @param string $routePath The route's path (with parameters)
     * @param array $params List of parameters to use for initializing the call
     * @param array|null $targetParams Target parameters to use as override
     * @throws Exceptions\InvalidTargetException
     * @return ControllerCall Computed controller call object
     */
    public function getCall($routePath, $params, $targetParams = null)
    {
        $callInfos = new ControllerCall();
        if ($targetParams === null) {
            $targetParams = $this->targetParams;
        }
        $callInfos->methodParams = $this->controllerCallTemplate->methodParams;
        $queryString = '';
        if ($params !== null) {
            foreach ($params as $paramName => $paramValue) {
                $generateQueryString = true;
                if (!is_array($paramValue)) {
                    $generateQueryString = strpos($routePath, '{' . $paramName . '}') == FALSE;
                    $routePath = str_replace('{' . $paramName . '}', urlencode($paramValue), $routePath);
                }
                if (array_key_exists($paramName, $targetParams)) {
                    // This parameter corresponds to a dynamic invocation parameter
                    $targetParams[$paramName] = $paramValue;
                } else {
                    // We'll use this parameter as a method parameter
                    $callInfos->methodParams[$paramName] = $paramValue;
                    if ($generateQueryString) {
                        if (is_array($paramValue) && !empty($paramValue)) {
                            foreach ($paramValue as $key => $val) {
                                $queryString .= ($queryString === '' ? '?' : '&') . urlencode($paramName) . '[' . urlencode($key) . ']=' . urlencode($val);
                            }
                        } else {
                            $queryString .= ($queryString === '' ? '?' : '&') . urlencode($paramName) . '=' . urlencode($paramValue);
                        }
                    }
                }
            }
        }
        $callInfos->setUrl($routePath, $queryString);
        $targetMatches = array();
        if (preg_match('#([^.]+)\.(.+)#', $this->target, $targetMatches) > 0) {
            $controllerClassName = $this->replaceParams($targetMatches[1], $targetParams);
            $methodName = $this->replaceParams($targetMatches[2], $targetParams);
            $this->initCallControllerAndMethod($controllerClassName, $methodName, $callInfos);
        } else {
            throw new InvalidTargetException('Invalid target: ' . $this->target);
        }
        return $callInfos;
    }

    /**
     * @param string $controllerClassName Controller class to use
     * @param string $methodName Controller method to call
     * @param ControllerCall $callInfos Call information to initialize
     * @throws \Exception
     */
    private function initCallControllerAndMethod($controllerClassName, $methodName, &$callInfos)
    {
        $classLoader = PlayHP::classLoder();
        $callInfos->controllerClass = $classLoader->getReflectionClass($controllerClassName);
        if (!$callInfos->controllerClass->hasMethod($methodName)) {
            throw new \Exception("No method $methodName for controller $controllerClassName");
        }
        $callInfos->controllerMethod = $callInfos->controllerClass->getMethod($methodName);
        $paramTypes = Annotation::getParamTypes($callInfos->controllerMethod->getDocComment());
        $reflectionParameters = $callInfos->controllerMethod->getParameters();
        foreach ($reflectionParameters as $reflectionParameter) {
            $propName = $reflectionParameter->getName();
            if (!array_key_exists($propName, $callInfos->methodParams)) {
                if ($reflectionParameter->isOptional()) {
                    $callInfos->methodParams[$propName] = $reflectionParameter->getDefaultValue();
                } else {
                    $callInfos->methodParams[$propName] = null;
                }
            } else if (array_key_exists($propName, $paramTypes)) {
                $paramType = $paramTypes[$propName];
                $filterType = null;
                switch ($paramType) {
                    case 'double':
                    case 'float':
                        $filterType = FILTER_VALIDATE_FLOAT;
                        break;
                    case 'int':
                    case 'integer':
                        $filterType = FILTER_VALIDATE_INT;
                        break;
                    case 'bool':
                    case 'boolean':
                        $filterType = FILTER_VALIDATE_BOOLEAN;
                        break;
                }
                if ($filterType !== null) {
                    $callInfos->methodParams[$propName] = filter_var($callInfos->methodParams[$propName], $filterType);
                }
            }
        }
    }

    /**
     * @return ControllerCall The definitive call to invoke
     */
    public function getDefinitiveCall()
    {
        return $this->definitiveCall;
    }

    /**
     * @return array List of target parameters
     */
    public function getTargetParams()
    {
        return $this->targetParams;
    }

    /**
     * @return string The target controller class name
     */
    public function getControllerClassName()
    {
        if ($this->controllerCallTemplate !== null) {
            return $this->controllerCallTemplate->getControllerClassName();
        }
        return null;
    }

    /**
     * @return string The target controller method name
     */
    public function getControllerMethodName()
    {
        if ($this->controllerCallTemplate !== null) {
            return $this->controllerCallTemplate->getControllerMethodName();
        }
        return null;
    }

    /**
     * @return string The target path
     */
    public function getTargetPath()
    {
        return $this->target;
    }

    /**
     * Updates a route target parameter's value
     * @param string $paramName Name of the parameter
     * @param mixed $paramValue The value of the parameter
     * @throws Exceptions\InvalidParamException When no parameter exists with this name
     */
    public function setParam($paramName, $paramValue)
    {
        if ($this->targetParams === null || !array_key_exists($paramName, $this->targetParams)) {
            // Error
            throw new InvalidParamException('No parameter named ' . $paramName . ' in this route target');
        }
        $this->targetParams[$paramName] = $paramValue;
    }
}

<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Routing;

use Exception;
use PlayHP\Controllers\ControllerMetaData;
use PlayHP\Ioc\ObjectPool;
use PlayHP\Lang\Annotation;
use PlayHP\PlayHP;
use PlayHP\Security\SecurityException;
use PlayHP\Security\SecurityService;
use SimpleXMLElement;

/**
 *
 */
class Router
{
    /**
     * Compiled routes
     * @var Route[]
     */
    private $_routes = array();

    /**
     * @var \SimpleXMLElement
     */
    private $_conf;

    /**
     * All controller definitions
     * @var ControllerMetaData[]
     */
    private $_controllerDefinitions;

    /**
     * @var RequestParser
     */
    private $_requestParser;

    /**
     * @Inject
     * @var SecurityService
     */
    public $securityService;


    /**
     * @param null|SimpleXMLElement $config XML configuration
     */
    public function init($config)
    {
        $this->_requestParser = new RequestParser();
        $this->_conf = $config;
        $this->_controllerDefinitions = array();
        $this->_routes = array('/' => Route::DEFAULT_ROUTE());
    }

    /**
     * @throws SecurityException When current user is not allowed to access target route
     * @return null|Route The found route
     */
    public function guessCurrentRoute()
    {
        PlayHP::profileStep('Router/prepareRoute');
        $this->_requestParser->parseCurrentRequest();
        return $this->prepareRoute(
            $this->_requestParser->getPath(),
            $this->_requestParser->getMethod(),
            $this->_requestParser->getParameters()
        );
    }

    /**
     * @param string $path Path corresponding to the route
     * @param string $method The HTTP method to match, null for any
     * @param array $params The route parameters
     * @return Route The found route
     */
    public function getRoute($path = '/', $method = null, $params = array())
    {
        PlayHP::profileStep('Router/prepareRoute');
        $this->_requestParser->parse($path, $method, $params);
        return $this->prepareRoute(
            $this->_requestParser->getPath(),
            $this->_requestParser->getMethod(),
            $this->_requestParser->getParameters()
        );
    }

    /**
     * Searches for a route which corresponds to provided path
     * @param string $path Path corresponding to the route
     * @param string $method The HTTP method to match, null for any
     * @param array $params The route parameters
     * @throws SecurityException When current user is not allowed to access target route
     * @return null|Route The found route
     */
    protected function prepareRoute($path = '/', $method = null, $params = array())
    {
        /* @var Route $foundRoute */
        $foundRoute = null;
        /* @var Route $unsecuredRoute */
        $unsecuredRoute = null;
        $call = null;
        foreach ($this->_routes as $route) {
            $call = $route->matches($path, $method, $params);
            if ($call !== false) {
                // Check if protocol matches perfectly or not (in case two routes match on all but HTTPS forced protocol)
                $protocol = self::getHttpProtocol();
                if (($route->forceHttps && $protocol == 'https') || (!$route->forceHttps && $protocol == 'http')) {
                    $foundRoute = $route;
                    break;
                } else if ($unsecuredRoute === null) {
                    $unsecuredRoute = $route;
                }
            }
        }
        if ($foundRoute === null && $unsecuredRoute !== null) {
            // We found an route with(out) HTTPS expectation which matches the path but we're not using the HTTP(S) protocol: redirect
            redirect($this->getHttpUrl($path, $unsecuredRoute->forceHttps));
        }
        PlayHP::profileStep('Router/routeMatched');
        if ($foundRoute !== null) {

            // Check security if a security service has been defined
            if ($this->securityService !== null) {
                $this->securityService->checkRouteSecurity($foundRoute);
            }

            // Initialize route call
            if ($call === null) {
                try {
                    $call = $foundRoute->getTarget()->getCall($path, $params);
                } catch (Exception $e) {
                    return null;
                }
            }
            if ($call !== false) {
                $foundRoute->getTarget()->setDefinitiveCall($call);
                return $foundRoute;
            }
        }
        return null;
    }

    /**
     * @return ControllerMetaData[] The list of all controller meta data
     */
    public function getControllerDefinitions()
    {
        return $this->_controllerDefinitions;
    }

    /**
     * @return \PlayHP\Routing\Route[] The list of configured routes
     */
    public function getRoutes()
    {
        return $this->_routes;
    }

    /**
     * Parses application controllers
     */
    public function parseRoutes($rootNode = null, $defaultSecurity = null)
    {
        // Parse configuration
        if ($rootNode === null) {
            $rootNode = $this->_conf;
        }
        foreach ($rootNode->children() as $node) {
            switch ($node->getName()) {
                case 'route':
                    $route = $this->parseRoute($node, $defaultSecurity);
                    if ($route !== null) {
                        $this->_routes[$route->path] = $route;
                    }
                    break;
                case 'secured':
                    $allow = (string)$node['allow'];
                    $deny = (string)$node['deny'];
                    $forceHttps = (string)$node['forceHttps'] == 'true';
                    $this->parseRoutes($node, array('allow' => $allow, 'deny' => $deny, 'forceHttps' => $forceHttps));
                    break;
            }
        }
    }


    /**
     * Gets a computed URL of a given page
     * @param string $targetClassName Controller class name
     * @param string $targetClassMethod Controller class method name
     * @param array $methodParams List of method parameters
     * @internal param string $target The controller
     * @return string URL
     */
    public function reverse($targetClassName, $targetClassMethod, $methodParams = null)
    {
        $template = $this->getURLTemplate($targetClassName, $targetClassMethod);
        if ($template === null) {
            return null;
        }
        try {
            $call = $template->routeTarget->getCall($template->routePath, $methodParams);
            return $this->getHttpUrl($call->getUrl(), $template->forceHttps);
        } catch (Exception $e) {
            // TODO log
            return null;
        }
    }

    /**
     * @param string $value URL to format
     * @return string Formatted URL
     */
    public function url($value)
    {
        $httpStart = substr($value, 0, 6);
        if ($httpStart === 'http:/' || $httpStart == 'https:') {
            return $value;
        }
        if (substr($value, 0, 1) != '/') {
            $value = '/' . $value;
        }
        if (strpos($value, BASE) !== 0) {
            return HTTP_BASE . $value;
        }
        return $value;
    }

    /**
     * Adds a new route to the router
     * @param string|Route $routeOrPath Route to add to router (Route instance), or path (string). In case of a path,
     * you must provide the HTTP method (*, get, post, put, delete) and the target
     * @param null|string $method The HTTP method
     * @param null|string $target The route's target
     * @throws Exceptions\InvalidPathException If provided path was invalid
     * @throws Exceptions\InvalidTargetException If provided target was invalid
     */
    public function addRoute($routeOrPath, $method = null, $target = null)
    {
        if (is_a($routeOrPath, 'PlayHP\Routing\Route')) {
            $this->_routes[$routeOrPath->path] = $routeOrPath;
        } else {
            $route = new Route();
            /* @var HttpMethod $httpMethod */
            if (is_object($method) && is_a($method, 'PlayHP\Routing\HttpMethod')) {
                $httpMethod = $method;
            } else {
                $httpMethod = HttpMethod::parse($method);
            }
            if ($httpMethod !== null) {
                $route->setHttpMethod($httpMethod);
            }
            $route->setPathAndTarget($routeOrPath, $target);
            $this->_routes[$route->path] = $route;
        }
    }

    /**
     * @param Route[] $routes List of routes to add to router
     */
    public function addRoutes($routes)
    {
        foreach ($routes as $route) {
            $this->addRoute($route);
        }
    }

    /**
     * Returns the list of parsed request parameters
     * @return array|null List of parsed request parameters or null if nothing has been parsed yet
     */
    public function getRequestParams()
    {
        if ($this->_requestParser) {
            return $this->_requestParser->getParameters();
        }
        return null;
    }

    /**
     * @return string The current path user is trying to access
     */
    public function getCurrentPath()
    {
        return $this->_requestParser->getPath();
    }

    /**
     * Parses a route to add to router
     * @param SimpleXMLElement $routeConfig
     * @param array|null $defaultSecurity
     * @return null|Route The parsed route
     */
    private function parseRoute(SimpleXMLElement $routeConfig, $defaultSecurity = null)
    {
        $method = trim(strtoupper(strval($routeConfig['method'])));
        if ($method == '') {
            $method = 'GET';
        }
        $path = trim(strval($routeConfig['path']));
        $target = trim(strval($routeConfig['target']));
        $forceHttps = trim((string)$routeConfig['forceHttps']) == 'true';
        if (!in_array($method, array('', '*', 'GET', 'POST', 'PUT', 'DELETE'))) {
            // Invalid route configuration
            return null;
        }
        if (substr($path, 0, 1) != '/') {
            // Invalid route configuration
            return null;
        }
        if (empty($target)) {
            // Invalid route configuration
            return null;
        }
        try {
            $route = new Route();
            /* @var HttpMethod $httpMethod */
            $httpMethod = HttpMethod::parse($method);
            if ($httpMethod !== null) {
                $route->setHttpMethod($httpMethod);
            }
            $route->setPathAndTarget($path, $target);

            // Security parsing
            $allow = null;
            $deny = null;
            if ($defaultSecurity != null) {
                $allow = $defaultSecurity['allow'];
                $deny = $defaultSecurity['deny'];
                $forceHttps = $defaultSecurity['forceHttps'];
            }
            $route->forceHttps = $forceHttps;

            // Override with route config if defined
            if (isset($routeConfig['allow'])) {
                $allow = (string)$routeConfig['allow'];
            }
            if (isset($routeConfig['deny'])) {
                $deny = (string)$routeConfig['deny'];
            }

            // Finally define the route security
            if ($allow != null) {
                $route->allowed = explode(',', $allow);
            }
            if ($deny != null) {
                $route->denied = explode(',', $deny);
            }
            return $route;
        } catch (Exception $exc) {
            // Invalid route
            return null;
        }
    }

    /**
     * @param string $targetClassName Controller class name to invoke
     * @param string $targetClassMethodName Controller method name to invoke
     * @return URLTemplate The URL template matched or null if none found
     */
    public function getURLTemplate($targetClassName, $targetClassMethodName)
    {
        // Check availability of requested class
        $classLoader = PlayHP::classLoder();
        if (substr($targetClassName, 0, 1) == '\\') {
            $targetClassName = substr($targetClassName, 1);
        }
        $preferQualified = (strpos($targetClassName, '\\') > 0);
        try {
            $targetClass = $classLoader->getReflectionClass($targetClassName);
            if (!$targetClass->hasMethod($targetClassMethodName)) {
                return null;
            }
        } catch (Exception $e) {
            // No class found
            return null;
        }

        // RegExp to use to match the name in parametrized targets
        $requestedUnqualifiedTarget = $targetClass->getShortName() . '.' . $targetClassMethodName;
        $requestedQualifiedTarget = $targetClass->getNamespaceName() . '\\' . $requestedUnqualifiedTarget;
        foreach ($this->_routes as $route) {
            try {
                $routePath = $route->path;
                $routeTarget = $route->getTarget();
                $targetParams = array_keys($routeTarget->getTargetParams());
                $targetMatches = array();

                // Create a regexp to capture the parts in the route target expression
                $testExpression = '/' . preg_replace('/\\\{[^}]+\\\}/', '([a-zA-Z0-9_\\\\\.]+)', preg_quote($routeTarget->getTargetPath())) . '/';

                // Match qualified or unqualified expression
                if ($preferQualified) {
                    $match = preg_match($testExpression, $requestedQualifiedTarget, $targetMatches) ||
                        preg_match($testExpression, $requestedUnqualifiedTarget, $targetMatches);
                } else {
                    $match = preg_match($testExpression, $requestedUnqualifiedTarget, $targetMatches) ||
                        preg_match($testExpression, $requestedQualifiedTarget, $targetMatches);
                }

                if (!$match) {
                    continue;
                }

                // Get target parameter values if any
                array_shift($targetMatches); // Remove first value of preg_match result
                if (count($targetParams) != count($targetMatches)) {
                    // Problem here, we should have the same number of target parameters as matched values
                    throw new Exception('Matched target ' . $routeTarget->getTargetPath() . ' with ' . $requestedQualifiedTarget . ' but number of parameters mismatch');
                }

                // Replace matched target parameters with their values in the route path and update the route target
                $definedRouteParams = array();
                while (count($targetParams) > 0) {
                    $targetParamName = array_shift($targetParams);
                    $expressionMatch = array_shift($targetMatches);
                    $routeTarget->setParam($targetParamName, $expressionMatch);
                    $definedRouteParams[] = $targetParamName;
                    $routePath = str_replace('{' . $targetParamName . '}', $expressionMatch, $routePath);
                }

                // List route parameters
                $routeParams = array_diff($route->getPathParams(), $definedRouteParams);
                $targetClassMethod = $targetClass->getMethod($targetClassMethodName);
                $methodParamsReflect = $targetClassMethod->getParameters();
                $methodParams = array();
                foreach ($methodParamsReflect as $methodParam) {
                    $methodParams[] = $methodParam->name;
                }
                $urlTemplate = new URLTemplate($routePath, $routeParams, $routeTarget, $methodParams, $route->forceHttps);
                return $urlTemplate;
            } catch (Exception $e) {
                // TODO: Log
                // Problem while matching, try the next one
            }
        }
        return null;
    }

    public static function getHttpProtocol()
    {
//        if ($this->trustForwarded && isset($_SERVER['HTTP_X_FORWARDED_PROTO'])) {
//            if ($_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
//                return 'https';
//            }
//            return 'http';
//        }
        if (isset($_SERVER['HTTPS']) &&
            ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] == 1)
        ) {
            return 'https';
        }
        /*nginx way of checking for https*/
        if (isset($_SERVER['SERVER_PORT']) &&
            ($_SERVER['SERVER_PORT'] === '443')
        ) {
            return 'https';
        }
        return 'http';
    }

    /**
     * @param string $relativeUrl The relative URL to convert to absolute HTTPS url
     * @param bool $https true to ask for HTTPS url, false otherwise
     * @return string The absolute, https URL to use
     */
    private function getHttpUrl($relativeUrl, $https = false)
    {
        $settings = PlayHP::inst()->settings();
        $serverPath = $_SERVER['SERVER_NAME'];
        if ($https) {
            if ($settings->httpsPort != 443) {
                $serverPath .= ':' . $settings->httpsPort;
            }
            $result = 'https://';
        } else {
            if ($settings->httpPort != 80) {
                $serverPath .= ':' . $settings->httpPort;
            }
            $result = 'http://';
        }
        return $result . $serverPath . BASE . $relativeUrl;
    }

}
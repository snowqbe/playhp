<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Routing;


use PlayHP\Lang\Enum;

class RoutingExceptionType extends Enum
{

    static $cancel;

    static $continue;
}

RoutingExceptionType::init();
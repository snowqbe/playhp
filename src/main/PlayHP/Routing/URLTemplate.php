<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Routing;

/**
 * Template of an URL
 *
 * @package PlayHP\Routing
 */
class URLTemplate
{

    /**
     * @var string The route path
     */
    public $routePath;

    /**
     * @var array Parameters to yet defined in route path
     */
    public $routeParameters;

    /**
     * @var RouteTarget The route's target
     */
    public $routeTarget;

    /**
     * @var array List of method parameters (can overlap with routeParameters)
     */
    public $methodParams;

    /**
     * @var bool Forces HTTPS
     */
    public $forceHttps;

    function __construct($routePath, $routeParameters, $routeTarget = null, $methodParams = null, $forceHttps = false)
    {
        $this->routePath = $routePath;
        $this->routeParameters = $routeParameters;
        $this->routeTarget = $routeTarget;
        $this->methodParams = $methodParams;
        $this->forceHttps = $forceHttps;
    }


}
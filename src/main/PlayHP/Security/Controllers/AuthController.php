<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Security\Controllers;


use PlayHP\Controllers\Controller;
use PlayHP\Mail\MailService;
use PlayHP\PlayHP;
use PlayHP\Security\Model\User;
use PlayHP\Security\SecurityService;

class AuthController extends Controller
{
    /**
     * @Inject
     * @var SecurityService Service handling security matters
     */
    public $securityService;

    /**
     * @Inject
     * @var MailService Service for sending emails
     */
    public $mailService;

    /**
     * Login entry point
     */
    public function login($returnUrl)
    {
        $this->render('login', array('returnUrl' => $returnUrl));
    }

    /**
     * Checks for authentication or resets a user's forgotten password
     * @param string $login User login @Required @Validator(UserLoginExistsValidator)
     * @param string $password User password @Required
     * @param null $returnUrl URL to go back
     */
    public function verifyCredentials($login, $password, $returnUrl = null)
    {
        $errors = $this->validate();
        if ($errors) {
            $this->render('login', array(
                'returnUrl' => $returnUrl,
                'errors' => $errors
            ));
            return;
        }
        $user = $this->securityService->authenticate($login, $password);
        if ($user !== null) {

            // Is account verified?
            if (!$user->verified) {
                $this->render('login', array(
                    'returnUrl' => $returnUrl,
                    'error' => _p('Your account is locked: you need to activate your email first. Please check your inbox and click on the validation link, thank you.'),
                ));
                return;
            }
            if ($returnUrl === null) {
                $returnUrl = '';
            }
            if (substr($returnUrl, 0, 1) != '/') {
                $returnUrl = '/' . $returnUrl;
            }
            redirect(HTTP_BASE . $returnUrl);
        } else {
            $this->render('login', array(
                'returnUrl' => $returnUrl,
                'error' => _p('Your login or password is invalid'),
            ));
        }
    }

    /**
     * Logout default handler
     */
    public function logout()
    {
        $this->securityService->logout();

        // Defaults to login
        redirect(BASE . '/');
    }

    /**
     * Displays the forgot password form view
     */
    public function forgot($login = null)
    {
        $this->render('forgot', array('login' => $login));
    }

    /**
     * Validates a given user's email
     * @param string $token Token used for validation
     * @param string $login User login
     */
    public function verifyAccount($token, $login)
    {
        $errorMessage = _p('There\'s been an issue validating your email. Please check your inbox our contact the website\'s administrator.');
        $successMessage = sprintf(_p('Thank you for validating your email. You will be redirected to <a href="%s">home page</a> in 5 seconds...'), BASE);
        $verified = false;
        $user = $this->securityService->getUserByLogin($login);
        if ($user !== null) {
            $verified = $this->securityService->verifyAccount($user, $token);
        }
        $this->render('verifyEmail', $verified ? array('success' => $successMessage) : array('error' => $errorMessage));
    }

    /**
     * Proposes to change the user's password
     * @param string $password
     * @param string $newPassword
     * @param string $passwordConfirm
     */
    public function changePassword($password, $newPassword, $passwordConfirm)
    {
        if ($password === null) {
            $this->render('changePassword', get_defined_vars());
        }
        $user = $this->securityService->getAuthenticatedUser();
        if ($user === null) {
            $this->render('changePassword', array('error' => 'You must be connected to change your password'));
        }
        $errors = array();
        // Control
        if (!$this->securityService->comparePasswords($user->password, $password)) {
            $errors['password'] = _p('Wrong actual password');
        }
        if (trim($newPassword) === '') {
            $errors['newPassword'] = _p('Please enter a new password');
        }
        if (trim($passwordConfirm) === '') {
            $errors['passwordConfirm'] = _p('Please enter your new password\'s confirmation');
        }
        if ($newPassword !== $passwordConfirm) {
            $errors['newPassword'] = _p('New password confirmation does not match');
        }
        if (empty($errors)) {
            $this->securityService->updateUserPassword($user, $passwordConfirm);
            $success = _p('Password changed successfully!');
        }
        $this->render('changePassword', get_defined_vars());
    }

    /**
     * Resets the password for given user
     * @param string $login
     */
    public function resetPassword($login)
    {
        $user = $this->securityService->getUserByLogin($login);
        if ($user === null) {
            $this->render('forgot', array(
                'login' => $login,
                'error' => _p('Sorry, no user found with this login...')
            ));
        } else {
            // Regenerate a password
            $newPassword = $this->securityService->resetPassword($user);
            $viewVariables = array(
                'newPassword' => $newPassword,
                'website' => HTTP_BASE,
                'changeLink' => HTTP_BASE . $this->securityService->changePasswordRoute,
                'displayName' => $user->getDisplayName()
            );
            $mailContents = $this->getContents('mail/resetPassword', $viewVariables);
            $mailContentsText = $this->getContents('mail/resetPasswordText', $viewVariables);
            $subject = utf8_decode($user->firstName . _p(', your password was reset'));
            $this->mailService->send($user->email, $subject, $mailContents, $mailContentsText, P_ROOT);
            $this->render('forgot', array(
                'login' => $login,
                'success' => _p('OK, password was reset. You will receive an email shortly at: ') . $user->email
            ));
        }
    }

    /**
     * Delete account of currently authenticated user
     */
    public function deleteAccount($confirm, $cancel)
    {
        if ($cancel) {
            redirect(HTTP_BASE);
        }

        $user = $this->securityService->getAuthenticatedUser();

        // Check for authenticated user, even if route should be protected, user account removal is serious...
        if ($user === null) {
            $this->error(_p('User should be authenticated to remove his account'));
        }
        if ($confirm) {
            // Remove user account
            $this->securityService->markAccountForRemoval($user);
            $this->securityService->logout();

            // Send confirmation email
            $viewVariables = array(
                'website' => HTTP_BASE,
                'displayName' => $user->getDisplayName()
            );
            $mailContents = $this->getContents('mail/accountRemoved', $viewVariables);
            $mailContentsText = $this->getContents('mail/accountRemovedText', $viewVariables);
            $subject = utf8_decode($user->firstName . _p(', sorry to see you go'));
            $this->mailService->send($user->email, $subject, $mailContents, $mailContentsText, P_ROOT);
            $this->render('deleteAccount', array(
                'success' => sprintf(_p('Your account has been marked for removal. You will shortly receive a confirmation email at: %s.'), $user->email)
            ));
        }
        $this->render('deleteAccount');
    }

    /**
     * @param User $validatedUser User to register with an API level validation
     * @return array The list of variables to pass to the view
     */
    protected function registerFromApi($validatedUser)
    {
        return $this->registerWithValidation($validatedUser, 'api');
    }

    /**
     * @param User $user User filled with submitted form values
     */
    public function register($user)
    {
        $viewVariables = $this->registerWithValidation($user, 'register');
        $this->render('register', $viewVariables);
    }

    /**
     * Registers the user and validates him using provided validation profile
     * @param User $user
     * @param string $validationProfile The validation profile to use
     * @return array The list of variables to pass to the view
     */
    private function registerWithValidation($user, $validationProfile = 'register')
    {
        if ($user !== null) {
            // Check form correctly filled (checks the login availability with the custom validator)
            $errors = $this->validateObject($user, array($validationProfile));
            if ($errors !== false) {
                $this->render('register', get_defined_vars());
                return get_defined_vars();
            }
            // Save user with email validation token
            if (!$this->securityService->register($user)) {
                $this->render('register', array('error' => _p('Unable to register user')));
                return get_defined_vars();
            }

            if (!$user->verified) {
                // User account verification
                $router = PlayHP::router();
                $verifyEmailLink = $router->reverse('PlayHP\Security\Controllers\AuthController', 'verifyAccount', array(
                    'token' => $user->token,
                    'login' => $user->login,
                ));

                // 4. Send user's email verification mail
                $viewVariables = array(
                    'website' => HTTP_BASE,
                    'verifyLink' => HTTP_BASE . $verifyEmailLink,
                    'displayName' => $user->getDisplayName()
                );
                $mailContents = $this->getContents('mail/registration', $viewVariables);
                $mailContentsText = $this->getContents('mail/registrationText', $viewVariables);
                $subject = utf8_decode($user->firstName . _p(', please verify your email'));
                $this->mailService->send($user->email, $subject, $mailContents, $mailContentsText, P_ROOT);
                $success = _p('Thanks for registering. You will shortly receive an email to activate your account.');
            } else {
                $success = sprintf(_p('Thanks for registering! <a href="%s">Back home</a>'), HTTP_BASE);
            }
        }
        return get_defined_vars();
    }

}
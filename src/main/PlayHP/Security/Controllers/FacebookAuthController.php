<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Security\Controllers;


use PlayHP\PlayHP;

abstract class FacebookAuthController extends AuthController implements ThirdPartyAuthenticator
{

    /**
     * @Config
     * @var string Facebook App ID
     */
    public $facebookAppId;

    /**
     * @Config
     * @var string Facebook App Secret
     */
    public $facebookAppSecret;

    /**
     * Facebook API
     * @var \Facebook
     */
    protected $f8;

    /**
     * @Init
     */
    public function initClient()
    {
        $this->f8 = new \Facebook(array(
            'appId' => $this->facebookAppId,
            'secret' => $this->facebookAppSecret
        ));
    }

    /**
     * Sign in with facebook
     */
    public function facebookSignIn()
    {
        $userId = $this->f8->getUser();
        if ($userId) {
            // Validate existing
            $this->getUserInformation(true);
        } else {
            // Login
            $router = PlayHP::router();
            $callbackUrl = HTTP_BASE . $router->reverse(get_class($this), 'facebookCallback', array());
            $params = array(
                'scope' => 'email',
                'redirect_uri' => $callbackUrl
            );
            $loginUrl = $this->f8->getLoginUrl($params);
            redirect($loginUrl);
        }
    }

    /**
     * Callback after user sign in
     */
    public function facebookCallback()
    {
        $this->getUserInformation(false);
    }

    /**
     * @inheritdoc
     */
    public function logout()
    {
        parent::logout();
    }

    /**
     * @inheritdoc
     */
    public function getUserInformation($trySignIn)
    {
        $userId = $this->f8->getUser();
        if ($userId) {
            try {
                $userInfos = $this->f8->api('/me', 'GET');
                $this->handleAuthenticatedUser($userInfos);
            } catch (\FacebookApiException $e) {
                if ($trySignIn) {
                    $this->f8->destroySession();
                    $this->facebookSignIn();
                } else {
                    $this->handleError($e->getCode(), $e->getMessage());
                }
            }
        } else if ($trySignIn) {
            $this->facebookSignIn();
        }
    }

}
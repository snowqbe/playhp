<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Security\Controllers;

use Google_Client;
use Google_Oauth2Service;
use PlayHP\PlayHP;

abstract class GoogleAuthController extends AuthController implements ThirdPartyAuthenticator
{
    /**
     * Storage key for google oauth2 token
     */
    const KEY_TOKEN = 'gtoken';

    /**
     * @Config
     * @var string Google client ID
     */
    public $googleClienId;

    /**
     * @Config
     * @var string Google client secret
     */
    public $googleSecretId;

    /**
     * @Config
     * @var string Google developer key
     */
    public $googleDeveloperKey;

    /**
     * @Config
     * @var string Google developer key
     */
    public $appName;

    /**
     * Google client for authentication
     * @var Google_Client
     */
    private $googleClient;

    /**
     * OAuth2 service
     * @var Google_Oauth2Service
     */
    private $oauth2;


    /**
     * @Init
     * Initializes the google client for authentication
     */
    public function initClient()
    {
        $router = PlayHP::router();
        $callbackUrl = HTTP_BASE . $router->reverse(get_class($this), 'googleCallback', array());
        $this->googleClient = new Google_Client();
        $this->googleClient->setApplicationName($this->appName);
        $this->googleClient->setClientId($this->googleClienId);
        $this->googleClient->setClientSecret($this->googleSecretId);
        $this->googleClient->setRedirectUri($callbackUrl);
        $this->googleClient->setDeveloperKey($this->googleDeveloperKey);
        $this->oauth2 = new Google_Oauth2Service($this->googleClient);

        if (isset($_SESSION[self::KEY_TOKEN])) {
            $this->googleClient->setAccessToken($_SESSION[self::KEY_TOKEN]);
        }
    }

    /**
     * Google sign in feature
     */
    public function googleSignIn()
    {
        if ($this->googleClient->getAccessToken()) {
            $this->getUserInformation(false);
        } else {
            $authUrl = $this->googleClient->createAuthUrl();
            redirect($authUrl);
        }
    }

    /**
     * OAuth2 callback
     */
    public function googleCallback()
    {
        try {
            // Try to authenticate
            $this->googleClient->authenticate();

            // Authenticated successfuly
            $_SESSION[self::KEY_TOKEN] = $this->googleClient->getAccessToken();

            // Reinitialize the client to avoid redirecting again
            $this->initClient();

            $this->getUserInformation(false);
        } catch (\Google_AuthException $e) {
            $this->handleError($e->getCode());
        }
    }

    /**
     * Get user information from thr third party provider
     * @param bool $trySignIn Flag telling to trigger sign in process if an error occurs
     */
    function getUserInformation($trySignIn)
    {
        try {
            // Pass user info to implementation
            $googleUser = $this->oauth2->userinfo->get();
            $this->handleAuthenticatedUser($googleUser);
        } catch (\Google_AuthException $e) {
            if ($trySignIn) {
                $this->googleSignIn();
            } else {
                $this->handleError($e->getCode());
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function logout()
    {
        // Revoke token when logging out
        if (isset($_SESSION[self::KEY_TOKEN])) {
            unset($_SESSION[self::KEY_TOKEN]);
            $this->googleClient->revokeToken();
        }
        parent::logout();
    }
}
<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Security\Controllers;


/**
 * Class ThirdPartyAuthenticator.
 * Enables third party authentication (Facebook, Twitter, Google)
 * @package PlayHP\Security\Controllers
 */
interface ThirdPartyAuthenticator
{

    /**
     * @Init
     * Initializes the authenticator
     */
    function initClient();

    /**
     * @param mixed $user Authenticated user information
     */
    function handleAuthenticatedUser($user);

    /**
     * @param int $httpCode HTTP Code returned on error
     * @param string $message Error message
     */
    function handleError($httpCode, $message = '');

    /**
     * Handles user logout action
     */
    function logout();

    /**
     * Get user information from thr third party provider
     * @param bool $trySignIn Flag telling to trigger sign in process if an error occurs
     */
    function getUserInformation($trySignIn);

}
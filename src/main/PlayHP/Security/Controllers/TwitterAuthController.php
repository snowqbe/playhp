<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Security\Controllers;

use Google_Client;
use PlayHP\PlayHP;

abstract class TwitterAuthController extends AuthController implements ThirdPartyAuthenticator
{
    /**
     * Storage key for twitter token
     */
    const KEY_ACCESS_TOKEN = 'ttoken';

    /**
     * Storage key for twitter token secret
     */
    const KEY_REQ_TOKEN = 't_req_token';

    /**
     * Storage key for twitter token secret
     */
    const KEY_REQ_TOKEN_SECRET = 't_req_token_secret';

    /**
     * @Config
     * @var string Twitter consumer key
     */
    public $twitterConsumerKey;

    /**
     * @Config
     * @var string Google client secret
     */
    public $twitterConsumerSecret;

    /**
     * @Config
     * @var string Google developer key
     */
    public $appName;

    /**
     * @var \TwitterOAuth
     */
    protected $twitterOAuth;


    /**
     * @Init
     * Initializes the google client for authentication
     */
    public function initClient()
    {
        $token = null;
        $tokenSecret = null;
        if (isset($_SESSION[self::KEY_REQ_TOKEN])) {
            $token = $_SESSION[self::KEY_REQ_TOKEN];
            $tokenSecret = $_SESSION[self::KEY_REQ_TOKEN_SECRET];
        } else if (isset($_SESSION[self::KEY_ACCESS_TOKEN])) {
            $accessToken = $_SESSION[self::KEY_ACCESS_TOKEN];
            $token = $accessToken['oauth_token'];
            $tokenSecret = $accessToken['oauth_token_secret'];
        }
        $this->twitterOAuth = new \TwitterOAuth($this->twitterConsumerKey, $this->twitterConsumerSecret, $token, $tokenSecret);
    }

    /**
     * Google sign in feature
     */
    public function twitterSignIn()
    {
        if (isset($_SESSION[self::KEY_REQ_TOKEN]) || isset($_SESSION[self::KEY_ACCESS_TOKEN])) {
            $this->getUserInformation(true);
            return;
        }
        $router = PlayHP::router();
        $callbackUrl = HTTP_BASE . $router->reverse(get_class($this), 'twitterCallback', array());
        $requestToken = $this->twitterOAuth->getRequestToken($callbackUrl);
        $_SESSION[self::KEY_REQ_TOKEN] = $token = $requestToken['oauth_token'];
        $_SESSION[self::KEY_REQ_TOKEN_SECRET] = $requestToken['oauth_token_secret'];

        if ($this->twitterOAuth->http_code === 200) {
            $authUrl = $this->twitterOAuth->getAuthorizeURL($token);
            redirect($authUrl);
        } else {
            trace($this->twitterOAuth->http_info);
            $this->handleError($this->twitterOAuth->http_code);
        }
    }

    /**
     * OAuth2 callback
     */
    public function twitterCallback()
    {
        /* Request access tokens from twitter */
        $accessToken = $this->twitterOAuth->getAccessToken($_REQUEST['oauth_verifier']);

        /* Save the access tokens. Normally these would be saved in a database for future use. */
        $_SESSION[self::KEY_ACCESS_TOKEN] = $accessToken;

        /* If HTTP response is 200 continue otherwise send to connect page to retry */
        if (200 == $this->twitterOAuth->http_code) {
            $this->getUserInformation(false);
        } else {
            $this->handleError($this->twitterOAuth->http_code, 'Error while getting access token from Twitter.' . PlayHP::isDevMode() ? '<br><pre>' . print_r($this->twitterOAuth->http_info, true) : '');
        }
    }

    /**
     * @inheritdoc
     */
    public function logout()
    {
        // Revoke token when logging out
        if (isset($_SESSION[self::KEY_ACCESS_TOKEN])) {
            unset($_SESSION[self::KEY_ACCESS_TOKEN]);
        }
        parent::logout();
    }


    /**
     * Get user information from Twitter
     * @param bool $trySignIn Flag telling to trigger sign in process if an error occurs
     */
    public function getUserInformation($trySignIn)
    {
        if (isset($_SESSION[self::KEY_REQ_TOKEN])) {
            unset($_SESSION[self::KEY_REQ_TOKEN]);
            unset($_SESSION[self::KEY_REQ_TOKEN_SECRET]);
        }

        $twitterUser = $this->twitterOAuth->get('account/verify_credentials');
        if (200 == $this->twitterOAuth->http_code) {
            $this->handleAuthenticatedUser($twitterUser);
            return;
        }
        if ($trySignIn) {
            if (isset($_SESSION[self::KEY_ACCESS_TOKEN])) {
                unset($_SESSION[self::KEY_ACCESS_TOKEN]);
            }
            $this->twitterSignIn();
        } else {
            $this->handleError($this->twitterOAuth->http_code, 'Unable to verify token');
        }
    }
}
<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Security\Crypto;


/**
 * Generalizes cryptographic functions using a default algorithm and a salt
 *
 * @package PlayHP\Security\Crypto
 */
class Crypto
{
    /**
     * @var CryptoAlgo
     */
    private $_algo;

    /**
     * @var string The salt
     */
    private $_salt;

    /**
     * @param string $salt Salt to use for encryption
     * @param string $algo Encryption algorithm (md5 or sha1)
     * @throws CryptoException When no salt is given
     */
    function __construct($salt, $algo = 'sha1')
    {
        if ($salt === null) {
            throw new CryptoException('You must provide a salt for using the crypto service');
        }
        $this->_salt = (string)$salt;
        $algo = CryptoAlgo::parse($algo);
        if ($algo === null) {
            $algo = CryptoAlgo::$sha1;
        }
        $this->_algo = $algo;
    }


    /**
     * Encrypts a value using the defined algorithm and salt
     * @param mixed $value Value to encrypt
     * @return string Encrypted value
     */
    public function encrypt($value)
    {
        $toEncrypt = $this->_salt . (string)$value;
        switch ($this->_algo) {
            case CryptoAlgo::$md5:
                return md5($toEncrypt);
                break;
            default:
            case CryptoAlgo::$sha1:
                return sha1($toEncrypt);
                break;
        }
    }

    /**
     * @return string The salt used for encryption
     */
    public function getSalt()
    {
        return $this->_salt;
    }
}
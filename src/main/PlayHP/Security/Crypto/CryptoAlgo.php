<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Security\Crypto;


use PlayHP\Lang\Enum;

class CryptoAlgo extends Enum
{

    public static $sha1;
    public static $md5;
}

CryptoAlgo::init();
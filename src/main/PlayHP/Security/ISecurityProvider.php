<?php

namespace PlayHP\Security;

use PlayHP\Security\Model\SecurityContext;
use PlayHP\Security\Model\User;

/**
 * Describes a security data provider
 */
interface ISecurityProvider
{
    /**
     * Fetches a user based on login and password
     * @param string $login Input login
     * @param string $password Input password
     * @return User
     */
    function getUserByLoginPassword($login, $password);

    /**
     * Fetching a user's credentials
     * @param User $user User whom we want to check the rights
     * @param SecurityContext $context Context in which we want to know user's rights. Use null for global user rights.
     * @return string[] The list of user rights
     */
    function getUserCredentials(User $user, SecurityContext $context = null);

    /**
     * Checks for a user's credentials
     * @param string $right Right we want to check
     * @param User $user User whom we want to check the rights
     * @param SecurityContext $context Context in which we want to know user's rights. Use null for global user rights.
     * @return bool true if user has provided right, false otherwise
     */
    function hasCredential($right, User $user, SecurityContext $context = null);

    /**
     * @return User[]
     */
    function getAllUsers();

    /**
     * Fetches a user based on login
     * @param string $login Input login
     * @return User
     */
    public function getUserByLogin($login);

    /**
     * Saves provided user
     * @param User $user
     * @return bool Save status (true = OK, false = KO)
     */
    public function save($user);

    /**
     * Remove accounts marked for removal depending on provided time
     * @param int $time Time limit to use for removing accounts
     * @return int Number of removed accounts
     */
    public function removeAccounts($time);

}

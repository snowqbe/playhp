<?php

namespace PlayHP\Security\Model;

/**
 * User of an application
 * @Table(Users)
 */
class User
{
    /**
     * User's login
     *
     * @Required
     * @Validator(class="ExistingUserValidator", profiles="register")
     *
     * @Id(name="user_login", nullable=false)
     * @var string
     */
    public $login;

    /**
     * User's email
     *
     * @Required(profiles="register")
     * @Email
     *
     * @Column(name="user_email", nullable=false)
     * @var string
     */
    public $email;

    /**
     * @Column(name="user_verified", nullable=false)
     * @var bool Flag telling the email has been verified
     */
    public $verified = false;

    /**
     * User's password
     * @Required(profiles="register")
     * @Equals(field="passwordConfirm", profiles="register")
     *
     * @Column(name="user_password", nullable=false)
     * @var string
     */
    public $password;

    /**
     * Password confirmation used for registration form.
     *
     * @Required(profiles="register")
     *
     * @var string
     */
    public $passwordConfirm;

    /**
     * List of user's profiles
     * @Fetch(select profile_code from UserProfiles WHERE user_login = :login)
     * @var string[]
     */
    public $profiles;

    /**
     * @Required(profiles="register")
     *
     * @Column(name="user_firstname", nullable=false)
     * @var string User first name
     */
    public $firstName;

    /**
     * @Required(profiles="register")
     *
     * @Column(name="user_lastname", nullable=false)
     * @var string User last name
     */
    public $lastName;

    /**
     * @Column(name="user_token", nullable=true)
     * @var string The password reset token
     */
    public $token;

    /**
     * @Column(name="user_tokenExp", nullable=true)
     * @var int Time of the token expiration
     */
    public $tokenExpiration;

    /**
     * @Checked(profiles="register")
     * @var bool true if user accepts the terms of use
     */
    public $acceptTerms;

    /**
     * @Column(name="user_removal", nullable=true)
     * @var int Time after which user account will be removed
     */
    public $removalDate;

    /**
     * @return string Name displayed on screen when logged in
     */
    public function getDisplayName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    /**
     * @param User $authUser User to copy info from
     */
    public function copyFrom(User $authUser)
    {
        $this->profiles = $authUser->profiles;
        $this->login = $authUser->login;
        $this->email = $authUser->email;
        $this->firstName = $authUser->firstName;
        $this->lastName = $authUser->lastName;
        $this->password = $authUser->password;
        $this->token = $authUser->token;
        $this->tokenExpiration = $authUser->tokenExpiration;
        $this->removalDate = $authUser->removalDate;
        $this->verified = $authUser->verified;
    }
}

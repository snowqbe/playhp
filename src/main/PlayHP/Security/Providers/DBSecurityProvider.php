<?php
namespace PlayHP\Security\Providers;

use Exception;
use PlayHP\DB\DBObjectMeta;
use PlayHP\DB\DBService;
use PlayHP\Security\ISecurityProvider;
use PlayHP\Security\Model\SecurityContext;
use PlayHP\Security\Model\User;

/**
 * Database security provider
 */
class DBSecurityProvider implements ISecurityProvider
{
    /**
     * @var DBService
     */
    protected $_service;

    /**
     * @param DBService $dbService @Inject
     */
    function __construct($dbService)
    {
        $this->_service = $dbService;
    }

    /**
     * @inheritdoc
     */
    function getUserByLoginPassword($login, $password)
    {
        return $this->_service->fetchFirst('User', array('login' => $login, 'password' => $password));
    }

    /**
     * @inheritdoc
     */
    function getUserCredentials(User $user, SecurityContext $context = null)
    {
        return $this->_service->fetchRelation($user, 'profiles');
    }

    /**
     * @inheritdoc
     */
    function getAllUsers()
    {
        return $this->_service->fetchAll('User');
    }

    /**
     * @inheritdoc
     */
    function hasCredential($right, User $user, SecurityContext $context = null)
    {
        if ($user->profiles === null) {
            $this->_service->fetchRelation($user, 'profiles');
        }
        return in_array($right, $user->profiles);
    }

    /**
     * Fetches a user based on login
     * @param string $login Input login
     * @return User
     */
    public function getUserByLogin($login)
    {
        $user = $this->_service->fetchFirst('User', array(
            'login' => $login
        ));
        return $user;
    }

    /**
     * Saves provided user
     * @param User $user
     * @return bool Save status (true = OK, false = KO)
     */
    public function save($user)
    {
        $existing = $this->getUserByLogin($user->login);
        try {
            if ($existing === null) {
                $this->_service->insert($user);
            } else {
                $this->_service->update($user);
            }
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * Remove accounts marked for removal depending on provided time
     * @param int $time Time limit to use for removing accounts
     * @return int Number of removed accounts
     */
    public function removeAccounts($time)
    {
        $meta = DBObjectMeta::getMeta('User');
        $sql = sprintf('DELETE FROM {schema}%s WHERE %s < %d', $meta->tableName, $meta->getColumn('removalDate'), $time);
        return $this->_service->execute($sql);
    }
}

<?php
namespace PlayHP\Security\Providers;

use PlayHP\Exceptions\MissingFileException;
use PlayHP\Security\Crypto\Crypto;
use PlayHP\Security\ISecurityProvider;
use PlayHP\Security\Model\SecurityContext;
use PlayHP\Security\Model\User;
use SimpleXMLElement;

/**
 * File security provider
 */
class XMLSecurityProvider implements ISecurityProvider
{
    /**
     * Path to configuration of the XML repository
     * @var string
     */
    private $_configPath;

    /**
     * XML Configuration of the security provider's repository
     * @var \SimpleXMLElement
     */
    private $_conf;

    /**
     * List of users defined in the repository
     * @var User[]
     */
    private $_users;

    /**
     * @Inject
     * Encryption service
     * @var Crypto
     */
    public $crypto;

    /**
     * @param string $repoConfigPath Path to XML configuration file storing users
     */
    function __construct($repoConfigPath)
    {
        $this->_configPath = $repoConfigPath;
    }

    /**
     * Initializes the users repository based on configuration
     * @Init
     * @throws MissingFileException When the configuration file is missing or invalid
     * @throws InvalidConfigurationException When configuration is invalid
     */
    public function initRepository()
    {
        $this->_users = array();
        if (!file_exists($this->_configPath)) {
            throw new MissingFileException('Configuration file is missing: ' . $this->_configPath);
        }
        $this->_conf = simplexml_load_file($this->_configPath);
        if ($this->_conf === false) {
            throw new InvalidConfigurationException('XML security configuration is not properly formatted');
        }

        foreach ($this->_conf->user as $userXml) {
            $user = new User();
            $user->login = (string)$userXml['login'];
            $user->email = (string)$userXml['email'];
            $user->password = (string)$userXml['password'];
            $user->firstName = (string)$userXml['firstName'];
            $user->lastName = (string)$userXml['lastName'];
            $user->token = (string)$userXml['token'];
            $user->tokenExpiration = intval((string)$userXml['tokenExpiration']);
            $user->removalDate = intval((string)$userXml['removalDate']);
            $user->verified = strtolower((string)$userXml['verifiedEmail']) === 'true';
            $user->lastName = (string)$userXml['lastName'];
            $user->profiles = explode(',', (string)$userXml['profiles']);
            $this->_users[$user->login] = $user;
        }
    }

    /**
     * @inheritDoc
     */
    function getUserByLoginPassword($login, $password)
    {
        if ($this->crypto !== null) {
            $password = $this->crypto->encrypt($password);
        }
        foreach ($this->_users as $user) {
            if ($user->login === $login && $user->password === $password) {
                return $user;
            }
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    function getUserCredentials(User $user, SecurityContext $context = null)
    {
        if ($user === null) {
            return null;
        }
        return $user->profiles;
    }

    /**
     * @inheritDoc
     */
    function getAllUsers()
    {
        return $this->_users;
    }

    /**
     * @inheritDoc
     */
    function register(User $user)
    {
        // Check if user with same login already exists
        foreach ($this->_users as $existing) {
            if (strtolower($existing->login) === strtolower($user->login)) {
                throw new UserExistsException('User with login ' . $existing->login . ' already exists. Cannot register.');
            }
        }
        $this->_users[] = $user;
    }

    /**
     * @inheritDoc
     */
    function hasCredential($right, User $user, SecurityContext $context = null)
    {
        if ($user === null) {
            return false;
        }
        return in_array($right, $user->profiles);
    }

    /**
     * @inheritdoc
     */
    public function getUserByLogin($login)
    {
        foreach ($this->_users as $user) {
            if ($user->login === $login) {
                return $user;
            }
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function save($user)
    {
        $xmlUser = $this->_conf->xpath('//user[@login="' . $user->login . '"]');
        if (empty($xmlUser)) {
            $xmlUser = $this->_conf->addChild('user');
            $xmlUser['login'] = $user->login;
        } else {
            $xmlUser = $xmlUser[0];
        }
        $xmlUser['email'] = $user->email;
        $xmlUser['password'] = $user->password;
        $xmlUser['firstName'] = $user->firstName;
        $xmlUser['lastName'] = $user->lastName;
        $xmlUser['verifiedEmail'] = $user->verified ? 'true' : 'false';
        if (is_array($user->profiles)) {
            $xmlUser['profiles'] = implode(',', $user->profiles);
        }
        $this->removeOrUpdate($user, $xmlUser, 'removalDate');
        $this->removeOrUpdate($user, $xmlUser, 'token');
        $this->removeOrUpdate($user, $xmlUser, 'tokenExpiration');

        $this->_conf->asXML($this->_configPath);
        $this->_users[$user->login] = $user->login;
        return true;
    }

    /**
     * @param User $user
     * @param SimpleXMLElement $xmlUser
     * @param string $property
     */
    private function removeOrUpdate(&$user, &$xmlUser, $property)
    {
        if ($user->$property === null && isset($xmlUser[$property])) {
            unset($xmlUser[$property]);
        } else {
            $xmlUser[$property] = $user->$property;
        }
    }

    /**
     * @inheritdoc
     */
    public function removeAccounts($time)
    {
        $doc = new \DOMDocument();
        $doc->load($this->_configPath);
        $xpath = new \DOMXPath($doc);
        $xmlUsers = $xpath->query('//user[@removalDate < "' . $time . '"]');
        if (empty($xmlUsers)) {
            return 0;
        }
        /* @var \DOMNode $xml */
        foreach ($xmlUsers as $xml) {
            $xml->parentNode->removeChild($xml);
        }
        $doc->save($this->_configPath);
    }
}

<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Security;


/**
 * Thrown when router prevents from accessing a route because of current user's rights
 * @package PlayHP\Routing\Exceptions
 */
class SecurityException extends \Exception
{

}
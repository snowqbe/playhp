<?php

namespace PlayHP\Security;

use PlayHP\Mail\MailService;
use PlayHP\PlayHP;
use PlayHP\Routing\HttpMethod;
use PlayHP\Routing\Route;
use PlayHP\Security\Crypto\Crypto;
use PlayHP\Security\Model\User;

/**
 * Base class for handling security.
 * Mainly retains authenticated user and security context.
 * Relies on the security provider for the rest.
 */
class SecurityService
{
    // -----------------------------------------------------------------------------------------------------------------
    // CONSTANTS
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * @var string Key for storing security context value
     */
    const KEY_CONTEXT = 'PlayHP::Security::Context';

    /**
     * @var string Key for storing user login
     */
    const KEY_USER = 'PlayHP::Security::User';

    // -----------------------------------------------------------------------------------------------------------------
    // PUBLIC PROPERTIES
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * @Config(security.removeAccountDelay)
     * @var int Delay in days after which we should effectively delete accounts mark for removal
     */
    public $removeAccountDelay = 10;

    /**
     * @var string Route for logging in the user
     */
    public $loginRoute = '/@login';

    /**
     * @var string Route for verifying user credentials
     */
    public $verifyCredentialsRoute = '/@auth';

    /**
     * @var string Route for logging out the user
     */
    public $logoutRoute = '/@logout';

    /**
     * @var string Route to the forgot password view
     */
    public $forgotRoute = '/@forgot';

    /**
     * @var string Route to the change password view
     */
    public $changePasswordRoute = '/@changepwd';

    /**
     * @var string Route to the reset password routine
     */
    public $resetPasswordRoute = '/@resetpwd';

    /**
     * @var string Route to the register a user
     */
    public $registerRoute = '/@register';

    /**
     * @var string The route to account verification
     */
    public $verifyAccountRoute = '/@verify/{token}/{login}';

    /**
     * @var string Route to delete current user's account
     */
    public $deleteAccountRoute = '/@delaccount';

    /**
     * @var string The fully qualified name of the function to call when user wants to log in
     */
    public $loginTarget = 'login';

    /**
     * @var string The fully qualified name of the function to call when user wants to log out
     */
    public $logoutTarget = 'logout';

    /**
     * @var string The fully qualified name of the function to call when verifying provided credentials
     */
    public $verifyCredentialsTarget = 'verifyCredentials';

    /**
     * @var string The fully qualified name of the function to call when user forgot his password
     */
    public $forgotTarget = 'forgot';

    /**
     * @var string Route to the change password view
     */
    public $changePasswordTarget = 'changePassword';

    /**
     * @var string The fully qualified name of the function to call when user forgot his password
     */
    public $resetPasswordTarget = 'resetPassword';

    /**
     * @var string The fully qualified name of the function to call when user forgot his password
     */
    public $registerTarget = 'register';

    /**
     * @var string The target invoked for verifying account
     */
    public $verifyAccountTarget = 'verifyAccount';

    /**
     * @var string The target invoked for verifying account
     */
    public $deleteAccountTarget = 'deleteAccount';

    /**
     * @var string Name of the AuthController class to use (defaults to PlayHP's AuthController)
     */
    public $authControllerClass = 'PlayHP\Security\Controllers\AuthController';

    // -----------------------------------------------------------------------------------------------------------------
    // PRIVATE PROPERTIES
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * @var User Currently authenticated user
     */
    private $_authenticatedUser;

    // -----------------------------------------------------------------------------------------------------------------
    // INJECTED PROPERTIES
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * The security data provider
     * @Inject
     * @var ISecurityProvider
     */
    public $provider;

    /**
     * @Inject
     * @var Crypto The cryptography service
     */
    public $crypto;

    /**
     * @Inject
     * @var MailService The mailing service
     */
    public $mailService;

    /**
     * @var string Default role
     */
    public $defaultRole = 'USER';

    /**
     * Shows the login page
     */
    public function promptLogin()
    {
        PlayHP::forward($this->loginRoute);
    }

    /**
     * @return User The currently authenticated user
     */
    public function getAuthenticatedUser()
    {
        if ($this->_authenticatedUser !== null) {
            return $this->_authenticatedUser;
        }
        // TODO: generic storage
        if (isset($_SESSION[self::KEY_USER])) {
            $this->_authenticatedUser = $this->provider->getUserByLogin($_SESSION[self::KEY_USER]);
            return $this->_authenticatedUser;
        }
        return null;
    }

    /**
     * Returns user based on provided login or null if not exists
     * @param string $login Login of searched user
     * @return \PlayHP\Security\Model\User Found user or null if no user has provided login
     */
    public function getUserByLogin($login)
    {
        return $this->provider->getUserByLogin($login);
    }

    /**
     * Authenticates the user using provided parameters
     * @param string $login Entered login
     * @param string $password Entered password
     * @return User
     */
    public function authenticate($login, $password)
    {
        $user = $this->provider->getUserByLoginPassword($login, $this->crypto->encrypt($password));
        if ($user !== null && $user->verified) {
            $this->storeCurrentUser($user);
        }
        return $user;
    }

    /**
     * Checks whether or not currently authenticated user has a given right (or profile or whatever you call this)
     * @param string $right Right to check for current user
     * @return bool Returns true if current user has appropriate right/profile, false otherwise
     */
    public function hasRight($right)
    {
        $user = $this->getAuthenticatedUser();
        if ($user === null) {
            return false;
        }
        if ($right === '*') {
            return true;
        }
        return in_array($right, $user->profiles);
    }

    /**
     * Registers a new user in the application, and creates an email validation token if email is not marked as verified
     * @param User $user User to register
     * @return bool Save status
     */
    public function register(User $user)
    {
        $user->password = $this->crypto->encrypt($user->password);
        if ($user->profiles === null || empty($user->profiles)) {
            $user->profiles = array($this->defaultRole);
        }
        if (!$user->verified) {
            $this->createUserToken($user);
        }
        return $this->provider->save($user);
    }

    /**
     * Logs the user out
     */
    public function logout()
    {
        $this->storeCurrentUser(null);
    }

    /**
     * Stores current user in session-level storage
     * @param null|User $user User to mark as authenticated, null to delete
     */
    private function storeCurrentUser($user)
    {
        // TODO: generic storage
        if ($user === null) {
            unset($_SESSION[self::KEY_USER]);
        } else {
            $_SESSION[self::KEY_USER] = $user->login;
        }
    }

    /**
     * @Init
     * Initializes the router with security routes for web authentication
     */
    public function initRouter()
    {
        $router = PlayHP::router();

        // Add security routes
        $router->addRoute($this->loginRoute . '/{returnUrl}', HttpMethod::$GET, $this->authControllerClass . '.' . $this->loginTarget);
        $router->addRoute($this->loginRoute, HttpMethod::$GET, $this->authControllerClass . '.' . $this->loginTarget);
        $router->addRoute($this->verifyCredentialsRoute, HttpMethod::$POST, $this->authControllerClass . '.' . $this->verifyCredentialsTarget);
        $router->addRoute($this->logoutRoute, null, $this->authControllerClass . '.' . $this->logoutTarget);
        $router->addRoute($this->forgotRoute, null, $this->authControllerClass . '.' . $this->forgotTarget);
        $router->addRoute($this->resetPasswordRoute, null, $this->authControllerClass . '.' . $this->resetPasswordTarget);
        $router->addRoute($this->registerRoute, null, $this->authControllerClass . '.' . $this->registerTarget);
        $router->addRoute($this->verifyAccountRoute, null, $this->authControllerClass . '.' . $this->verifyAccountTarget);

        // Limited to connected user
        $route = new Route();
        $route->setPathAndTarget($this->deleteAccountRoute, $this->authControllerClass . '.' . $this->deleteAccountTarget);
        $route->allowed = array('*');
        $router->addRoute($route);

        $route = new Route();
        $route->setPathAndTarget($this->changePasswordRoute, $this->authControllerClass . '.' . $this->changePasswordTarget);
        $route->allowed = array('*');
        $router->addRoute($route);
    }

    /**
     * Resets provided user's password
     * @param User $user User whose password should be reset
     * @return string The generated password
     */
    public function resetPassword(User $user)
    {
        $password = substr(md5($this->crypto->getSalt() . substr(microtime(), -4)), 0, 5);
        $user->password = $this->crypto->encrypt($password);
        $this->createUserToken($user);
        $this->provider->save($user);
        return $password;
    }

    /**
     * Compares two passwords with each other
     * @param string $targetPassword Unencrypted password to compare
     * @param string $existingPassword Existing, encrypted password to compare against
     * @return bool true if the two passwords are equal, false otherwise
     */
    public function comparePasswords($existingPassword, $targetPassword)
    {
        if ($this->crypto !== null) {
            $targetPassword = $this->crypto->encrypt($targetPassword);
        }
        return $existingPassword === $targetPassword;
    }

    /**
     * Updates a given user's password
     * @param User $user User to update
     * @param string $password New user's password
     */
    public function updateUserPassword(User $user, $password)
    {
        $user->password = $this->crypto ? $this->crypto->encrypt($password) : $password;
        $user->token = null;
        $user->tokenExpiration = null;
        $this->provider->save($user);
    }

    /**
     * @param User $user User whose account should be verified
     * @param string $token Validation token
     * @return bool Status of account validation
     */
    public function verifyAccount($user, $token)
    {
        // Check token
        if ($user->token == $token) {
            // Check token expiration
            if ($user->tokenExpiration > time()) {
                $user->verified = true;
                $user->token = null;
                $user->tokenExpiration = null;
                $this->provider->save($user);

                // Consider user logged in
                $this->storeCurrentUser($user);
                return true;
            }
        }
        return false;
    }

    /**
     * @param User $user
     */
    public function createUserToken(User $user)
    {
        $user->token = $this->crypto->encrypt(md5(rand()));
        $user->tokenExpiration = time() + 3600;
    }


    /**
     * @param $foundRoute
     * @return bool true if
     * @throws SecurityException
     */
    public function checkRouteSecurity($foundRoute)
    {
        $isAllowed = true;
        if ($foundRoute->allowed !== null) {
            $isAllowed = false;
            foreach ($foundRoute->allowed as $allowed) {
                if ($this->hasRight($allowed)) {
                    $isAllowed = true;
                    break;
                }
            }
        }
        if (!$isAllowed) {
            throw new SecurityException('Current user is not allowed to access this route');
        }
        if ($foundRoute->denied !== null) {
            foreach ($foundRoute->denied as $denied) {
                if ($this->hasRight($denied)) {
                    throw new SecurityException('Current user is not allowed to access this route');
                }
            }
        }
    }

    /**
     * Marks a user account for removal so that the removal job gets rid of it after specified time
     * @param User $user User to remove
     */
    public function markAccountForRemoval($user)
    {
        $user->removalDate = time() + intval($this->removeAccountDelay);
        $this->provider->save($user);
    }

    /**
     * Sets provided user as authenticated
     * @param User $user User to consider authenticated
     */
    public function setAuthenticated($user)
    {
        $this->storeCurrentUser($user);
    }
}

<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Security\Validator;


use PlayHP\Security\Model\User;
use PlayHP\Security\SecurityService;
use PlayHP\Validation\Validator;

class ExistingUserValidator extends Validator
{
    /**
     * @Inject
     * @var SecurityService
     */
    public $securityService;

    /**
     * Validates provided value and returns the list of validation errors
     * @param string $fieldName Name of the field to valide (for error messages)
     * @param mixed $fieldValue Value of the field to validate
     * @return array|bool List of errors found by the validator (localized) or false if no error
     */
    function validate($fieldName, $fieldValue)
    {
        if ($this->securityService === null) {
            return false;
        }
        /* @var User $fieldValue */
        $errors = false;
        $existingUser = $this->securityService->getUserByLogin($fieldValue->$fieldName);
        if ($existingUser !== null) {
            $errors = array(sprintf(_p('A user already exists with this login: %s'), $fieldValue->$fieldName));
        }
        return $errors;
    }

    /**
     * Builds the custom validator
     */
    function __construct()
    {
        $this->passObject = true; // We want the whole user object to be passed
    }

    /**
     * @inheritdoc
     */
    public function hydrateInPool()
    {
        return true; // We're using the security service
    }

}
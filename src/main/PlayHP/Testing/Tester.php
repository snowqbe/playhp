<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Testing;

use PlayHP\Controllers\Controller;
use PlayHP\IO\FileSystem;
use PlayHP\PlayHP;

/**
 * Test controller
 */
class Tester extends Controller
{
    /**
     * Max depth for stack trace
     */
    const DEPTH = 6;

    /**
     * Lists test package looking for unit tests
     * @return array List of launchable unit tests
     */
    private function listPackages()
    {
        $absoluteFiles = array();
        $testFiles = array();
        FileSystem::listPath(APP_TEST_PATH, $testFiles, '');
        $packages = array();
        foreach ($testFiles as $testFile) {
            if (strtolower(substr($testFile, stripos($testFile, '.'))) != '.php') {
                continue;
            }
            require_once APP_TEST_PATH . $testFile;
            $absoluteFiles[] = APP_TEST_PATH . $testFile;
            $testFile = str_replace('/', '\\', $testFile);
            $package = substr($testFile, 0, strrpos($testFile, '\\'));
            $className = substr($testFile, strrpos($testFile, '\\') + 1, -4);
            $reflect = new \ReflectionClass($package . "\\" . $className);
            if (!$reflect->isAbstract() && in_array('PHPUnit_Framework_Test', $reflect->getInterfaceNames())) {
                if (!isset($packages[$package])) {
                    $packages[$package] = array();
                }
                $classMethods = array();
                $methods = $reflect->getMethods();
                foreach ($methods as $method) {
                    if ($method->isPublic() && $method->getDeclaringClass() == $reflect && !in_array($method->getName(), array('setUp', 'tearDown'))) {
                        $classMethods[$method->getName()] = $method;
                    }
                }
                $packages[$package][$className] = $classMethods;
            }
        }
        return $packages;
    }

    /**
     * Runs a test suite depending on selected elements
     * @param string $selectedPackage Selected package for testing @Field
     * @param string $selectedClass Selected class to test @Field
     * @param array $selectedMethods List of selected methods to test @Field
     * @param boolean $all Tells to relaunch all tests @Field
     */
    public function runTestSuite($selectedPackage = null, $selectedClass = null, $selectedMethods = null, $all = true)
    {
        $packages = $this->listPackages();
        list($alerts, $executionTime) = $this->createAndRunTestSuite($selectedPackage, $selectedClass, $selectedMethods, $all, $packages);
        $this->render('tester', array(
            'packages' => $packages,
            'selectedPackage' => $selectedPackage,
            'selectedClass' => $selectedClass,
            'selectedMethods' => $selectedMethods,
            'alerts' => $alerts,
            'executionTime' => $executionTime,
            'deep' => self::DEPTH,
        ));
    }

    /**
     * Shows available tests a test suite depending on selected elements
     */
    public function showTests()
    {
        $packages = $this->listPackages();
        $this->render('tester', array(
            'packages' => $packages,
            'selectedPackage' => null,
            'selectedClass' => null,
            'selectedMethods' => array(),
            'alerts' => null,
            'executionTime' => null,
            'deep' => self::DEPTH,
        ));
    }

    /**
     * @param $selectedPackage
     * @param $selectedClass
     * @param $selectedMethods
     * @param $all
     * @param $packages
     * @return \PHPUnit_Framework_TestResult The results of the test suite
     */
    public function createAndRunTestSuite($selectedPackage, $selectedClass, $selectedMethods, $all, $packages)
    {
        $result = null;
        $suite = new \PHPUnit_Framework_TestSuite();
        // Get test to execute
        if (empty($selectedPackage) || empty($selectedClass) || $all) {
            $selectedClass = null;
            $selectedPackage = null;
            foreach ($packages as $package => $testClasses) {
                foreach ($testClasses as $className => $testMethods) {
                    foreach ($testMethods as $methodName => $method) {
                        $this->addTestToSuite($package, $className, $methodName, $suite);
                    }
                }
            }

        } else {
            $testMethods = $packages[$selectedPackage][$selectedClass];
            /* @var \ReflectionMethod $method */
            foreach ($testMethods as $methodName => $method) {
                if ($selectedMethods === null || (is_array($selectedMethods) && in_array($methodName, $selectedMethods))) {
                    $this->addTestToSuite($selectedPackage, $selectedClass, $methodName, $suite);
                }
            }
        }
        $result = $suite->run();

        $alerts = array();

        // Result beautifier
        if ($result->failureCount() > 0 || $result->errorCount() > 0) {
            $allErrors = array_merge($result->errors(), $result->failures());
            /* @var \PHPUnit_Framework_TestFailure $failure */
            foreach ($allErrors as $failure) {
                /* @var \PHPUnit_Framework_TestCase $test */
                $test = $failure->failedTest();
                $e = $failure->thrownException();
                $trace = $e->getTrace();
                $line = 'unknown';
                $class = 'Unknown';
                foreach ($trace as $i => $traceInfos) {
                    if ($traceInfos['function'] == $test->getName()) {
                        $line = $trace[$i - 1]['line'];
                        $class = $traceInfos['class'];
                        break;
                    }
                }
                $classParts = explode('\\', $class);
                $classPackage = substr($class, 0, strrpos($class, '\\'));
                $lastPart = $classParts[count($classParts) - 1];
                $execUrl = PlayHP::router()->reverse('PlayHP\Testing\Tester', 'runTestSuite', array(
                    'selectedPackage' => $classPackage,
                    'selectedClass' => $lastPart,
                    'selectedMethods' => array($test->getName())
                ));
                $alerts[] = array(
                    'line' => $line,
                    'testName' => $test->getName(),
                    'statusMessage' => $test->getStatusMessage(),
                    'testClassName' => $lastPart,
                    'execUrl' => $execUrl,
                    'testClassPackage' => $classPackage,
                    'stackTrace' => $trace,
                );
            }
        }
        return array($alerts, $result->time());
    }


    /**
     * @param string $package Package name
     * @param string $className Class name
     * @param string $methodName Method name
     * @param \PHPUnit_Framework_TestSuite $suite
     */
    private function addTestToSuite($package, $className, $methodName, $suite)
    {
        $absoluteClassName = $package . '\\' . $className;
        /* @var \PHPUnit_Framework_TestCase $test */
        $test = new $absoluteClassName();
        $test->setName($methodName);
        $suite->addTest($test);
    }
}

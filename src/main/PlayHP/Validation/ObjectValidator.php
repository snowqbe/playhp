<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Validation;
use PlayHP\Lang\Annotation;
use PlayHP\Lang\ClassMeta;
use PlayHP\PlayHP;
use PlayHP\Validation\Validators\BoolValidator;
use PlayHP\Validation\Validators\CheckedValidator;
use PlayHP\Validation\Validators\CompareValidator;
use PlayHP\Validation\Validators\EmailValidator;
use PlayHP\Validation\Validators\RangeValidator;
use PlayHP\Validation\Validators\RequiredValidator;
use ReflectionClass;
use ReflectionMethod;
use ReflectionProperty;


/**
 * Validates annotated objects
 * @package PlayHP\Validation
 */
class ObjectValidator
{
    static $validationAnnotations = array(
        'Checked',
        'Validator',
        'Equals',
        'Before',
        'After',
        'Email',
        'Required',
    );

    /**
     * @param string|$objectOrClassName
     * @param string $methodName Name of the method on which we should parse parameters
     * @throws ValidationException
     * @return ValidationMeta The parsed meta data for given object / class
     */
    protected function getValidationMeta($objectOrClassName, $methodName = null)
    {
        if ($objectOrClassName === null) {
            return null;
        }
        $classLoader = PlayHP::classLoder();

        // Cleanup param
        /* @var ReflectionClass $reflect */
        $reflect = null;
        if (is_object($objectOrClassName)) {
            if (is_a($objectOrClassName, '\ReflectionClass')) {
                $className = $objectOrClassName->name;
                $reflect = $objectOrClassName;
            } else {
                $className = get_class($objectOrClassName);
            }
        } else {
            $className = (string)$objectOrClassName;
        }
        $key = $className;
        if ($methodName !== null) {
            $key .= '::' . $methodName;
        }

        // Get existing meta
        $existingMeta = $classLoader->getMeta(ClassMeta::$Validation, $key);
        if ($existingMeta !== null) {
            // Already parsed
            return $existingMeta;
        }

        // Non existent, parse
        if ($reflect === null) {
            $reflect = $classLoader->getReflectionClass($className);
        }
        $validators = null;
        $existingMeta = new ValidationMeta();
        $existingMeta->className = $reflect->name;
        if ($methodName !== null) {
            if (!$reflect->hasMethod($methodName)) {
                throw new ValidationException('No method named ' . $methodName . ' in class ' . $className);
            }
            $existingMeta->methodName = $methodName;
            $method = $reflect->getMethod($methodName);
            $validators = $this->parseMethodValidators($method);

        } else {
            $validators = $this->parseClassValidators($reflect);
        }
        // Store meta and return
        $existingMeta->validators = $validators;
        $classLoader->setMeta(ClassMeta::$Validation, $key, $existingMeta);
        return $existingMeta;
    }

    /**
     * @param ReflectionClass $reflect
     * @return array|null List of parsed validator
     */
    protected function parseClassValidators($reflect)
    {
        $validators = array();
        $publicProperties = $reflect->getProperties(ReflectionProperty::IS_PUBLIC);
        foreach ($publicProperties as $property) {
            // Check for documentation
            $doc = $property->getDocComment();
            if (empty($doc)) {
                return null;
            }
            // List annotations
            $propAnnotations = Annotation::parseAnnotations($doc, self::$validationAnnotations);
            $rangeValidator = null; // Auto detection of 'between' validation using both Before and After annotations
            foreach ($propAnnotations as $annotation) {
                $validator = $this->parseValidator($property->name, $annotation, $rangeValidator);
                // Add validator
                if ($validator !== null) {
                    if (!array_key_exists($property->name, $validators)) {
                        $validators[$property->name] = array($validator);
                    } else {
                        $validators[$property->name][] = $validator;
                    }
                }
            }
        }
        return $validators;
    }

    /**
     * Parses validators from a method signature (looks for sub annotation in (at)param annotations)
     * @param ReflectionMethod $method The method name
     * @return array The list of validators to update
     */
    protected function parseMethodValidators($method)
    {
        $validators = array();
        $doc = $method->getDocComment();
        $annotations = Annotation::parseAnnotations($doc, array('param'));
        foreach ($annotations as $annotation) {
            $paramName = Annotation::getParamName($annotation);
            $rangeValidator = null;
            if ($annotation->subAnnotations === null) {
                continue;
            }
            foreach ($annotation->subAnnotations as $subAnnotation) {
                $validator = $this->parseValidator($paramName, $subAnnotation, $rangeValidator);
                // Add validator
                if ($validator !== null) {
                    if (!array_key_exists($paramName, $validators)) {
                        $validators[$paramName] = array($validator);
                    } else {
                        $validators[$paramName][] = $validator;
                    }
                }
            }
        }
        return $validators;
    }

    /**
     * Validates a given object
     * @param mixed $object Object to validate
     * @param string|null|array $profiles Validation profile(s) to use, null for all
     * @throws \Exception|ValidationException
     * @return array|bool List of errors or false if no error
     */
    public function validateObject($object, $profiles = null)
    {
        try {
            $validationMeta = $this->getValidationMeta($object);
        } catch (ValidationException $e) {
            // TODO: log?
            throw $e;
        }
        return $this->validateFromMeta($object, $profiles, $validationMeta);


    }

    /**
     * @private
     * @param Annotation $annotation Annotation to check
     * @return bool true if the annotation concerns validation, false otherwise
     */
    private function isValidatorAnnotation(Annotation $annotation)
    {
        return in_array($annotation->name, self::$validationAnnotations);
    }

    /**
     * @param string $elementName Name of the element on which validation annotation is parsed (property or parameter)
     * @param Annotation $annotation Annotation to parse
     * @param Validator $rangeValidator Existing Before of After validator to merge into a Range validator
     * @return Validator The parsed validator or null if none found
     * @throws ValidationException
     */
    protected function parseValidator($elementName, $annotation, &$rangeValidator)
    {
        $classLoader = PlayHP::classLoder();
        $validator = null; // Current validator instance
        $profiles = null;
        if ($this->isValidatorAnnotation($annotation)) {
            $profilesParam = $annotation->getOptionalParam('profiles');
            if ($profilesParam !== null) {
                $profiles = explode(',', $profilesParam);
            }
        }

        switch ($annotation->name) {
            case 'Validator':
                // Custom validation class
                $validatorClassName = $annotation->getDefaultParam('class');
                if ($validatorClassName === null) {
                    throw new ValidationException('No custom validator defined in annotation "Validator" for property "' . $elementName . '"');
                }
                $validatorClass = $classLoader->getReflectionClass($validatorClassName);
                $validator = $validatorClass->newInstance();
                if (!is_subclass_of($validator, 'PlayHP\Validation\Validator')) {
                    throw new ValidationException('Custom validator class "' . $validatorClassName . '" should be a subclass of PlayHP\Validation\Validator');
                }
                break;
            case 'Checked':
                $validator = new CheckedValidator();
                break;
            case 'Required':
                $validator = new RequiredValidator();
                break;
            case 'Email':
                $validator = new EmailValidator();
                break;
            case 'Equals':
                $compareField = $annotation->getDefaultParam('field');
                if ($compareField === null) {
                    throw new ValidationException('No comparison field defined for "Before" annotation on property "' . $elementName . '"');
                }
                $validator = new CompareValidator();
                $validator->compareWith = $compareField;
                break;
            case 'Before':
                $compareField = $annotation->getDefaultParam('field');
                if ($compareField === null) {
                    throw new ValidationException('No comparison field defined for "Before" annotation on property "' . $elementName . '"');
                }
                // Reuse current property's range validator if relevant
                if ($rangeValidator === null) {
                    $rangeValidator = new RangeValidator();
                }
                $rangeValidator->maxValue = $compareField;
                $validator = $rangeValidator;
                break;
            case 'After':
                $compareField = $annotation->getDefaultParam('field');
                if ($compareField === null) {
                    throw new ValidationException('No comparison field defined for "Before" annotation on property "' . $elementName . '"');
                }
                // Reuse current property's range validator if relevant
                if ($rangeValidator === null) {
                    $rangeValidator = new RangeValidator();
                }
                $rangeValidator->minValue = $compareField;
                $validator = $rangeValidator;
                break;
            case 'var':
                // Get property type to automatically guess based on property type
                if ($annotation->hasParams()) {
                    $propType = strtolower(trim($annotation->getDefaultParam()));
                    switch ($propType) {
                        case 'int':
                        case 'integer':
                            $validator = new RangeValidator();
                            break;
                        case 'boolean':
                        case 'bool':
                            $validator = new BoolValidator();
                            break;
                    }
                }
                break;
        }
        if ($validator !== null) {
            $validator->validationProfiles = $profiles;
        }
        return $validator;
    }

    /**
     * Validates a method call based on provided parameters.
     * @param array $lastCall The call information you got from a debug_backtrace() call
     * @param array|string $profiles Validation profile or list of validation profiles
     * @throws ValidationException
     * @throws \Exception|ValidationException
     * @return array List of validation errors
     */
    public function validateMethodCall($lastCall, $profiles)
    {
        if (!is_array($lastCall)
            || !array_key_exists('function', $lastCall)
            || !array_key_exists('class', $lastCall)
            || !array_key_exists('args', $lastCall)
        ) {
            throw new ValidationException('Invalid call array given');
        }
        $ldr = PlayHP::classLoder();
        $methodName = $lastCall['function'];
        $controllerClassName = $lastCall['class'];
        $reflect = $ldr->getReflectionClass($controllerClassName);
        $method = $reflect->getMethod($methodName);
        $parameters = $method->getParameters();
        $args = $lastCall['args'];

        $object = new \stdClass();
        foreach ($parameters as $i => $parameter) {
            $paramName = $parameter->name;
            $object->$paramName = $args[$i];
        }

        try {
            $validationMeta = $this->getValidationMeta($controllerClassName, $methodName);
        } catch (ValidationException $e) {
            // TODO: log?
            throw $e;
        }
        return $this->validateFromMeta($object, $profiles, $validationMeta);
    }

    /**
     * Performs the validation of provided object based on profile and validation meta data
     * @param mixed $object Object to validate
     * @param string|string[] $profiles List of profiles to validate
     * @param ValidationMeta $validationMeta Metadata to use for validating
     * @return array|bool List of errors or false if no error
     */
    public function validateFromMeta($object, $profiles, $validationMeta)
    {
        $errors = false;
        // Is there something to do?
        if (empty($validationMeta)) {
            return $errors;
        }

        // Param cleanup
        if (is_string($profiles)) {
            $profiles = array($profiles);
        }

        // For hydratation
        $pool = PlayHP::pool();

        // Trigger all validators
        foreach ($validationMeta->validators as $propertyName => $validators) {
            /* @var Validator $validator $ */
            foreach ($validators as $validator) {

                // Check if validator should be triggered depending on selected profile(s)
                if (!$validator->isEnabled($profiles)) {
                    continue;
                }

                // Handle passing object as value
                if ($validator->passObject) {
                    $fieldValue = $object;
                } else {
                    $fieldValue = $object->$propertyName;
                }

                // Needs to by hydrated?
                if ($validator->hydrateInPool()) {
                    $pool->hydrate($validator);
                }

                // Proceed with validation
                $localErrors = $validator->validate($propertyName, $fieldValue);

                // Merge errors if new ones were found
                if ($localErrors !== false) {
                    if ($errors === false) {
                        $errors = $localErrors;
                    } else {
                        $errors = array_merge($errors, $localErrors);
                    }
                }
            }
        }

        return $errors;
    }

}
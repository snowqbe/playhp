<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Validation;


use Exception;

/**
 * Triggered on validation parsing errors.
 * @package PlayHP\Validation
 */
class ValidationException extends Exception
{

}
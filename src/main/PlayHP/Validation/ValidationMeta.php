<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Validation;


/**
 * Validation definition to use for validating a given object
 * @package PlayHP\Validation
 */
class ValidationMeta
{

    /**
     * @var string The class name
     */
    public $className;

    /**
     * @var string the name of the method on which meta data apply
     */
    public $methodName;

    /**
     * @var array List of validators indexed by property name
     * @see Validator
     */
    public $validators;

}
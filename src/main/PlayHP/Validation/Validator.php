<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Validation;


/**
 * Validator interface to implement for creating custom validators
 * @package PlayHP\Validation
 */
abstract class Validator
{

    /**
     * @var bool Flag telling to pass the object as $fieldValue parameter of the @link{validate} function instead of
     * directly providing the value to validate
     * @see CompareValidator
     */
    public $passObject = false;

    /**
     * Tells if the validator should have dependencies mapped using the ObjectPool before validation.
     * Should be turned of for performance, but useful for custom validators.
     * @return bool Flag telling to hydrate the validator with objects from the pool.
     */
    public function hydrateInPool()
    {
        return false;
    }

    /**
     * @var string[] List of validation profiles current validator is in to allow partial validation
     */
    public $validationProfiles;

    /**
     * Validates provided value and returns the list of validation errors
     * @param string $fieldName Name of the field to valide (for error messages)
     * @param mixed $fieldValue Value of the field to validate
     * @return array|bool List of errors found by the validator (localized) or false if no error
     */
    abstract function validate($fieldName, $fieldValue);

    /**
     * Checks whether current validator is active depending on currently active validation profiles
     * @param array $profiles List of profile to check against
     * @return bool true if the validator is active, false otherwise
     */
    public function isEnabled($profiles)
    {
        if ($profiles === null || $this->validationProfiles === null) {
            return true;
        }
        if (!is_array($profiles)) {
            $profiles = array((string)$profiles);
        }
        $intersection = array_intersect($profiles, $this->validationProfiles);
        if (empty($intersection)) {
            return false;
        }
        return true;
    }
}
<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Validation\Validators;


use PlayHP\Validation\Validator;

/**
 * Checks if value is equivalent to a boolean
 * @package PlayHP\Validation\Validators
 */
class BoolValidator extends Validator
{

    /**
     * @inheritdoc
     */
    function validate($fieldName, $fieldValue)
    {
        $errors = false;
        if (filter_var($fieldValue, FILTER_VALIDATE_BOOLEAN, array('flags' => FILTER_NULL_ON_FAILURE)) === null) {
            $errors = array(sprintf(_p('Field %s should be a valid boolean'), $fieldName));
        }
        return $errors;
    }
}
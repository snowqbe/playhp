<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Validation\Validators;


use PlayHP\Validation\Validator;

/**
 * Validates a checkbox is ticked
 * @package PlayHP\Validation\Validators
 */
class CheckedValidator extends Validator
{

    /**
     * @inheritdoc
     */
    function validate($fieldName, $fieldValue)
    {
        $errors = false;
        if (!filter_var($fieldValue, FILTER_VALIDATE_BOOLEAN)) {
            $errors = array(sprintf(_p('You must check the field %s'), $fieldName));
        }
        return $errors;
    }
}
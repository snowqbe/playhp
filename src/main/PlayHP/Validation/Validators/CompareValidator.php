<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Validation\Validators;


use PlayHP\Validation\Validator;

/**
 * Compares two fields which should match
 * @package PlayHP\Validation\Validators
 */
class CompareValidator extends Validator
{


    /**
     * Redefines the @link{passObject} property to allow fetching the value to compare with from the same object
     */
    function __construct()
    {
        $this->passObject = true;
    }

    /**
     * @var string Name of the property to compare with
     */
    public $compareWith;

    /**
     * @inheritdoc
     */
    function validate($fieldName, $fieldValue)
    {
        if (is_array($fieldValue)) {
            $value = $fieldValue[$fieldName];
            $compareValue = $fieldValue[$this->compareWith];
        } else if (is_object($fieldValue)) {
            $value = $fieldValue->$fieldName;
            $compareValue = $fieldValue->{$this->compareWith};
        } else {
            return false;
        }
        if ($value === $compareValue) {
            return false;
        }
        return array(sprintf(_p('%s and %s fields must match'), $fieldName, $this->compareWith));
    }
}
<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Validation\Validators;


use PlayHP\Validation\Validator;

/**
 * Validates an email
 * @package PlayHP\Validation\Validators
 */
class EmailValidator extends Validator
{

    /**
     * @inheritdoc
     */
    function validate($fieldName, $fieldValue)
    {
        $errors = false;
        if (!filter_var($fieldValue, FILTER_VALIDATE_EMAIL)) {
            $errors = array(sprintf(_p('Field %s is not a valid email'), $fieldName));
        }
        return $errors;
    }
}
<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Validation\Validators;


use PlayHP\Validation\Validator;

class RangeValidator extends Validator
{

    /**
     * @var int Minimum value
     */
    public $minValue;

    /**
     * @var int Minimum value
     */
    public $maxValue;

    /**
     * @inheritdoc
     */
    function validate($fieldName, $fieldValue)
    {
        $options = array();
        if ($this->maxValue !== null && $this->minValue !== null) {
            $options['min_range'] = $this->minValue;
            $options['max_range'] = $this->maxValue;
            $msg = sprintf(_p('Field %s should stay between %d and %d'), $fieldName, $this->minValue, $this->maxValue);
        } else if ($this->maxValue !== null) {
            $options['max_range'] = $this->maxValue;
            $msg = sprintf(_p('Field %s should stay lower than %d'), $fieldName, $this->maxValue);
        } else if ($this->minValue !== null) {
            $options['min_range'] = $this->minValue;
            $msg = sprintf(_p('Field %s should stay above %d'), $fieldName, $this->minValue);
        } else {
            $msg = sprintf(_p('Field %s should be a valid number'), $fieldName);
        }
        $errors = false;
        if (!filter_var($fieldValue, FILTER_VALIDATE_INT, array('options' => $options))) {
            $errors = array($msg);
        }
        return $errors;
    }
}
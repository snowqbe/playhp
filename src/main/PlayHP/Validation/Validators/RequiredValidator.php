<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHP\Validation\Validators;

use PlayHP\Validation\Validator;

/**
 * Checks required fields
 * @package PlayHP\Validation\Validators
 */
class RequiredValidator extends Validator
{

    /**
     * @inheritdoc
     */
    function validate($fieldName, $fieldValue)
    {
        $trimmed = trim($fieldValue);
        $errors = false;
        if (empty($trimmed)) {
            $errors = array(sprintf(_p('Field %s is required'), $fieldName));
        }
        return $errors;
    }
}
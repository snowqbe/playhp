<?php
namespace PlayHPTests\Controllers\Dummies;

/**
 * Test class which is not a controller
 */
class NotAController
{

    public function index()
    {

    }
}

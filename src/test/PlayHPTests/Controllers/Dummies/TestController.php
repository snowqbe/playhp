<?php

namespace PlayHPTests\Controllers\Dummies;

use PlayHP\Controllers\Controller;

/**
 * Controller created for testing purposes
 */
class TestController extends Controller
{

    public function defaultRender()
    {
        $this->render('test/test');
    }

    public function doPut()
    {
        $this->render('test/method', array('method' => 'PUT'));
    }

    public function doDelete()
    {
        $this->render('test/method', array('method' => 'DELETE'));
    }

    public function doPost()
    {
        $this->render('test/method', array('method' => 'POST'));
    }

    public function layoutRender()
    {
        $this->render('test/testLayout');
    }

    public function textRender()
    {
        $this->renderText('test');
    }

    public function jsonRender()
    {
        $sub = new \stdClass();
        $sub->key = 'first';
        $sub2 = new \stdClass();
        $sub2->key = 'second';
        $obj = new \stdClass();
        $obj->id = 1;
        $obj->content = "test";
        $obj->flag = true;
        $obj->list = array(
            $sub,
            $sub2,
        );

        $this->renderJSON($obj);
    }

}

<?php

namespace PlayHPTests\Controllers\Dummies;

use PlayHP\Controllers\Controller;

/**
 * Test controller A
 */
class TestControllerA extends Controller
{

    public function testA()
    {
    }

    /**
     * @param string $param1 String param field @Field
     * @param int $param2 Integer param field @Field
     * @param array $param3 Array param field @Field
     */
    public function testParams($param1, $param2, $param3)
    {

    }
}
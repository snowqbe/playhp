<?php
namespace PlayHPTests\Controllers\Dummies;

use PlayHP\Controllers\Controller;

/**
 * Test controller C
 */
class TestControllerC extends Controller
{
    public function testC($param)
    {
        $this->renderText($param);
    }

    public function testOK()
    {
        $this->renderText('OK');
    }
}
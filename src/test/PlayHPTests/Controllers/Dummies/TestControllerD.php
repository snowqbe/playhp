<?php
namespace PlayHPTests\Controllers\Dummies;

use PlayHP\Controllers\Controller;

/**
 * Test controller D
 */
class TestControllerD extends Controller
{
    public function testD()
    {
        $this->renderText('test');
    }
}
<?php
namespace PlayHPTests\Controllers;

use PlayHP\PlayHP;
use PlayHP\Routing\Route;
use PlayHP\Routing\Router;

/**
 * Rendering test class
 */
class RenderTest extends \PHPUnit_Framework_TestCase
{

    public function testDefaultRender()
    {
        $url = $this->buildTestUrl('test/defaultRender');
        $ch = curl_init();
        try {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            $this->assertEquals('<p>test</p>', $output, 'Output: <pre>' . htmlentities($output) . '</pre>');
            curl_close($ch);
        } catch (\Exception $e) {
            curl_close($ch);
            $this->fail($e->getMessage());
        }
    }

    public function testLayoutRender()
    {
        $url = $this->buildTestUrl('test/layoutRender');
        $ch = curl_init();
        try {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            $this->assertEquals('<h1>This is the Layout</h1><p>test</p>', $output, 'Output: <pre>' . htmlentities($output) . '</pre>');
            curl_close($ch);
        } catch (\Exception $e) {
            curl_close($ch);
            $this->fail($e->getMessage());
        }
    }

    public function testRenderText()
    {
        $url = $this->buildTestUrl('test/textRender');
        $ch = curl_init();
        try {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            $this->assertEquals('test', $output, 'Output: <pre>' . htmlentities($output) . '</pre>');
            curl_close($ch);
        } catch (\Exception $e) {
            curl_close($ch);
            $this->fail($e->getMessage());
        }
    }

    public function testRenderJson()
    {
        $url = $this->buildTestUrl('test/jsonRender');
        $ch = curl_init();
        try {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            $this->assertNotEquals(false, $output);
            $decoded = json_decode($output);
            $this->assertEquals(1, $decoded->id);
            $this->assertEquals('test', $decoded->content);
            $this->assertTrue($decoded->flag);
            $this->assertCount(2, $decoded->list);
            $this->assertEquals('first', $decoded->list[0]->key);
            $this->assertEquals('second', $decoded->list[1]->key);
            curl_close($ch);
        } catch (\Exception $e) {
            curl_close($ch);
            $this->fail($e->getMessage());
        }
    }

    public function testPut()
    {
        $url = $this->buildTestUrl('test/putOperation');
        $ch = curl_init();
        try {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PUT, true);
            $output = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $this->assertEquals(200, $httpCode, 'HTTP Code: ' . $httpCode . ' <pre>' . htmlentities($output) . '</pre>');
            curl_close($ch);
        } catch (\Exception $e) {
            curl_close($ch);
            $this->fail($e->getMessage());
        }
    }

    public function testPost()
    {
        $url = $this->buildTestUrl('test/postOperation');
        $ch = curl_init();
        try {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array());
            $output = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $this->assertEquals(200, $httpCode, 'HTTP Code: ' . $httpCode . ' <pre>' . htmlentities($output) . '</pre>');
            curl_close($ch);
        } catch (\Exception $e) {
            curl_close($ch);
            $this->fail($e->getMessage());
        }
    }

    public function testDelete()
    {
        $url = $this->buildTestUrl('test/delOperation');
        $ch = curl_init();
        try {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
            $output = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $this->assertEquals(200, $httpCode, 'HTTP Code: ' . $httpCode);
            $this->assertEquals('DELETE', $output, 'Output: <pre>' . htmlentities($output) . '</pre>');
            curl_close($ch);
        } catch (\Exception $e) {
            curl_close($ch);
            $this->fail($e->getMessage());
        }
    }

    public function testAppropriateMethod()
    {
        $url = $this->buildTestUrl('test/delOperation');
        $ch = curl_init();
        try {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PUT, true);
            curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $this->assertEquals(404, $httpCode, 'Return code: ' . $httpCode);
            curl_close($ch);
        } catch (\Exception $e) {
            curl_close($ch);
            $this->fail($e->getMessage());
        }
    }

    /**
     * @param $path
     * @return string
     */
    private function buildTestUrl($path)
    {
        $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'];
        $url = substr($url, 0, strrpos($url, '/')) . '/test.php/' . $path;
        return $url;
    }
}

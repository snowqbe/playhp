<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHPTests\DB\DBObjects;

/**
 * Class Inherited
 * @package PlayHPTests\DB\DBObjects
 * @Table(inherited)
 */
class Inherited extends TestObject
{

    /**
     * @Column(new_column)
     * @var string
     */
    public $new;

    /**
     * Transient property
     * @var string
     */
    public $transient;
}
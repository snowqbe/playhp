<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHPTests\DB\DBObjects;


/**
 * Test class for multiple primary keys
 * @package PlayHPTests\DB\DBObjects
 * @Table(multiple)
 */
class MultiplePK
{

    /**
     * @Id
     * @var string
     */
    public $login;

    /**
     * @Id
     * @var string
     */
    public $email;
}
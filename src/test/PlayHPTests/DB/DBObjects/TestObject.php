<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHPTests\DB\DBObjects;


/**
 * Class TestObject
 * @package PlayHPTests\DB\DBObjects
 *
 * @Table(existing)
 */
class TestObject
{

    /**
     * @Id(autoIncrement=true, sequenceName="testSequence")
     * @Column(name="existing_id")
     * @var int
     */
    public $id;

    /**
     * @Column
     * @var string
     */
    public $name;

    /**
     * @Column(name="last_update")
     * @var \DateTime
     */
    public $lastUpdate;

    /**
     * @Column(name="price")
     * @var int
     */
    public $price;

    /**
     * @Column(name="email", nullable=false)
     * @var string
     */
    public $email;
}
<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHPTests\DB;

use PlayHP\DB\DBService;

abstract class DBTest extends \PHPUnit_Extensions_Database_TestCase
{
    /**
     * @var \PDO
     */
    static protected $pdo = null;

    /**
     * @var \PHPUnit_Extensions_Database_DB_IDatabaseConnection
     */
    protected $conn = null;

    /**
     * @var DBService
     */
    protected $service = null;

    public function getConnection()
    {
        if ($this->conn === null) {
            $dsn = $this->getDsn();
            $this->service = new DBService($dsn, null, null);
            if (self::$pdo == null) {
                self::$pdo = $this->service->pdo;
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, 'playhp-dbTest');
        }
        return $this->conn;
    }

    /**
     * Returns the test dataset.
     *
     * @return \PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    protected function getDataSet()
    {
        $ds = new \PHPUnit_Extensions_Database_DataSet_QueryDataSet($this->getConnection());
        $ds->addTable('existing');
        $ds->addTable('profiles');
        return $ds;
    }

    /**
     * @return string The DSN to use for this connection
     */
    protected abstract function getDsn();
}
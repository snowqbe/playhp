<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHPTests\DB;


use PlayHPTests\DB\DBObjects\TestObject;

class OperationTest extends DBTest
{

    public function testInsert()
    {
        $obj = new TestObject();
        $obj->email = 'herve.labas@snowqbe.com';
        $obj->lastUpdate = new \DateTime();
        $obj->price = 2;
        $obj->name = 'Test object';
        $this->service->insert($obj);
        $this->assertEquals(2, $obj->id);
        $this->assertEquals(2, $this->service->count('TestObject'));
    }

    public function testDelete()
    {
        $obj = new TestObject();
        $obj->id = 1;
        $this->service->delete($obj);
        $this->assertEquals(0, $this->service->count('TestObject'));
    }

    public function testUpdate()
    {
        /* @var TestObject $obj */
        $obj = $this->service->fetchFirst('TestObject', 'select * from TestObject WHERE id = 1');
        $this->assertEquals(12, $obj->price);
        $obj->price = 999;
        $this->service->update($obj);
        $obj = $this->service->fetchFirst('TestObject', 'select * from TestObject WHERE id = 1');
        $this->assertEquals(999, $obj->price);
    }

    /**
     * @return string The DSN to use for this connection
     */
    protected function getDsn()
    {
        return 'sqlite::memory:';
    }

    public function getConnection()
    {
        // Hack to recreate database before each test as we are checking the core DB features (won't need DBUnit's dataset feature here)
        if (self::$pdo !== null) {
            self::$pdo = null;
            $this->conn = null;
        }
        $conn = parent::getConnection();
        self::$pdo->exec('CREATE TABLE existing (existing_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, last_update DATE, price NUMERIC, email TEXT NOT NULL DEFAULT \'\')');
        self::$pdo->exec('CREATE TABLE profiles (profile_code TEXT PRIMARY KEY)');
        self::$pdo->exec('INSERT INTO \'existing\' VALUES (\'1\', \'First\', \'2013-05-30\', \'12\', \'test@playhp.org\')');
        self::$pdo->exec('INSERT INTO \'profiles\' VALUES (\'USER\')');
        return $conn;
    }


}
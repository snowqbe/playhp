<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHPTests\DB;

use PlayHP\DB\DBObjectMeta;
use PlayHPTests\DB\DBObjects\TestObject;

class ParsingTest extends DBTest
{
    public function testParseTable()
    {
        $testObject = new TestObject();
        $meta = DBObjectMeta::getMeta($testObject);
        $this->assertEquals('existing', $meta->tableName, 'Meta: <pre>' . print_r($meta, true) . '</pre>');
    }

    public function testParseSimpleQuery()
    {
        $result = $this->service->getSQLQuery('select id, name, price from TestObject');
        $this->assertEquals('select existing.existing_id, existing.name, existing.price FROM existing', $result, 'Failed: ' . $result);
    }

    public function testParseSimpleQueryWhere()
    {
        $result = $this->service->getSQLQuery('select id, name, price from TestObject where id = :id');
        $this->assertEquals('select existing.existing_id, existing.name, existing.price FROM existing WHERE existing.existing_id = :id', $result, 'Failed: ' . $result);
    }

    public function testParseJoinQueryTableNameAlias()
    {
        $result = $this->service->getSQLQuery('select TestObject.id, count(TestObject.name) as nb, TestObject.price, Inherited.new from ' .
        'TestObject inner join Inherited on TestObject.id = Inherited.id left join Profile on Profile.profileCode = TestObject.email');
        $this->assertEquals('select  existing.existing_id, count( existing.name) as nb,  existing.price,  inherited.new_column FROM ' .
        'existing inner join inherited on  existing.existing_id =  inherited.existing_id left join profiles on  profiles.profile_code =  existing.email', $result, 'Failed: ' . $result);
    }

    public function testParseComplexQueryTableAlias()
    {
        $result = $this->service->getSQLQuery('select tst.id, count(tst.name) as nb, tst.price, inh.new from ' .
        'TestObject as tst inner join Inherited inh on tst.id = inh.id left join Profile as pro on pro.profileCode = tst.email, MultiplePK mu');
        $this->assertEquals('select  tst.existing_id, count( tst.name) as nb,  tst.price,  inh.new_column FROM existing as tst inner join ' .
        'inherited inh on  tst.existing_id =  inh.existing_id left join profiles as pro on  pro.profile_code =  tst.email, multiple mu', $result, 'Failed: ' . $result);
    }

    public function testParseQueryNoJoin()
    {
        $result = $this->service->getSQLQuery(
            'select tst.id, count(tst.name) as nb, tst.price, inh.new from ' .
            'TestObject as tst, Inherited inh, Profile as pro, MultiplePK mu');
        $this->assertEquals('select  tst.existing_id, count( tst.name) as nb,  tst.price,  inh.new_column FROM existing as tst, inherited inh, ' .
        'profiles as pro, multiple mu', $result, 'Failed: ' . $result);
    }

    public function testParseQueryFullJoin()
    {
        $result = $this->service->getSQLQuery('select tst.id, count(tst.name) as nb, tst.price, inh.new from ' .
        'TestObject as tst inner join Inherited inh on tst.id = inh.id left join Profile as pro on pro.profileCode = tst.email left join MultiplePK mu on mu.login = tst.id');
        $this->assertEquals('select  tst.existing_id, count( tst.name) as nb,  tst.price,  inh.new_column FROM existing as tst inner join inherited inh on  ' .
        'tst.existing_id =  inh.existing_id left join profiles as pro on  pro.profile_code =  tst.email left join multiple mu on  mu.login =  tst.existing_id', $result, 'Failed: ' . $result);
    }

    public function testParseQueryFullJoinWhere()
    {
        $result = $this->service->getSQLQuery('(select tst.id, count(tst.name) as nb from ' .
        'TestObject as tst where id in (select id from TestObject where (price <= 0.5 or price > 12.32))) union (select 1 from dual)');
        $this->assertEquals('(select  tst.existing_id, count( tst.name) as nb FROM existing as tst WHERE tst.existing_id' .
        ' in (select existing.existing_id FROM existing WHERE (existing.price <= 0.5 or existing.price > 12.32))) union (select 1 FROM dual)', $result, 'Failed: ' . $result);
    }

    public function testParseProps()
    {
        $meta = DBObjectMeta::getMeta('TestObject');
        $this->assertCount(5, $meta->propertiesMap, 'Meta: <pre>' . print_r($meta, true) . '</pre>');

        $this->assertArrayHasKey('id', $meta->propertiesMap);
        $this->assertEquals('existing_id', $meta->propertiesMap['id']->columnName);

        $this->assertArrayHasKey('lastUpdate', $meta->propertiesMap);
        $this->assertEquals('last_update', $meta->propertiesMap['lastUpdate']->columnName);
        $this->assertEquals('\DateTime', $meta->propertiesMap['lastUpdate']->columnType);
        $this->assertTrue($meta->propertiesMap['lastUpdate']->nullable);

        $this->assertArrayHasKey('name', $meta->propertiesMap);
        $this->assertEquals('name', $meta->propertiesMap['name']->columnName);

        $this->assertArrayHasKey('price', $meta->propertiesMap);
        $this->assertEquals('price', $meta->propertiesMap['price']->columnName);

        $this->assertArrayHasKey('email', $meta->propertiesMap);
        $this->assertEquals('email', $meta->propertiesMap['email']->columnName);
        $this->assertEquals('string', $meta->propertiesMap['email']->columnType);
        $this->assertFalse($meta->propertiesMap['email']->nullable);
    }

    public function testHasPK()
    {
        $meta = DBObjectMeta::getMeta('TestObject');
        $this->assertTrue($meta->hasPK());
    }

    public function testPKClause()
    {
        $meta = DBObjectMeta::getMeta('TestObject');
        $this->assertEquals('existing_id = :id', $meta->pkClause);
        $meta = DBObjectMeta::getMeta('Profile');
        $this->assertEquals('profile_code = :profileCode', $meta->pkClause);
    }

    public function testParsePK()
    {
        $meta = DBObjectMeta::getMeta('TestObject');
        $nbPk = 0;
        foreach ($meta->propertiesMap as $columnMeta) {
            if ($columnMeta->isInPK && $columnMeta->columnName == 'existing_id') {
                $nbPk++;
            }
        }
        $this->assertEquals(1, $nbPk);
    }

    public function testUniqueId()
    {
        $meta = DBObjectMeta::getMeta('TestObject');
        $uniqueId = $meta->getUniqueId();
        $this->assertNotNull($uniqueId);
        $this->assertEquals('id', $uniqueId->propertyName);
        $this->assertEquals('testSequence', $uniqueId->sequenceName);
        $this->assertTrue($uniqueId->isAutoIncremented);
    }

    public function testInheritance()
    {
        $meta = DBObjectMeta::getMeta('Inherited');
        $this->assertArrayHasKey('new', $meta->propertiesMap);
        $this->assertEquals('inherited', $meta->tableName);
        $this->assertArrayNotHasKey('transient', $meta->propertiesMap);
    }

    /**
     * @return string The DSN to use for this connection
     */
    protected function getDsn()
    {
        return 'sqlite:' . dirname(__FILE__) . '/dbTest.db3';
    }
}
<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHPTests\DB;


use PlayHP\DB\DBObjectMeta;
use PlayHPTests\DB\DBObjects\Profile;
use PlayHPTests\DB\DBObjects\TestObject;

class SelectTest extends DBTest
{

    public function testCount()
    {
        $this->assertEquals(2, $this->service->count('Profile'));
    }

    public function testCountFilter()
    {
        $this->assertEquals(1, $this->service->count('Profile', array('profileCode' => 'USER')));
    }

    public function testFetch()
    {
        $result = $this->service->pqlSelect('select * from TestObject');
        $this->assertCount(1, $result);
        /* @var TestObject $test */
        $test = $result[0];
        $this->assertTrue(is_a($test, 'PlayHPTests\DB\DBObjects\TestObject'));
        $this->assertEquals(1, $test->id);
        $this->assertEquals('test@playhp.org', $test->email);
        $this->assertEquals('First', $test->name);
        $this->assertEquals(12, $test->price);
        $this->assertEquals('30-05-2013', $test->lastUpdate->format('d-m-Y'));
    }

    public function testFetchPartial()
    {
        $result = $this->service->pqlSelect('select id from TestObject');
        $this->assertCount(1, $result);
        $test = $result[0];
        $this->assertTrue(is_a($test, 'PlayHPTests\DB\DBObjects\TestObject'));
        $this->assertEquals(1, $test->id);
        $this->assertNull($test->email);
        $this->assertNull($test->name);
        $this->assertNull($test->price);
        $this->assertNull($test->lastUpdate);
    }

    public function testFetchAssoc()
    {
        $result = $this->service->sqlSelect('SELECT count(*) as nb from profiles');
        $this->assertCount(1, $result);
        $this->assertArrayHasKey('nb', $result[0]);
        $this->assertEquals(2, $result[0]['nb']);
    }

    public function testPqlSimple()
    {
        $profile = new Profile();
        $profile->profileCode = 'USER';
        $sql = 'SELECT * FROM Profile WHERE profileCode = :code';
        list($first) = $this->service->pqlSelect($sql, array(':code' => 'USER'));
        $this->assertEquals($profile, $first);
    }

    public function testFetchFirst()
    {
        /* @var Profile $profile */
        $profile = $this->service->fetchFirst('Profile', array('profileCode' => 'USER'));
        $this->assertNotNull($profile);
        $this->assertEquals('USER', $profile->profileCode);
    }

    public function testFetchAllFilter()
    {
        /* @var \PlayHPTests\DB\DBObjects\Profile[] $profiles */
        $profiles = $this->service->fetchAll('Profile', array('profileCode' => 'USER'));
        $this->assertCount(1, $profiles);
        $this->assertEquals('USER', $profiles[0]->profileCode);
    }

    public function testFetchAllFilterNull()
    {
        /* @var \PlayHPTests\DB\DBObjects\Profile[] $profiles */
        $profiles = $this->service->fetchAll('Profile', array('profileCode' => null));
        $this->assertCount(0, $profiles);
    }

    public function testFetchAllFilterNotNull()
    {
        /* @var \PlayHPTests\DB\DBObjects\Profile[] $profiles */
        $profiles = $this->service->fetchAll('Profile', array('!profileCode' => null));
        $this->assertCount(2, $profiles);
    }

    public function testFetchAllFilterLike()
    {
        /* @var \PlayHPTests\DB\DBObjects\Profile[] $profiles */
        $profiles = $this->service->fetchAll('Profile', array('~profileCode' => 'U%'));
        $this->assertCount(1, $profiles);
        $this->assertEquals('USER', $profiles[0]->profileCode);
    }

    public function testHydrate()
    {
        $test = new TestObject();
        $test->id = 1;
        $this->service->hydrate($test);
        $this->assertEquals('test@playhp.org', $test->email);
    }

    public function testFetchAllFilterNotLike()
    {
        /* @var \PlayHPTests\DB\DBObjects\Profile[] $profiles */
        $profiles = $this->service->fetchAll('Profile', array('!~profileCode' => 'U%'));
        $this->assertCount(1, $profiles);
        $this->assertEquals('ADMIN', $profiles[0]->profileCode);
    }

    public function testFetchAllOrder()
    {
        /* @var \PlayHPTests\DB\DBObjects\Profile[] $profiles */
        $profiles = $this->service->fetchAll('Profile', array(), array('profileCode' => true));
        $this->assertCount(2, $profiles);
        $this->assertEquals('ADMIN', $profiles[0]->profileCode);
        $profiles = $this->service->fetchAll('Profile', array(), array('profileCode' => false));
        $this->assertEquals('USER', $profiles[0]->profileCode);
    }

    public function testFetchAll()
    {
        /* @var \PlayHPTests\DB\DBObjects\Profile[] $profiles */
        $profiles = $this->service->fetchAll('Profile');
        $this->assertCount(2, $profiles);
        $expectedProfileCodes = array(
            'USER' => true,
            'ADMIN' => true,
        );
        foreach ($profiles as $profile) {
            $this->assertEquals('PlayHPTests\DB\DBObjects\Profile', get_class($profile));
            $this->assertArrayHasKey($profile->profileCode, $expectedProfileCodes, '<pre>' . print_r($profile, true) . '</pre>');
        }
    }

    /**
     * @return string The DSN to use for this connection
     */
    protected function getDsn()
    {
        return 'sqlite:' . dirname(__FILE__) . '/dbTest.db3';
    }
}
<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHPTests\IOC;

use PlayHP\IOC\Exceptions\ObjectPoolException;
use PlayHP\Ioc\ObjectPool;
use PlayHP\PlayHP;
use PlayHPTests\IOC\TestObjects\OtherClass;
use PlayHPTests\IOC\TestObjects\SimpleClass;
use PlayHPTests\IOC\TestObjects\ThirdClass;

class IocTest extends \PHPUnit_Framework_TestCase
{

    public function testParsing()
    {
        $conf = simplexml_load_string('<pool>
        <object id="simple" class="PlayHPTests\IOC\TestObjects\SimpleClass">
            <property name="testValue" value="theValue" />
            <property name="other" ref="other" />
            <property name="inlineOther">
                <object class="PlayHPTests\IOC\TestObjects\OtherClass">
                    <property name="id" value="2" />
                </object>
            </property>
        </object>
        <object id="simple2" class="PlayHPTests\IOC\TestObjects\SimpleClass">
            <constructorArgs>
                <arg value="Simple value" />
            </constructorArgs>
        </object>
        <object id="other" class="PlayHPTests\IOC\TestObjects\OtherClass">
            <property name="id" value="1" />
        </object>
        <object id="dependent" class="PlayHPTests\IOC\TestObjects\DependentClass">
            <constructorArgs>
                <arg ref="simple2" />
                <arg ref="other" />
            </constructorArgs>
        </object>
    </pool>');
        $pool = new ObjectPool($conf);
        $pool->initializePool();
        $this->assertCount(5, $pool->pool);
    }

    public function testAutowiring()
    {
        $configValue = 'Config value';
        PlayHP::config('testKey', $configValue);

        $conf = simplexml_load_string('<pool autowire="true">
        <object id="simpleProp" class="PlayHPTests\IOC\TestObjects\SimpleClass" />
        <object id="simple" class="PlayHPTests\IOC\TestObjects\SimpleClass" />
        <object id="other" class="PlayHPTests\IOC\TestObjects\OtherClass">
            <property name="id" value="1" />
        </object>
        <object id="other2" class="PlayHPTests\IOC\TestObjects\OtherClass">
            <property name="id" value="2" />
        </object>
        <object id="annotated" class="PlayHPTests\IOC\TestObjects\AnnotatedClass" />
    </pool>');
        $pool = new ObjectPool($conf);
        $pool->initializePool();
        $simple = $pool->getObject('simple');
        $simpleProp = $pool->getObject('simpleProp');
        $other = $pool->getObject('other');
        $other2 = $pool->getObject('other2');
        $annotated = $pool->getObject('annotated');
        $this->assertNotNull($annotated);
        $this->assertEquals($other, $annotated->other);
        $this->assertEquals($simpleProp, $annotated->simple);
        $this->assertEquals($other, $annotated->otherConstructor);
        $this->assertEquals($simple, $annotated->simpleConstructor);
        $this->assertEquals($other, $annotated->byMethod);
        $this->assertEquals($other2, $annotated->byMethod2);
        $this->assertEquals($configValue, $annotated->config);
    }

    public function testXmlProperty()
    {
        $conf = simplexml_load_string('<pool>
        <object id="simple" class="PlayHPTests\IOC\TestObjects\SimpleClass">
            <property name="testValue" value="theValue" />
        </object>
    </pool>');
        $pool = new ObjectPool($conf);
        $pool->initializePool();
        $object = $pool->getObject('simple');
        $this->assertNotNull($object);
        $this->assertTrue(is_a($object, 'PlayHPTests\IOC\TestObjects\SimpleClass'));
        $this->assertEquals('theValue', $object->testValue);
    }

    public function testXmlConstructorArgValue()
    {
        $conf = simplexml_load_string('<pool>
        <object id="simple" class="PlayHPTests\IOC\TestObjects\SimpleClass">
            <constructorArgs>
                <arg value="Simple value" />
            </constructorArgs>
        </object>
    </pool>');
        $pool = new ObjectPool($conf);
        $pool->initializePool();
        $object = $pool->getObject('simple');
        $this->assertNotNull($object);
        $this->assertTrue(is_a($object, 'PlayHPTests\IOC\TestObjects\SimpleClass'));
        $this->assertEquals('Simple value', $object->testValue);
    }


    public function testXmlConstructorArgRef()
    {
        $conf = simplexml_load_string('<pool>
        <object id="simple" class="PlayHPTests\IOC\TestObjects\SimpleClass">
            <constructorArgs>
                <arg ref="other" />
            </constructorArgs>
        </object>
        <object id="other" class="PlayHPTests\IOC\TestObjects\OtherClass" />
    </pool>');
        $pool = new ObjectPool($conf);
        $pool->initializePool();
        $object = $pool->getObject('simple');
        $this->assertNotNull($object);
        $this->assertTrue(is_a($object, 'PlayHPTests\IOC\TestObjects\SimpleClass'));
        $this->assertNotNull($object->testValue);
        $this->assertTrue(is_a($object->testValue, 'PlayHPTests\IOC\TestObjects\OtherClass'));
    }


    public function testXmlConstructorArgByPosition()
    {
        $conf = simplexml_load_string('<pool>
        <object id="simple" class="PlayHPTests\IOC\TestObjects\DependentClass">
            <constructorArgs>
                <arg pos="1" value="Argument 2" />
                <arg pos="0" value="Argument 1" />
            </constructorArgs>
        </object>
    </pool>');
        $pool = new ObjectPool($conf);
        $pool->initializePool();
        $object = $pool->getObject('simple');
        $this->assertNotNull($object);
        $this->assertTrue(is_a($object, 'PlayHPTests\IOC\TestObjects\DependentClass'));
        $this->assertEquals('Argument 1', $object->getSimple());
        $this->assertEquals('Argument 2', $object->getOther());
    }

    public function testXmlConstructorArgByName()
    {
        $conf = simplexml_load_string('<pool>
        <object id="simple" class="PlayHPTests\IOC\TestObjects\DependentClass">
            <constructorArgs>
                <arg name="simple" value="Argument 2" />
                <arg name="other" value="Argument 1" />
            </constructorArgs>
        </object>
    </pool>');
        $pool = new ObjectPool($conf);
        $pool->initializePool();
        $object = $pool->getObject('simple');
        $this->assertNotNull($object);
        $this->assertTrue(is_a($object, 'PlayHPTests\IOC\TestObjects\DependentClass'));
        $this->assertEquals('Argument 2', $object->getSimple());
        $this->assertEquals('Argument 1', $object->getOther());
    }

    public function testXmlRefProperty()
    {
        $conf = simplexml_load_string('<pool>
        <object id="simple" class="PlayHPTests\IOC\TestObjects\SimpleClass">
            <property name="other" ref="other" />
        </object>
        <object id="other" class="PlayHPTests\IOC\TestObjects\OtherClass" />
    </pool>');
        $pool = new ObjectPool($conf);
        $pool->initializePool();
        $object = $pool->getObject('simple');
        $this->assertNotNull($object);
        $this->assertTrue(is_a($object, 'PlayHPTests\IOC\TestObjects\SimpleClass'));
        $this->assertNotNull($object->other);
        $this->assertTrue(is_a($object->other, 'PlayHPTests\IOC\TestObjects\OtherClass'));

    }


    public function testCyclicDependency()
    {
        $conf = simplexml_load_string('<pool>
        <object id="simple" class="PlayHPTests\IOC\TestObjects\SimpleClass">
            <constructorArgs>
                <arg ref="dependent" />
            </constructorArgs>
        </object>
        <object id="dependent" class="PlayHPTests\IOC\TestObjects\DependentClass">
            <constructorArgs>
                <arg ref="simple" />
            </constructorArgs>
        </object>
    </pool>');
        $pool = new ObjectPool($conf);
        try {
            $pool->initializePool();
        } catch (ObjectPoolException $e) {
            return;
        }
        $this->fail('There should be a cyclic dependency issue in the configuration');
    }

    public function testMissingDependency()
    {
        $conf = simplexml_load_string('<pool autowire="true">
        <object id="annotated" class="PlayHPTests\IOC\TestObjects\AnnotatedPostInit" />
    </pool>');
        $pool = new ObjectPool($conf);
        $pool->initializePool();
        $annotated = $pool->getObject('annotated');
        $this->assertNull($annotated->byMethod);
        $this->assertNull($annotated->third);
        $this->assertNull($annotated->simple);

        $pool->attach('other', new OtherClass());
        $this->assertNotNull($annotated->byMethod);

        $pool->attach('dummy', new SimpleClass());
        $this->assertNull($annotated->simple);

        $pool->attach('simple', new SimpleClass());
        $this->assertNotNull($annotated->simple);

        $pool->attach('last', new ThirdClass());
        $this->assertNotNull($annotated->third);
    }

}
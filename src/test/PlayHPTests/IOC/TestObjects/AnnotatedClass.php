<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace PlayHPTests\IOC\TestObjects;

class AnnotatedClass
{

    /**
     * @Inject
     * @var OtherClass
     */
    public $other;

    /**
     * @Inject(simpleProp)
     * @var SimpleClass
     */
    public $simple;

    /**
     * @var OtherClass
     */
    public $byMethod;

    /**
     * @var OtherClass
     */
    public $byMethod2;

    /**
     * @Config(testKey)
     * @var string
     */
    public $config;

    /**
     * @var OtherClass
     */
    public $otherConstructor;

    /**
     * @var SimpleClass
     */
    public $simpleConstructor;

    /**
     * @param OtherClass $test @Inject
     */
    public function injectInMethod($test)
    {
        $this->byMethod = $test;
    }

    /**
     * @Inject(other2)
     * @param OtherClass $test
     */
    public function otherInjectType($test)
    {
        $this->byMethod2 = $test;
    }

    /**
     * Injection in constructor
     *
     * @param OtherClass $other @Inject
     * @param SimpleClass $simple @Inject(simple)
     */
    function __construct($other, $simple)
    {
        $this->otherConstructor = $other;
        $this->simpleConstructor = $simple;
    }


}
<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace PlayHPTests\IOC\TestObjects;

class AnnotatedPostInit
{

    /**
     * @Inject
     * @var ThirdClass
     */
    public $third;

    /**
     * @var OtherClass
     */
    public $byMethod;

    /**
     * @Inject(simple)
     * @var SimpleClass
     */
    public $simple;

    /**
     * @param OtherClass $test @Inject
     */
    public function injectInMethod($test)
    {
        $this->byMethod = $test;
    }

    /**
     * Injection in constructor
     */
    function __construct()
    {
    }


}
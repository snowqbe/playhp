<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHPTests\IOC\TestObjects;

class DependentClass
{

    private $_simple;

    private $_other;

    /**
     * @param SimpleClass $simple
     * @param OtherClass $other
     */
    function __construct($simple, $other)
    {
        $this->_simple = $simple;
        $this->_other = $other;
    }

    public function getSimple()
    {
        return $this->_simple;
    }

    public function getOther()
    {
        return $this->_other;
    }
}
<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace PlayHPTests\IOC\TestObjects;

class SimpleClass
{


    /**
     * @var string
     */
    public $testValue;

    /**
     * @var OtherClass
     */
    public $other;

    /**
     * @var OtherClass
     */
    public $inlineOther;

    /**
     * @param null $dummy Only ued for testing cyclic dependency resolutions
     */
    function __construct($dummy = null)
    {
        $this->testValue = $dummy;
    }


}
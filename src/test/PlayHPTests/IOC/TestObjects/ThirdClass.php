<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace PlayHPTests\IOC\TestObjects;

class ThirdClass
{

    /**
     * @var string ID
     */
    public $id;

}
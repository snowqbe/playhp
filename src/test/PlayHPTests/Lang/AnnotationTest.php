<?php

namespace PlayHPTests\Lang;

use PlayHP\Lang\Annotation;

/**
 * Test class for annotations
 */
class AnnotationTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Tests a simple annotation
     */
    public function testAnnotationName()
    {
        $comment = '
        /**
         * @Test
         */
        ';
        $annotations = Annotation::parseAnnotations($comment);

        $this->assertCount(1, $annotations, 'No annotation detected');
        $this->assertEquals("Test", $annotations[0]->name);
    }

    /**
     * Tests the hasParams method
     */
    public function testAnnotationHasParams()
    {
        $comment = '
        /**
         * @Test
         */
        ';
        $annotations = Annotation::parseAnnotations($comment);
        $this->assertCount(1, $annotations, 'No annotation detected');
        $this->assertFalse($annotations[0]->hasParams());
    }

    /**
     * Test simple annotation with a single parameter
     */
    public function testParam()
    {
        $comment = '
        /**
         * @Test(id=test)
         */
        ';
        $annotations = Annotation::parseAnnotations($comment);
        $this->assertCount(1, $annotations, 'No annotation detected');
        $this->assertArrayHasKey("id", $annotations[0]->params);
        $this->assertEquals("test", $annotations[0]->params['id']);
    }

    /**
     * Test simple annotation with a single default valued parameter
     */
    public function testParamDefault()
    {
        $comment = '
        /**
         * @Test(test)
         */';

        $annotations = Annotation::parseAnnotations($comment);
        $this->assertCount(1, $annotations, 'No annotation detected');
        $this->assertArrayHasKey("test", $annotations[0]->params);
        $this->assertEquals("test", $annotations[0]->params['test']);
    }

    /**
     * Test simple annotation with a single default valued parameter
     */
    public function testParamDefaultNoParentheses()
    {
        $comment = '
        /**
         * @Test test
         */';

        $annotations = Annotation::parseAnnotations($comment);
        $this->assertCount(1, $annotations, 'No annotation detected');
        $this->assertCount(1, $annotations[0]->params);
        $this->assertEquals("test", $annotations[0]->params[0]);
    }

    /**
     * Test simple annotation with a single default valued parameter
     */
    public function testParamGetDefault()
    {
        $comment = '
        /**
         * @Test(test)
         */';

        $annotations = Annotation::parseAnnotations($comment);
        $this->assertCount(1, $annotations, 'No annotation detected');
        $this->assertEquals("test", $annotations[0]->getDefaultParam());
    }

    /**
     * Test simple annotation without param, and testing the fallback mecanism
     */
    public function testParamGetDefaultFallback()
    {
        $comment = '
        /**
         * @Test
         */';

        $annotations = Annotation::parseAnnotations($comment);
        $this->assertCount(1, $annotations, 'No annotation detected');
        $this->assertEquals('absent', $annotations[0]->getDefaultParam('name', 'absent'));
    }

    /**
     * Test quick annotation access
     */
    public function testGetAnnotation()
    {
        $comment = '
        /**
         * @Test(test="value",other=false)
         */';

        $annotation = Annotation::getAnnotation($comment, 'Test');
        $this->assertNotNull($annotation);
        $this->assertTrue(is_a($annotation, 'PlayHP\Lang\Annotation'));
        $this->assertEquals('Test', $annotation->name);
    }

    /**
     * Test quick annotation param access
     */
    public function testGetAnnotationParam()
    {
        $comment = '
        /**
         * @Test(test="value",other=false)
         */';

        $value = Annotation::getAnnotationDefaultParam($comment, 'Test', 'test');
        $this->assertEquals('value', $value);
    }

    /**
     * Test simple annotation with an optional parameter which has a value defined
     */
    public function testParamGetOptionalWithValue()
    {
        $comment = '
        /**
         * @Test(test)
         */';

        $annotations = Annotation::parseAnnotations($comment);
        $this->assertCount(1, $annotations, 'No annotation detected');
        $this->assertEquals("test", $annotations[0]->getOptionalParam('test', 'default'));
    }

    /**
     * Test simple annotation with an optional parameter which has NOT a value defined
     */
    public function testParamGetOptionalWithoutValue()
    {
        $comment = '
        /**
         * @Test(test)
         */';

        $annotations = Annotation::parseAnnotations($comment);
        $this->assertCount(1, $annotations, 'No annotation detected');
        $this->assertEquals("default", $annotations[0]->getOptionalParam('undefined', 'default'));
    }

    /**
     * Test simple annotation has two annotations
     */
    public function testAnnotationMultipleParams()
    {
        $comment = '
        /**
         * @Test(id=test, required=true)
         */';

        $annotations = Annotation::parseAnnotations($comment);
        $this->assertCount(1, $annotations, 'No annotation detected');
        $this->assertCount(2, $annotations[0]->params);
        $this->assertArrayHasKey("id", $annotations[0]->params);
        $this->assertArrayHasKey("required", $annotations[0]->params);
        $this->assertEquals("test", $annotations[0]->params['id']);
        $this->assertEquals("true", $annotations[0]->params['required']);
    }

    /**
     * Test simple annotation with a single default valued parameter
     */
    public function testDefaultValue()
    {
        $comment = '
        /**
         * @Test(test)
         */';
        $annotations = Annotation::parseAnnotations($comment);
        $this->assertCount(1, $annotations, 'No annotation detected');
        $this->assertArrayHasKey("test", $annotations[0]->params);
        $this->assertEquals("test", $annotations[0]->params['test']);
    }

    /**
     * Test simple annotation with a single default valued parameter
     */
    public function testMultipleParameters()
    {
        $comment = '
        /**
         * @Test(id=test, required=true)
         */
        ';
        $annotations = Annotation::parseAnnotations($comment);
        $this->assertCount(1, $annotations, 'No annotation detected');
        $this->assertCount(2, $annotations[0]->params, 'Annotation should have 2 parameters');
        $this->assertArrayHasKey("id", $annotations[0]->params, 'First parameter has not been parsed');
        $this->assertArrayHasKey("required", $annotations[0]->params, 'Second parameter has not been parsed');
        reset($annotations[0]->params);
        $this->assertEquals('id', key($annotations[0]->params), 'Parameters are not put in the correct order');
    }

    /**
     * Tests a double annotation
     */
    public function testMultipleAnnotations()
    {
        $comment = '
        /**
         * @Controller
         * @Test
         */
        ';
        $annotations = Annotation::parseAnnotations($comment);

        $this->assertCount(2, $annotations);
        $this->assertEquals("Controller", $annotations[0]->name);
        $this->assertEquals("Test", $annotations[1]->name);
        $this->assertFalse($annotations[0]->hasParams());
        $this->assertFalse($annotations[1]->hasParams());
    }

    /**
     * Tests multiple, complex annotations
     */
    public function testMultipleComplexAnnotations()
    {
        $comment = '
        /**
         * Introducing this complex comment
         * @Controller test
         * @Test
         * This is ignored
         * @var test
         * Hello
         */
        ';
        $annotations = Annotation::parseAnnotations($comment);

        $this->assertCount(3, $annotations);
        $this->assertEquals("Controller", $annotations[0]->name);
        $this->assertEquals("Test", $annotations[1]->name);
        $this->assertEquals("var", $annotations[2]->name);
    }

    /**
     * Tests multiple, complex annotations
     */
    public function testMultipleComplexAnnotationsFiltered()
    {
        $comment = '
        /**
         * Introducing this complex comment
         * @Controller test
         * @Test
         * This is ignored
         * @var test
         * Hello
         */
        ';
        $annotations = Annotation::parseAnnotations($comment, array('Test'));

        $this->assertCount(1, $annotations);
        $this->assertEquals("Test", $annotations[0]->name);
    }

    /**
     * Tests a var annotation with comment
     */
    public function testAnnotationWithComment()
    {
        $comment = '
            /**
             * @var string $test Comment on the variable @Field(name=test, required)
             */
            ';
        $annotations = Annotation::parseAnnotations($comment);
        $this->assertCount(1, $annotations, 'No annotation detected');
        $annot = $annotations[0];
        $this->assertEquals('var', $annot->name, 'Annotation name should be "var"');
        $this->assertCount(3, $annot->params, 'Annotation should have 2 parameters');
        $this->assertEquals('Comment on the variable', $annot->params[2], 'Comment has not been correctly parsed');
    }

    /**
     * Tests a param annotation
     */
    public function testNativeAnnotation()
    {
        $comment = '
        /**
         * @param string $test
         */
        ';
        $annotations = Annotation::parseAnnotations($comment);

        $this->assertCount(1, $annotations, 'No annotation detected');
        $this->assertEquals("param", $annotations[0]->name);
        $this->assertEmpty($annotations[0]->subAnnotations);
        $this->assertCount(2, $annotations[0]->params);
    }

    /**
     * Tests a param annotation with subannotation
     */
    public function testNativeAnnotationWithSubannotation()
    {
        $comment = '
            /**
             * @param string $test @Field(type=email)
             */
            ';

        $annotations = Annotation::parseAnnotations($comment);

        $this->assertCount(1, $annotations, 'No annotation detected');
        $this->assertCount(2, $annotations[0]->params);
        $this->assertEquals("string", $annotations[0]->params[0]);
        $this->assertEquals("\$test", $annotations[0]->params[1]);
    }

    /**
     * Tests a param annotation with subannotation
     */
    public function testSubAnnotation()
    {
        $comment = '
            /**
             * @param string $test @Field(type=email)
             */
            ';

        $annotations = Annotation::parseAnnotations($comment);

        $this->assertCount(1, $annotations, 'No annotation detected');
        $this->assertCount(1, $annotations[0]->subAnnotations);
        $this->assertEquals("Field", $annotations[0]->subAnnotations[0]->name);
        $this->assertCount(1, $annotations[0]->subAnnotations[0]->params);
        $this->assertArrayHasKey("type", $annotations[0]->subAnnotations[0]->params);
        $this->assertEquals("email", $annotations[0]->subAnnotations[0]->params['type']);
    }

    /**
     * Tests a subannotation with comment
     */
    public function testSubAnnotationWithComment()
    {
        $comment = '
            /**
             * @param string $test Testing with a comment @Field(type=email)
             */
            ';

        $annotations = Annotation::parseAnnotations($comment);

        $this->assertCount(1, $annotations, 'No annotation detected');
        $this->assertCount(1, $annotations[0]->subAnnotations, 'Annotation should have one subannotation');
        $this->assertEquals("Field", $annotations[0]->subAnnotations[0]->name);
        $this->assertCount(1, $annotations[0]->subAnnotations[0]->params);
        $this->assertArrayHasKey("type", $annotations[0]->subAnnotations[0]->params);
        $this->assertEquals("email", $annotations[0]->subAnnotations[0]->params['type']);
    }

    /**
     * Tests a multiple subannotations
     */
    public function testMultipleSubAnnotations()
    {
        $comment = '
            /**
             * @param string $test Testing with a comment @Required @Field(type=email)
             */
            ';
        $annotations = Annotation::parseAnnotations($comment);
        $this->assertCount(1, $annotations, 'No annotation detected');
        $this->assertCount(2, $annotations[0]->subAnnotations, 'Annotation should have two subannotations: Required and Field');
        $this->assertEquals('Required', $annotations[0]->subAnnotations[0]->name);
        $fieldSub = $annotations[0]->subAnnotations[1];
        $this->assertEquals('Field', $fieldSub->name);
        $this->assertCount(1, $fieldSub->params);
        $this->assertArrayHasKey('type', $fieldSub->params);
        $this->assertEquals('email', $fieldSub->params['type']);
    }

}

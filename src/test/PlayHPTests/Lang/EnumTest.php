<?php

namespace PlayHPTests\Lang;

use PlayHP\Lang\Enum;

/**
 * Test enumerations
 */
class EnumTest extends \PHPUnit_Framework_TestCase
{
    public function testEnum()
    {
        $this->assertNotNull(TestEnum::$TEST1);
        $this->assertEquals('TEST1', TestEnum::$TEST1->value);
        $this->assertNotNull(TestEnum::$TEST2);
    }

    public function testParseEnum()
    {
        $test1Enum = TestEnum::parse('TEST1');
        $this->assertNotNull($test1Enum);
        $this->assertEquals($test1Enum, TestEnum::$TEST1);
        $this->assertNotEquals($test1Enum, TestEnum::$TEST2);
    }
}

/**
 * Enumeration for testing purposes
 */
class TestEnum extends Enum
{
    public static $TEST1;
    public static $TEST2;
}

TestEnum::init();

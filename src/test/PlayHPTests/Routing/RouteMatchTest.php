<?php

namespace PlayHPTests\Routing;
use PlayHP\PlayHP;
use PlayHP\Routing\HttpMethod;
use PlayHP\Routing\Router;


/**
 * Route matching tests
 */
class RouteMatchTest extends RouterTest
{

    protected function setUp()
    {
        parent::setUp();
        $this->classLoader->init($this->compilationPaths);
        $this->router = new Router();
        $this->router->init($this->config);
        //$this->router->addRoutes(PlayHP::DEV_ROUTES());
        $this->router->parseRoutes();
    }

    public function testControllerRoute()
    {
        $route = $this->router->getRoute();
        $this->assertNotNull($route);
        $this->assertEquals('/', $route->path);

        $routeTarget = $route->getTarget();
        $call = $routeTarget->getDefinitiveCall();
        $this->assertEquals('PlayHPTests\Controllers\Dummies\TestControllerA', $call->getControllerClassName());
        $this->assertEquals('testA', $call->getControllerMethodName());
    }

    public function testRouteMatch()
    {
        $route = $this->router->getRoute('/test');
        $this->assertNotNull($route);
        $this->assertEquals('/test', $route->path);

        $routeTarget = $route->getTarget();
        $call = $routeTarget->getDefinitiveCall();
        $this->assertEquals('PlayHPTests\Controllers\Dummies\TestControllerB', $call->getControllerClassName());
        $this->assertEquals('testB', $call->getControllerMethodName());
    }

    public function testInvalidHttpMatch()
    {
        $route = $this->router->getRoute('/never_reached');
        $this->assertNull($route);
    }

    public function testHttpMethodMatch()
    {
        $route = $this->router->getRoute('/');
        $this->assertNotNull($route);
        $this->assertEquals(HttpMethod::$GET, $route->getHttpMethod());
    }

    public function testDynamicParam()
    {
        $route = $this->router->getRoute('/index/test', 'PUT');
        $this->assertNotNull($route);

        $routeParams = $route->getPathParams();
        $targetControllerClass = new \ReflectionClass('PlayHPTests\Controllers\Dummies\TestControllerC');
        $controllerMethod = $targetControllerClass->getMethod('testC');
        $methodParams = $controllerMethod->getParameters();

        $routeTarget = $route->getTarget();
        $call = $routeTarget->getDefinitiveCall();
        $this->assertEquals($targetControllerClass->getName(), $call->getControllerClassName());
        $this->assertEquals($controllerMethod->getName(), $call->getControllerMethodName());
        $this->assertEquals($methodParams[0]->getName(), $routeParams[0]);

    }

    public function testDynamicRouteQualified()
    {
        $route = $this->router->getRoute('/PlayHPTests\Controllers\Dummies\TestControllerA/testA');
        $this->assertNotNull($route);
        $routeTarget = $route->getTarget();
        $call = $routeTarget->getDefinitiveCall();
        $this->assertEquals('PlayHPTests\Controllers\Dummies\TestControllerA', $call->getControllerClassName());
        $this->assertEquals('testA', $call->getControllerMethodName());
    }

    public function testDynamicRoute()
    {
        $route = $this->router->getRoute('/TestControllerA/testA');
        $this->assertNotNull($route);
        $routeTarget = $route->getTarget();
        $call = $routeTarget->getDefinitiveCall();
        $this->assertEquals('PlayHPTests\Controllers\Dummies\TestControllerA', $call->getControllerClassName());
        $this->assertEquals('testA', $call->getControllerMethodName());
    }


    public function testMatchMethodParams()
    {
        $route = $this->router->getRoute('/methodParams?param1=value&param2=2&param3[0]=test1&param3[1]=test2', 'GET');
        $this->assertNotNull($route);
        $routeTarget = $route->getTarget();
        $call = $routeTarget->getDefinitiveCall();
        $this->assertEquals('PlayHPTests\Controllers\Dummies\TestControllerA', $call->getControllerClassName());
        $this->assertEquals('testParams', $call->getControllerMethodName());
        $this->assertCount(3, $call->methodParams);
        $this->assertArrayHasKey('param1', $call->methodParams);
        $this->assertArrayHasKey('param2', $call->methodParams);
        $this->assertArrayHasKey('param3', $call->methodParams);
        $this->assertEquals('value', $call->methodParams['param1']);
        $this->assertEquals(2, $call->methodParams['param2']);
        $this->assertCount(2, $call->methodParams['param3']);
        $this->assertEquals('test1', $call->methodParams['param3'][0]);
        $this->assertEquals('test2', $call->methodParams['param3'][1]);
    }

    public function testRoutePriority()
    {
        $route = $this->router->getRoute('/index/ok');
        $this->assertNotNull($route);
        $routeTarget = $route->getTarget();
        $call = $routeTarget->getDefinitiveCall();
        $this->assertEquals('PlayHPTests\Controllers\Dummies\TestControllerC', $call->getControllerClassName());
        $this->assertEquals('testC', $call->getControllerMethodName());
    }

}

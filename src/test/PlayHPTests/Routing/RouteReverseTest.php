<?php

namespace PlayHPTests\Routing;

use PlayHP\PlayHP;
use PlayHP\Routing\Router;

/**
 * Reverse URL composition tests
 */
class RouteReverseTest extends RouterTest
{

    protected function setUp()
    {
        parent::setUp();
        $this->classLoader->init($this->compilationPaths);
        $this->router = new Router();
        $this->router->init($this->config);
        $this->router->addRoutes(PlayHP::devRoutes());
        $this->router->parseRoutes();
    }

    public function testReverseSimple()
    {
        $url = $this->router->reverse('PlayHPTests\Controllers\Dummies\TestControllerB', 'testB');
        $this->assertEquals('/test', $url, 'Found URL: ' . $url);
    }

    public function testReverseDynamicQualified()
    {
        $url = $this->router->reverse('PlayHPTests\Controllers\Dummies\TestControllerD', 'testD');
        $this->assertEquals('/PlayHPTests\Controllers\Dummies\TestControllerD/testD', $url, 'Found URL: ' . $url);
    }

    public function testReverseDynamic()
    {
        $url = $this->router->reverse('TestControllerD', 'testD');
        $this->assertEquals('/TestControllerD/testD', $url, 'Found URL: ' . $url);
    }

    public function testReverseParam()
    {
        $url = $this->router->reverse('PlayHPTests\Controllers\Dummies\TestControllerC', 'testC', array(
            'param' => 'value'
        ));
        $this->assertEquals('/index/value', $url, 'Found URL: ' . $url);
    }

    public function testReverseMethodParams()
    {
        $url = $this->router->reverse('PlayHPTests\Controllers\Dummies\TestControllerA', 'testParams', array(
            'param1' => 'value',
            'param2' => 2,
            'param3' => array('test1', 'test2'),
        ));
        $this->assertEquals('/methodParams?param1=value&param2=2&param3[0]=test1&param3[1]=test2', $url, 'Found URL: ' . $url);
    }

    public function testReverseInvalid()
    {
        $url = $this->router->reverse('inexistent', 'neither');
        $this->assertNull($url);
    }
}

<?php


namespace PlayHPTests\Routing;

use PlayHP\Lang\ClassLoader;
use PlayHP\PlayHP;
use PlayHP\Routing\Router;

/**
 * Compiler test class
 */
class RouterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var array List of paths to compile
     * @see Router#compile
     */
    protected $compilationPaths;

    /**
     * PlayHP Router
     * @var Router
     */
    protected $router;

    /**
     * ClassLoader
     * @var ClassLoader
     */
    protected $classLoader;

    /**
     * Routes configuration for testing
     * @var \SimpleXMLElement
     */
    protected $config;

    protected function setUp()
    {
        parent::setUp();
        $this->config = new \SimpleXMLElement('<routes>
                <!-- Invalid configuration -->
                <route path="/" method="GET" target="invalid" />
                <route path="/" method="GET" />
                <route path="/never_reached" method="invalid" target="PlayHPTests\Controllers\Dummies\TestControllerA.testA" />
                <route method="GET" target="PlayHPTests\Controllers\Dummies\TestControllerA.testA" />
                <route path="invalid" method="GET" target="PlayHPTests\Controllers\Dummies\TestControllerA.testA" />

                <!-- Valid configuration -->
                <route path="/" method="GET" target="PlayHPTests\Controllers\Dummies\TestControllerA.testA" />
                <route path="/other" target="PlayHPTests\Controllers\Dummies\TestControllerA.testA" />
                <route path="/test" method="POST" target="PlayHPTests\Controllers\Dummies\TestControllerB.testB" />
                <route path="/index/{param}" method="PUT" target="PlayHPTests\Controllers\Dummies\TestControllerC.testC" />
                <route path="/index/ok" method="GET" target="PlayHPTests\Controllers\Dummies\TestControllerC.testOK" />
                <route path="/methodParams" method="GET" target="PlayHPTests\Controllers\Dummies\TestControllerA.testParams" />
                <route path="/{controller}/{method}" method="*" target="{controller}.{method}" />
            </routes>');

        $this->classLoader = PlayHP::classLoder();
        $this->compilationPaths = array(
            P_SRC_PATH,
            P_TEST_PATH,
        );
    }

    public function testCompilation()
    {
        $this->classLoader->init($this->compilationPaths);
        $this->assertNotNull($this->classLoader->getReflectionClass('RouterTest'));
        try {
            $this->classLoader->getReflectionClass('NotAController');
            $this->classLoader->getReflectionClass('TestControllerA');
        } catch (\Exception $exc) {
            $this->fail('Router should know class NotAController and TestControllerA');
        }
    }

    public function testRouteParsing()
    {
        $this->classLoader->init($this->compilationPaths);
        $this->router = new Router();
        $this->router->init($this->config);
        $this->router->parseRoutes();
        $this->assertCount(7, $this->router->getRoutes(), 'A problem occured while parsing routes');
    }
}

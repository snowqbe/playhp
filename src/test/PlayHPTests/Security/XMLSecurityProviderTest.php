<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PlayHPTests\Security;


use PlayHP\Exceptions\MissingFileException;
use PlayHP\Security\Crypto\Crypto;
use PlayHP\Security\Model\User;
use PlayHP\Security\Providers\InvalidConfigurationException;
use PlayHP\Security\Providers\UserExistsException;
use PlayHP\Security\Providers\XMLSecurityProvider;

class XMLSecurityProviderTest extends \PHPUnit_Framework_TestCase
{
    public $repoConfigPath;

    /**
     * @var XMLSecurityProvider
     */
    private $xmlProvider;

    protected function setUp()
    {
        $this->repoConfigPath = dirname(__FILE__) . '/users.xml';
    }

    public function testParseConfiguration()
    {
        try {
            $this->xmlProvider = new XMLSecurityProvider($this->repoConfigPath);
            $this->xmlProvider->initRepository();
            $this->assertCount(2, $this->xmlProvider->getAllUsers());
        } catch (MissingFileException $e) {
            $this->fail('Configuration not found');
        }
        catch (InvalidConfigurationException $e) {
            $this->fail('Invalid configuration');
        }
        catch (\Exception $e) {
            $this->fail('Unknown error while parsing configuration');
        }
    }

    public function testGetUserByLoginPassword()
    {
        $this->xmlProvider = new XMLSecurityProvider(dirname(__FILE__) . '/users.xml');
        $this->xmlProvider->initRepository();
        $user1 = $this->xmlProvider->getUserByLoginPassword('test', 'test');
        $this->assertNotNull($user1);
        $user2 = $this->xmlProvider->getUserByLoginPassword('test', 'false');
        $this->assertNull($user2);
    }

    public function testGetUserByLoginPasswordMD5()
    {
        $this->xmlProvider = new XMLSecurityProvider(dirname(__FILE__) . '/users_md5.xml');
        $this->xmlProvider->crypto = new Crypto('', 'md5');
        $this->xmlProvider->initRepository();
        $user1 = $this->xmlProvider->getUserByLoginPassword('test', 'test');
        $this->assertNotNull($user1);
    }

    public function testGetUserByLoginPasswordSHA1()
    {
        $this->xmlProvider = new XMLSecurityProvider(dirname(__FILE__) . '/users_sha1.xml');
        $this->xmlProvider->crypto = new Crypto('', 'sha1');
        $this->xmlProvider->initRepository();
        $user1 = $this->xmlProvider->getUserByLoginPassword('test', 'test');
        $this->assertNotNull($user1);
    }

    public function testGetUserByLogin()
    {
        $this->xmlProvider = new XMLSecurityProvider(dirname(__FILE__) . '/users.xml');
        $this->xmlProvider->initRepository();
        $user1 = $this->xmlProvider->getUserByLogin('test');
        $this->assertNotNull($user1);
        $this->assertCount(2, $user1->profiles);
    }

    public function testHasCredential()
    {
        $this->xmlProvider = new XMLSecurityProvider(dirname(__FILE__) . '/users.xml');
        $this->xmlProvider->initRepository();
        $user1 = $this->xmlProvider->getUserByLogin('test');
        $this->assertNotNull($user1);
        $this->assertTrue($this->xmlProvider->hasCredential('user', $user1));
        $this->assertTrue($this->xmlProvider->hasCredential('admin', $user1));
        $this->assertFalse($this->xmlProvider->hasCredential('nope', $user1));
    }

    public function testRegisterExistingLogin()
    {
        $this->xmlProvider = new XMLSecurityProvider(dirname(__FILE__) . '/users.xml');
        $this->xmlProvider->initRepository();
        $user = new User();
        $user->login = 'test';
        $user->password = 'test';
        $user->profiles = array('user', 'admin');
        try {
            $this->xmlProvider->register($user);
        } catch (UserExistsException $e) {
            return;
        }
        $this->fail('User registration should have been rejected');
    }

    public function testRegister()
    {
        $this->xmlProvider = new XMLSecurityProvider(dirname(__FILE__) . '/users.xml');
        $this->xmlProvider->initRepository();
        $user = new User();
        $user->login = 'new';
        $user->password = 'test';
        $user->profiles = array('user', 'admin');
        try {
            $this->xmlProvider->register($user);
        } catch (UserExistsException $e) {
            $this->fail('User registration should have gone well');
        }
    }
}
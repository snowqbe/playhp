<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

define('P_MODE', 'playhp_test');
date_default_timezone_set('Europe/Paris');

require __DIR__ . "/vendor/autoload.php";

\PlayHP\PlayHP::TEST();
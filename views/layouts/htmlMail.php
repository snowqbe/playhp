<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * @var \PlayHP\Controllers\Rendering\View $this
 */

?>
<html>
<head>
    <title><?= $title ?></title>
    <link rel="stylesheet" type="text/css" href="static/css/mail.css"/>
</head>
<body>
<div>
    <img src="static/img/playhp.png" width="196" height="72"/>
</div>
<div>
    <?php $this->renderBody() ?>
</div>
</body>
</html>
<?php
/**
 * Main layout
 * @var \PlayHP\Controllers\Rendering\View $this
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>PlayHP - Welcome</title>
    <?php $this->renderHeader() ?>

    <!-- Le styles -->
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
    <style type="text/css">
        body {
            padding-top: 60px;
            padding-bottom: 40px;
        }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" type="image/png" href="<?= STATIC_BASE ?>img/favicon.png">
</head>
<body>
<?php $this->renderBody() ?>
</body>
</html>
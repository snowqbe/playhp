<?php
/**
 * @var \PlayHP\Controllers\Rendering\View $this
 */

$this->useLayout('authentication');

?>
<div class="container">
    <div class="row well">
        <h1><?= _p('Change your password') ?></h1>

        <?php $this->renderPart('feedback') ?>
        <form action="<?= $this->href('/@changepwd') ?>" method="post">
            <div class="control-group">
                <label class="control-label" for="oldpassword"><?= _p('Your password') ?></label>
                <input id="oldpassword" class="input-large" name="password" type="password"
                       value=""/>
            </div>
            <div class="control-group">
                <label class="control-label" for="password"><?= _p('New password') ?></label>
                <input id="password" class="input-large" name="newPassword" type="password"
                       value=""/>
            </div>
            <div class="control-group">
                <label class="control-label" for="passwordconfirm"><?= _p('Password confirmation') ?></label>
                <input id="passwordconfirm" class="input-large" name="passwordConfirm" type="password"
                       value=""/>
            </div>
            <div class="control-group">
                <input class="input-large btn btn-primary" name="submit" type="submit" value="<?= _p('Change') ?>"/>
            </div>
            <div class="control-group">
                <a href="<?= $this->href('/') ?>" class="btn btn-link"><?= _p('Back home') ?></a>
            </div>
        </form>
    </div>
</div>

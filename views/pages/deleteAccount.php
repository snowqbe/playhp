<div class="container" style="max-width: 500px; margin: 0 auto 20px; text-align: center">
    <?php
    /**
     * verifyEmail page
     *
     * @var \PlayHP\Controllers\Rendering\View $this
     */

    $this->useLayout('authentication');
    if (isset($success)) {
        $this->metaRedirect(5, HTTP_BASE);
        $this->renderPart('feedback');
    } else {
        ?>
        <form action="<?= $this->href('/@delaccount') ?>" method="post">
            <p class="hero-unit"><?= _p('Are you sure you want to delete your account? This can\'t be undone!') ?></p>

            <p>
                <input class="btn btn-danger btn-large" type="submit" name="confirm" value="<?= _p('Yes') ?>">
                <input class="btn btn-large" type="submit" name="cancel" value="<?= _p('No') ?>">
            </p>
        </form>
    <?php } ?>
    <p><a href="<?= HTTP_BASE ?>"><?= _p('Back to home page') ?></a></p>
</div>

<?php
/**
 * @var \PlayHP\Controllers\Rendering\View $this
 */

$this->useLayout('authentication');

?>
<div class="container">
    <div class="row well">
        <h1><?= _p('Reset your password') ?></h1>

        <?php $this->renderPart('feedback') ?>
        <form action="<?= $this->href('/@resetpwd') ?>" method="post">
            <div class="control-group">
                <label class="control-label" for="login"><?= _p('Your login') ?></label>
                <input id="login" class="input-large" name="login" type="text"
                       value="<?= filter_var($login, FILTER_SANITIZE_SPECIAL_CHARS) ?>"/>
            </div>
            <div class="control-group">
                <input class="input-large btn btn-primary" name="submit" type="submit" value="<?= _p('Reset') ?>"/>
            </div>
            <div class="control-group">
                <a href="<?= $this->href('/@login') ?>" class="btn btn-link"><?= _p('Back to login') ?></a>
            </div>
        </form>
    </div>
</div>

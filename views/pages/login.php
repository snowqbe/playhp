<?php
/**
 * @var \PlayHP\Controllers\Rendering\View $this
 */

$this->useLayout('authentication');

?>
<div class="container">
    <div class="row well">
        <h1><?= _p('Authentication') ?></h1>

        <?php $this->renderPart('feedback') ?>
        <form action="<?= HTTP_BASE . '/@auth' ?>" method="post">
            <input type="hidden" name="returnUrl" value="<?= $returnUrl ?>"/>

            <div class="control-group">
                <label class="control-label" for="login"><?= _p('Your login') ?></label>
                <input id="login" class="input-large" name="login" type="text"/>
            </div>
            <div class="control-group">
                <label class="control-label" for="password"><?= _p('Your password') ?></label>
                <input id="password" class="input-large" name="password" type="password"/>
            </div>
            <div class="control-group">
                <input class="input-large btn btn-primary" name="submit" type="submit" value="<?= _p('Login!') ?>"/>
            </div>
            <div class="control-group">
                <a href="<?= HTTP_BASE . '/@forgot' ?>" class="btn btn-link"><?= _p('Forgot your password?') ?></a>
            </div>
        </form>
    </div>
</div>

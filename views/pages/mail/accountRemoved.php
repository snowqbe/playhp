<?php
/**
 * Account removed HTML email content
 *
 * @var \PlayHP\Controllers\Rendering\View $this
 */

$this->useLayout('htmlMail');

?>
<p><?= sprintf(_p('Hi %s!'), $displayName) ?></p>
<p><?= _p('Although we\'re sorry to see you go, we confirm that your account has been marked for removal.') ?><br>
    <?= sprintf(_p('We hope to see you back on %s soon...'), $website) ?>
</p>
<p><?= _p('Sincerely,') ?><br/>
    <?= _p('Our team') ?></p>

<?php
/**
 * Account removed HTML email content
 *
 * @var \PlayHP\Controllers\Rendering\View $this
 */

$this->useLayout('textMail');

?>
<?= sprintf(_p('Hi %s!'), $displayName) ?>

<?= _p('Although we\'re sorry to see you go, we confirm that your account has been marked for removal.') ?>
<?= sprintf(_p('We hope to see you back on %s soon...'), $website) ?>

<?= _p('Sincerely,') ?>
<?= _p('Our team') ?>

<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * @var \PlayHP\Controllers\Rendering\View $this
 */

$this->useLayout('htmlMail');
?>
<p><?= sprintf(_p('Hi %s!'), $displayName) ?></p>
<p><?= sprintf(_p('Thank you for joining us on %s, and welcome!'), $website) ?><br>
    <?= sprintf(_p('Please <a href="%s">verify your email address</a> by clicking on the following link, or pasting it in the browser: %s'), $verifyLink, $verifyLink) ?>
</p>
<p><?= _p('Thank you!') ?></p>
<p><?= _p('Our team') ?></p>

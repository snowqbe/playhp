<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * @var \PlayHP\Controllers\Rendering\View $this
 */

$this->useLayout('textMail');

echo sprintf(_p('Hi %s!'), $displayName)
?>
<?= sprintf(_p('Thank you for joining us on %s, and welcome!'), $website) ?>

<?= sprintf(_p('Please verify your email address by visiting on the following link: %s'), $verificationLink) ?>

<?= _p('Thank you!') ?>
<?= _p('Our team') ?>
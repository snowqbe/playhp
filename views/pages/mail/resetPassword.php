<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * @var \PlayHP\Controllers\Rendering\View $this
 */

$this->useLayout('htmlMail');
?>
<p><?= sprintf(_p('Hi %s!'), $displayName) ?></p>
<p><?= sprintf(_p('You, or someone on your behalf has asked to reset your password on %s'), $website) ?><br>
    <?= sprintf(_p('This new password is ')) ?>
    <strong><?= $newPassword ?></strong>
</p>
<p><?= sprintf(_p('You can <a href="%s">create a new one here</a>'), $changeLink) ?></p>
<p><?= _p('Thank you!') ?><br/>
    <?= _p('Our team') ?></p>

<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * @var \PlayHP\Controllers\Rendering\View $this
 */

$this->useLayout('textMail');

echo sprintf(_p('Hi %s!'), $displayName)
?>
<?= sprintf(_p('You, or someone on your behalf has asked to reset your password on %s'), $website) ?>

<?= sprintf(_p('This new password is ')) . $newPassword ?>

<?= sprintf(_p('You can create a new one here: %s'), $changeLink) ?>

<?= _p('Thank you!') ?>
<?= _p('Our team') ?>
<?php
/**
 * @var \PlayHP\Controllers\Rendering\View $this
 */

$this->useLayout('authentication');

?>
<div class="container well">
    <h1><?= _p('Register') ?></h1>
    <?php $this->renderPart('feedback') ?>
    <form action="<?= HTTP_BASE . '/@register' ?>" method="post">
        <div class="row">
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="login"><?= _p('Login') ?></label>
                    <input id="login" class="input-large" name="user.login" type="text"
                           value="<?= $user ? $user->login : '' ?>"/>
                </div>
                <div class="control-group">
                    <label class="control-label" for="password"><?= _p('Choose your password') ?></label>
                    <input id="password" class="input-large" name="user.password" type="password"/>
                </div>
                <div class="control-group">
                    <label class="control-label" for="passwordConfirm"><?= _p('Confirm your password') ?></label>
                    <input id="passwordConfirm" class="input-large" name="user.passwordConfirm" type="password"/>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="email"><?= _p('E-mail') ?></label>
                    <input id="email" class="input-large" name="user.email" type="email"
                           value="<?= $user ? $user->email : '' ?>"/>
                </div>
                <div class="control-group">
                    <label class="control-label" for="firstName"><?= _p('First name') ?></label>
                    <input id="firstName" class="input-large" name="user.firstName" type="text"
                           value="<?= $user ? $user->firstName : '' ?>"/>
                </div>
                <div class="control-group">
                    <label class="control-label" for="lastName"><?= _p('Last name') ?></label>
                    <input id="lastName" class="input-large" name="user.lastName" type="text"
                           value="<?= $user ? $user->lastName : '' ?>"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="span12">
                <div class="control-group">
                    <label class="checkbox" for="acceptTerms">
                        <input id="acceptTerms" class="input-large" name="user.acceptTerms"
                               type="checkbox" <?= $user ? ($user->acceptTerms ? 'checked="checked"' : '') : '' ?>/>
                        <?= _p('I have read and accept the terms of use') ?>
                    </label>
                </div>
                <div class="control-group">
                    <input class="btn btn-primary btn-large" name="submit" type="submit" value="<?= _p('Register') ?>"/>
                </div>
                <div class="control-group">
                    <a href="<?= HTTP_BASE . '/@login' ?>"
                       class="btn-link"><?= _p('Already have an account? Login!') ?></a>
                </div>
            </div>
        </div>
    </form>
</div>

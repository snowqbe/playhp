<?php
/**
 * Copyright SnowQbe (c) 2013
 * @var \PlayHP\Controllers\Rendering\View $this
 */

use PlayHP\PlayHP;

$r = PlayHP::router();
$showTestsLink = $r->reverse('\PlayHP\Testing\Tester', 'showTests');

/**
 * Shortcut for creating a simple link, keeping XDEBUG_SESSION_START parameter for easy debugging
 * @param String $link Link to echo
 * @return string Transformed link
 */
function l($link)
{
    if (isset($_GET['XDEBUG_SESSION_START'])) {
        $xdbg = $_GET['XDEBUG_SESSION_START'];
        if (strpos($link, '?') !== FALSE) {
            $link .= "&";
        } else {
            $link .= "?";
        }
        $link .= "XDEBUG_SESSION_START=$xdbg";
    }
    return BASE . $link;
}

$this->useLayout('playhp');

$this->script('initToggle', '
    $.ready(function() {
        $(".details").each(function() {
            $(this).toggle();
        }
    });', array('jquery'));

?>

<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="<?= l($showTestsLink) ?>">PlayHP Test Runner</a>

            <div class="nav-collapse collapse">
                <p class="navbar-text pull-right">
                </p>
                <ul class="nav">
                    <li class="active"><a href="<?= l($showTestsLink) ?>">Home</a></li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>


<div class="container-fluid">
    <div class="row-fluid">
        <div class="span3">
            <div class="well sidebar-nav">
                <ul class="nav nav-list">
                    <?php
                    foreach ($packages as $package => $testClasses) {
                        echo "<li class='nav-header'>" . substr($package, strpos(str_replace('\\', '.', $package), '\\')) . "</li>";
                        foreach ($testClasses as $testClass => $testMethods) {
                            $execUrl = $r->reverse('\PlayHP\Testing\Tester', 'runTestSuite', array(
                                'selectedPackage' => $package,
                                'selectedClass' => $testClass,
                                'selectedMethods' => array_keys($testMethods),
                                'all' => false
                            ));
                            echo "<li" . ($testClass == $selectedClass ? ' class="active"' : '') . "><a href='" . l($execUrl) . "'>$testClass</a></li>";
                        }
                    }

                    ?>
                </ul>
            </div>
            <!--/.well -->
        </div>
        <!--/span-->
        <div class="span9">
            <form action="<?= BASE . $r->reverse('PlayHP\Testing\Tester', 'runTestSuite') ?>" method="post">
                <input type="hidden" name="selectedClass" value="<?= $selectedClass ?>"/>
                <input type="hidden" name="selectedPackage" value="<?= $selectedPackage ?>"/>
                <?php
                if (!is_null($alerts)) {
                    if (!empty($alerts)) {
                        echo '<div class="alert alert-error">';
                        echo '<h1>Errors</h1>';
                        echo '<p></p>';
                        echo '<ul>';
                        foreach ($alerts as $i => $alert) {
                            echo '<li>';
                            $lastPart = "<a href='" . l($alert['execUrl']) . "'>$alert[testClassPackage]\\$alert[testClassName]::$alert[testName]():$alert[line]</a>";
                            echo "<strong>$lastPart</strong><br/>" . $alert['statusMessage'];
                            echo '<br><a href="#details' . $i . '" onclick="toggleDetails(' . $i . ')">Show stack trace</a><div class="errorDetails" id="details' . $i . '"><pre>' . "\n";
                            $top = min($deep, count($alert['stackTrace']));
                            for ($j = 1; $j < $top; $j++) {
                                $traceElementLine = $alert['stackTrace'][$j - 1];
                                $traceElement = $alert['stackTrace'][$j];
                                if (!isset($traceElement['class'])) {
                                    $traceFile = str_replace('\\', '/', $traceElement['file']);
                                    $traceElement['class'] = substr($traceFile, strrpos($traceFile, '/') + 1) . ' ';
                                } else {
                                    $traceElement['class'] = $traceElement['class'] . '->';
                                }
                                echo $traceElement['class'] . $traceElement['function'] . '()' . (isset($traceElementLine['line']) ? ' at line ' . $traceElementLine['line'] : '') . '<br/>';
                            }
                            echo '</pre></div>';
                            echo '</li>';
                        }
                        echo '</ul>';
                        echo '</div>';
                    } else {
                        echo '<div class="alert alert-success">';
                        echo '<h1>OK !</h1>';
                        echo '</div>';
                    }
                    echo '<hr>';
                }
                ?>
                <div>
                    <?php
                    if (!empty($selectedClass)) {
                        echo "<h2 onclick='toggle(\"${package}-${testClass}\")' style='cursor: pointer'>$selectedClass</h2>";
                        echo "<p>Choose a test to launch:</p>";
                        echo "<ul class='unstyled'>";
                        if (!empty($selectedPackage)) {
                            $testMethods = $packages[$selectedPackage][$selectedClass];
                            foreach ($testMethods as $testMethod => $testMethodReflect) {
                                $checkId = "${package}-${testClass}-${testMethod}";
                                echo "<label for='$checkId' class='checkbox' style='cursor: pointer'>";
                                echo "<input type='checkbox' onclick='checkActivation()' name='selectedMethods[]' id='$checkId' value='$testMethod' " . (is_array($selectedMethods) && in_array($testMethod, $selectedMethods) ? " checked='checked'" : "") . " />";
                                echo " ${testMethod}()</label>";
                            }
                        }
                        echo "</ul>";
                    }
                    ?>
                </div>
                <div class="row-fluid">
                    <?php
                    if (!empty($selectedClass)) {
                        ?>
                        <input type="submit"
                               class="btn btn-large <?= count($selectedMethods) > 0 ? "btn-primary" : "" ?>"
                               id="test" <?= count($selectedMethods) > 0 ? "" : 'disabled="disabled"' ?> name="test"
                               value="Test selected">
                    <?php
                    }
                    ?>

                    <input type="submit" class="btn btn-success btn-large" id="testAll" name="testAll"
                           value="Test all !">
                </div>
            </form>

            <?php if ($executionTime !== null): ?>
                <hr/>
                <div>
                    <blockquote class="muted">Execution time: <?= round($executionTime * 1000, 3) ?>ms
                    </blockquote>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<script src="//code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript">

    function activateSelectiveTest() {
        $("#test").removeAttr('disabled');
        $("#test").addClass('btn-primary');
    }

    function disableSelectiveTest() {
        $("#test").attr('disabled', "disabled");
        $("#test").toggleClass('btn-primary');
    }

    function toggle(className) {
        $("input[type='checkbox']").each(function () {
            if ($(this).attr('checked') == 'checked') {
                $(this).removeAttr('checked');
            }
            else {
                $(this).attr('checked', 'checked');
            }
        })
        if ($("input[type='checkbox'][checked='checked']").length > 0) {
            activateSelectiveTest();
        } else {
            disableSelectiveTest();
        }
    }
    function checkActivation() {
        var ok = false;
        $("input[type='checkbox']").each(function () {
            if ($(this).attr('checked') == 'checked') {
                ok = true;
            }
        });
        if (ok) {
            activateSelectiveTest();
        }
        else {
            disableSelectiveTest();
        }
    }

    function toggleDetails(detailsId) {
        $("#details" + detailsId).toggle();
    }

    $(function () {
        $(".errorDetails").each(function () {
            $(this).hide();
        });
    });
</script>
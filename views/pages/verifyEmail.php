<?php
/**
 * verifyEmail page
 *
 * @var \PlayHP\Controllers\Rendering\View $this
 */

$this->useLayout('authentication');

// Redirect in 5 seconds
$this->metaRedirect(5, HTTP_BASE);
?>
<div class="container">
    <?php $this->renderPart('feedback'); ?>
    <p><a href="<?= HTTP_BASE ?>"><?= _p('Back to home page') ?></a></p>
</div>

<?php
/**
 * Welcome page for PlayHP
 * @var \PlayHP\Controllers\Rendering\View $this
 */
$this->useLayout('main');

?>
<header>
    <h1><?= _("PlayHP - Welcome") ?></h1>
</header>
<p>You've set up your PlayHP app!</p>
<p>Where to go from here:</p>
<ul>
    <li>Adjust your first controller</li>
    <li>Adjust your first view</li>
    <li>Adjust your main layout</li>
    <li>...</li>
    <li>Want more? Follow the tutorial</li>
</ul>
<hr>
<footer>
    <p><?= _("2012 - Powered by SnowQbe") ?></p>
</footer>
<?php
/*
 * This file is part of the PlayHP package.
 *
 * (c) Herve Labas <herve@snowqbe.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

if (isset($error)): ?>
    <div class="alert alert-error">
        <?= $error ?>
    </div>
<?php endif; ?>
<?php if (isset($errors) && !empty($errors)): ?>
    <div class="alert alert-error">
        <?= _p('Please correct the following errors') ?>
        <ul>
            <?php
            foreach ($errors as $error) {

                echo '<li>' . $error . '</li>' . PHP_EOL;
            }
            ?>
        </ul>
    </div>
<?php endif; ?>
<?php if (isset($success)): ?>
    <div class="alert alert-success">
        <?= $success ?>
    </div>
<?php endif; ?>